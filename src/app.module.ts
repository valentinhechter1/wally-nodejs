import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './modules/users/users.module'
import { FavouritesModule } from './modules/favourites/favourites.module'





@Module({
  imports: [UsersModule, FavouritesModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
