import { Body, Response, Controller, Get, Post, Put, Query } from '@nestjs/common';
import { FavouritesService } from './favourites.service';

@Controller('favourites')
export class FavouritesController {
  constructor(private readonly favourites: FavouritesService) { }

  @Post('all')
  getFavourite(@Body() data): Promise<Array<any>> {

    return this.favourites.getFavourite(data)
  }

  @Post('new')
  createFavourite(@Body() data: object): Promise<any> {

    return this.favourites.createFavourite(data)
  }

}