import { Injectable } from '@nestjs/common';
import { PrismaClient } from '@prisma/client'


@Injectable()
export class FavouritesService {
  public Favourites = new PrismaClient();

  async createFavourite(favourite): Promise<String> {
    try {

      await this.Favourites.favourites.create({
        data: {
          userId: favourite.userId,
          favouriteCity: favourite.favouriteCity,
        }
      })
      return 'ok';
    } catch (err) {
      console.log(err);
      return err;

    }
  }

  async getFavourite(data): Promise<Array<any>> {

    try {
      
      const findFavourite = await this.Favourites.favourites.findMany({
        where: {
          userId: data.userId,
        },
      })

      return findFavourite;

    } catch (err) {
      console.log(err);
      return err;

    }
  }
}
