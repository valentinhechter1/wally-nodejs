import { Injectable } from '@nestjs/common';
import { PrismaClient } from '@prisma/client'


@Injectable()
export class UsersService {
  public user = new PrismaClient();

  async createUser(user): Promise<String> {
    // console.log(user);
    try {

      await this.user.user.create({
        data: {
          email: user.email,
          name: user.name,
          password: user.password,
        }
      })
      return 'ok';
    } catch (err) {
      console.log(err);
      return err;

    }
  }

  async getUser(user): Promise<Object> {
    // console.log(user);

    try {
      // console.log(user.email)
      const findUser = await this.user.user.findUnique({
        where: {
          email: user.email,
        },
      })
      
      if (findUser !== null) return { id: findUser.id, logged: 'logged' };
      return 'null';

    } catch (err) {
      console.log(err);
      return err;

    }
  }
}
