import { Body, Response, Controller, Get, Post, Put, Query } from '@nestjs/common';
import { UsersService } from './users.service';

@Controller('')
export class UsersController {
  constructor(private readonly users: UsersService) { }

  @Post('signup')
  createUser(@Body() data:object): Promise<String>  {
    
    return this.users.createUser(data)
  }

  @Post('login')
  getUser(@Body() data:object): Promise<Object>  {
    // console.log(data);
    
    return this.users.getUser(data)
  }

}