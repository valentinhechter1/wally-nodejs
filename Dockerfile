FROM node:14 as base

WORKDIR /app

# Add package file
RUN apt update && apt install -y netcat

COPY package.json ./
COPY yarn.lock ./

# Install deps
RUN yarn install --frozen-lockfile

# Copy source
COPY src ./src
COPY prisma ./prisma
COPY client ./client
COPY tsconfig*.json ./

# Build dist
RUN yarn build
RUN yarn prisma generate

# RUN npx prisma generate

# Expose port 3000
EXPOSE 6543
CMD ["yarn", "run", "start:prod"]