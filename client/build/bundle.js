
(function (l, r) { if (!l || l.getElementById('livereloadscript')) return; r = l.createElement('script'); r.async = 1; r.src = '//' + (self.location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1'; r.id = 'livereloadscript'; l.getElementsByTagName('head')[0].appendChild(r) })(self.document);
var app = (function () {
	'use strict';

	function noop() { }
	const identity = x => x;
	function assign(tar, src) {
		// @ts-ignore
		for (const k in src)
			tar[k] = src[k];
		return tar;
	}
	function add_location(element, file, line, column, char) {
		element.__svelte_meta = {
			loc: { file, line, column, char }
		};
	}
	function run(fn) {
		return fn();
	}
	function blank_object() {
		return Object.create(null);
	}
	function run_all(fns) {
		fns.forEach(run);
	}
	function is_function(thing) {
		return typeof thing === 'function';
	}
	function safe_not_equal(a, b) {
		return a != a ? b == b : a !== b || ((a && typeof a === 'object') || typeof a === 'function');
	}
	function is_empty(obj) {
		return Object.keys(obj).length === 0;
	}
	function validate_store(store, name) {
		if (store != null && typeof store.subscribe !== 'function') {
			throw new Error(`'${name}' is not a store with a 'subscribe' method`);
		}
	}
	function subscribe(store, ...callbacks) {
		if (store == null) {
			return noop;
		}
		const unsub = store.subscribe(...callbacks);
		return unsub.unsubscribe ? () => unsub.unsubscribe() : unsub;
	}
	function component_subscribe(component, store, callback) {
		component.$$.on_destroy.push(subscribe(store, callback));
	}
	function create_slot(definition, ctx, $$scope, fn) {
		if (definition) {
			const slot_ctx = get_slot_context(definition, ctx, $$scope, fn);
			return definition[0](slot_ctx);
		}
	}
	function get_slot_context(definition, ctx, $$scope, fn) {
		return definition[1] && fn
			? assign($$scope.ctx.slice(), definition[1](fn(ctx)))
			: $$scope.ctx;
	}
	function get_slot_changes(definition, $$scope, dirty, fn) {
		if (definition[2] && fn) {
			const lets = definition[2](fn(dirty));
			if ($$scope.dirty === undefined) {
				return lets;
			}
			if (typeof lets === 'object') {
				const merged = [];
				const len = Math.max($$scope.dirty.length, lets.length);
				for (let i = 0; i < len; i += 1) {
					merged[i] = $$scope.dirty[i] | lets[i];
				}
				return merged;
			}
			return $$scope.dirty | lets;
		}
		return $$scope.dirty;
	}
	function update_slot_base(slot, slot_definition, ctx, $$scope, slot_changes, get_slot_context_fn) {
		if (slot_changes) {
			const slot_context = get_slot_context(slot_definition, ctx, $$scope, get_slot_context_fn);
			slot.p(slot_context, slot_changes);
		}
	}
	function get_all_dirty_from_scope($$scope) {
		if ($$scope.ctx.length > 32) {
			const dirty = [];
			const length = $$scope.ctx.length / 32;
			for (let i = 0; i < length; i++) {
				dirty[i] = -1;
			}
			return dirty;
		}
		return -1;
	}
	function exclude_internal_props(props) {
		const result = {};
		for (const k in props)
			if (k[0] !== '$')
				result[k] = props[k];
		return result;
	}
	function compute_rest_props(props, keys) {
		const rest = {};
		keys = new Set(keys);
		for (const k in props)
			if (!keys.has(k) && k[0] !== '$')
				rest[k] = props[k];
		return rest;
	}
	function set_store_value(store, ret, value) {
		store.set(value);
		return ret;
	}
	function action_destroyer(action_result) {
		return action_result && is_function(action_result.destroy) ? action_result.destroy : noop;
	}

	const is_client = typeof window !== 'undefined';
	let now = is_client
		? () => window.performance.now()
		: () => Date.now();
	let raf = is_client ? cb => requestAnimationFrame(cb) : noop;

	const tasks = new Set();
	function run_tasks(now) {
		tasks.forEach(task => {
			if (!task.c(now)) {
				tasks.delete(task);
				task.f();
			}
		});
		if (tasks.size !== 0)
			raf(run_tasks);
	}
	/**
	 * Creates a new task that runs on each raf frame
	 * until it returns a falsy value or is aborted
	 */
	function loop(callback) {
		let task;
		if (tasks.size === 0)
			raf(run_tasks);
		return {
			promise: new Promise(fulfill => {
				tasks.add(task = { c: callback, f: fulfill });
			}),
			abort() {
				tasks.delete(task);
			}
		};
	}
	function append(target, node) {
		target.appendChild(node);
	}
	function get_root_for_style(node) {
		if (!node)
			return document;
		const root = node.getRootNode ? node.getRootNode() : node.ownerDocument;
		if (root.host) {
			return root;
		}
		return document;
	}
	function append_empty_stylesheet(node) {
		const style_element = element('style');
		append_stylesheet(get_root_for_style(node), style_element);
		return style_element;
	}
	function append_stylesheet(node, style) {
		append(node.head || node, style);
	}
	function insert(target, node, anchor) {
		target.insertBefore(node, anchor || null);
	}
	function detach(node) {
		node.parentNode.removeChild(node);
	}
	function destroy_each(iterations, detaching) {
		for (let i = 0; i < iterations.length; i += 1) {
			if (iterations[i])
				iterations[i].d(detaching);
		}
	}
	function element(name) {
		return document.createElement(name);
	}
	function svg_element(name) {
		return document.createElementNS('http://www.w3.org/2000/svg', name);
	}
	function text(data) {
		return document.createTextNode(data);
	}
	function space() {
		return text(' ');
	}
	function empty() {
		return text('');
	}
	function listen(node, event, handler, options) {
		node.addEventListener(event, handler, options);
		return () => node.removeEventListener(event, handler, options);
	}
	function prevent_default(fn) {
		return function (event) {
			event.preventDefault();
			// @ts-ignore
			return fn.call(this, event);
		};
	}
	function attr(node, attribute, value) {
		if (value == null)
			node.removeAttribute(attribute);
		else if (node.getAttribute(attribute) !== value)
			node.setAttribute(attribute, value);
	}
	function set_attributes(node, attributes) {
		// @ts-ignore
		const descriptors = Object.getOwnPropertyDescriptors(node.__proto__);
		for (const key in attributes) {
			if (attributes[key] == null) {
				node.removeAttribute(key);
			}
			else if (key === 'style') {
				node.style.cssText = attributes[key];
			}
			else if (key === '__value') {
				node.value = node[key] = attributes[key];
			}
			else if (descriptors[key] && descriptors[key].set) {
				node[key] = attributes[key];
			}
			else {
				attr(node, key, attributes[key]);
			}
		}
	}
	function children(element) {
		return Array.from(element.childNodes);
	}
	function set_input_value(input, value) {
		input.value = value == null ? '' : value;
	}
	function set_style(node, key, value, important) {
		node.style.setProperty(key, value, important ? 'important' : '');
	}
	function toggle_class(element, name, toggle) {
		element.classList[toggle ? 'add' : 'remove'](name);
	}
	function custom_event(type, detail, bubbles = false) {
		const e = document.createEvent('CustomEvent');
		e.initCustomEvent(type, bubbles, false, detail);
		return e;
	}

	const active_docs = new Set();
	let active = 0;
	// https://github.com/darkskyapp/string-hash/blob/master/index.js
	function hash(str) {
		let hash = 5381;
		let i = str.length;
		while (i--)
			hash = ((hash << 5) - hash) ^ str.charCodeAt(i);
		return hash >>> 0;
	}
	function create_rule(node, a, b, duration, delay, ease, fn, uid = 0) {
		const step = 16.666 / duration;
		let keyframes = '{\n';
		for (let p = 0; p <= 1; p += step) {
			const t = a + (b - a) * ease(p);
			keyframes += p * 100 + `%{${fn(t, 1 - t)}}\n`;
		}
		const rule = keyframes + `100% {${fn(b, 1 - b)}}\n}`;
		const name = `__svelte_${hash(rule)}_${uid}`;
		const doc = get_root_for_style(node);
		active_docs.add(doc);
		const stylesheet = doc.__svelte_stylesheet || (doc.__svelte_stylesheet = append_empty_stylesheet(node).sheet);
		const current_rules = doc.__svelte_rules || (doc.__svelte_rules = {});
		if (!current_rules[name]) {
			current_rules[name] = true;
			stylesheet.insertRule(`@keyframes ${name} ${rule}`, stylesheet.cssRules.length);
		}
		const animation = node.style.animation || '';
		node.style.animation = `${animation ? `${animation}, ` : ''}${name} ${duration}ms linear ${delay}ms 1 both`;
		active += 1;
		return name;
	}
	function delete_rule(node, name) {
		const previous = (node.style.animation || '').split(', ');
		const next = previous.filter(name
			? anim => anim.indexOf(name) < 0 // remove specific animation
			: anim => anim.indexOf('__svelte') === -1 // remove all Svelte animations
		);
		const deleted = previous.length - next.length;
		if (deleted) {
			node.style.animation = next.join(', ');
			active -= deleted;
			if (!active)
				clear_rules();
		}
	}
	function clear_rules() {
		raf(() => {
			if (active)
				return;
			active_docs.forEach(doc => {
				const stylesheet = doc.__svelte_stylesheet;
				let i = stylesheet.cssRules.length;
				while (i--)
					stylesheet.deleteRule(i);
				doc.__svelte_rules = {};
			});
			active_docs.clear();
		});
	}

	let current_component;
	function set_current_component(component) {
		current_component = component;
	}
	function get_current_component() {
		if (!current_component)
			throw new Error('Function called outside component initialization');
		return current_component;
	}
	function onMount(fn) {
		get_current_component().$$.on_mount.push(fn);
	}
	function afterUpdate(fn) {
		get_current_component().$$.after_update.push(fn);
	}
	function onDestroy(fn) {
		get_current_component().$$.on_destroy.push(fn);
	}
	function createEventDispatcher() {
		const component = get_current_component();
		return (type, detail) => {
			const callbacks = component.$$.callbacks[type];
			if (callbacks) {
				// TODO are there situations where events could be dispatched
				// in a server (non-DOM) environment?
				const event = custom_event(type, detail);
				callbacks.slice().forEach(fn => {
					fn.call(component, event);
				});
			}
		};
	}
	// TODO figure out if we still want to support
	// shorthand events, or if we want to implement
	// a real bubbling mechanism
	function bubble(component, event) {
		const callbacks = component.$$.callbacks[event.type];
		if (callbacks) {
			// @ts-ignore
			callbacks.slice().forEach(fn => fn.call(this, event));
		}
	}

	const dirty_components = [];
	const binding_callbacks = [];
	const render_callbacks = [];
	const flush_callbacks = [];
	const resolved_promise = Promise.resolve();
	let update_scheduled = false;
	function schedule_update() {
		if (!update_scheduled) {
			update_scheduled = true;
			resolved_promise.then(flush);
		}
	}
	function tick() {
		schedule_update();
		return resolved_promise;
	}
	function add_render_callback(fn) {
		render_callbacks.push(fn);
	}
	function add_flush_callback(fn) {
		flush_callbacks.push(fn);
	}
	let flushing = false;
	const seen_callbacks = new Set();
	function flush() {
		if (flushing)
			return;
		flushing = true;
		do {
			// first, call beforeUpdate functions
			// and update components
			for (let i = 0; i < dirty_components.length; i += 1) {
				const component = dirty_components[i];
				set_current_component(component);
				update(component.$$);
			}
			set_current_component(null);
			dirty_components.length = 0;
			while (binding_callbacks.length)
				binding_callbacks.pop()();
			// then, once components are updated, call
			// afterUpdate functions. This may cause
			// subsequent updates...
			for (let i = 0; i < render_callbacks.length; i += 1) {
				const callback = render_callbacks[i];
				if (!seen_callbacks.has(callback)) {
					// ...so guard against infinite loops
					seen_callbacks.add(callback);
					callback();
				}
			}
			render_callbacks.length = 0;
		} while (dirty_components.length);
		while (flush_callbacks.length) {
			flush_callbacks.pop()();
		}
		update_scheduled = false;
		flushing = false;
		seen_callbacks.clear();
	}
	function update($$) {
		if ($$.fragment !== null) {
			$$.update();
			run_all($$.before_update);
			const dirty = $$.dirty;
			$$.dirty = [-1];
			$$.fragment && $$.fragment.p($$.ctx, dirty);
			$$.after_update.forEach(add_render_callback);
		}
	}

	let promise;
	function wait() {
		if (!promise) {
			promise = Promise.resolve();
			promise.then(() => {
				promise = null;
			});
		}
		return promise;
	}
	function dispatch(node, direction, kind) {
		node.dispatchEvent(custom_event(`${direction ? 'intro' : 'outro'}${kind}`));
	}
	const outroing = new Set();
	let outros;
	function group_outros() {
		outros = {
			r: 0,
			c: [],
			p: outros // parent group
		};
	}
	function check_outros() {
		if (!outros.r) {
			run_all(outros.c);
		}
		outros = outros.p;
	}
	function transition_in(block, local) {
		if (block && block.i) {
			outroing.delete(block);
			block.i(local);
		}
	}
	function transition_out(block, local, detach, callback) {
		if (block && block.o) {
			if (outroing.has(block))
				return;
			outroing.add(block);
			outros.c.push(() => {
				outroing.delete(block);
				if (callback) {
					if (detach)
						block.d(1);
					callback();
				}
			});
			block.o(local);
		}
	}
	const null_transition = { duration: 0 };
	function create_in_transition(node, fn, params) {
		let config = fn(node, params);
		let running = false;
		let animation_name;
		let task;
		let uid = 0;
		function cleanup() {
			if (animation_name)
				delete_rule(node, animation_name);
		}
		function go() {
			const { delay = 0, duration = 300, easing = identity, tick = noop, css } = config || null_transition;
			if (css)
				animation_name = create_rule(node, 0, 1, duration, delay, easing, css, uid++);
			tick(0, 1);
			const start_time = now() + delay;
			const end_time = start_time + duration;
			if (task)
				task.abort();
			running = true;
			add_render_callback(() => dispatch(node, true, 'start'));
			task = loop(now => {
				if (running) {
					if (now >= end_time) {
						tick(1, 0);
						dispatch(node, true, 'end');
						cleanup();
						return running = false;
					}
					if (now >= start_time) {
						const t = easing((now - start_time) / duration);
						tick(t, 1 - t);
					}
				}
				return running;
			});
		}
		let started = false;
		return {
			start() {
				if (started)
					return;
				started = true;
				delete_rule(node);
				if (is_function(config)) {
					config = config();
					wait().then(go);
				}
				else {
					go();
				}
			},
			invalidate() {
				started = false;
			},
			end() {
				if (running) {
					cleanup();
					running = false;
				}
			}
		};
	}
	function create_out_transition(node, fn, params) {
		let config = fn(node, params);
		let running = true;
		let animation_name;
		const group = outros;
		group.r += 1;
		function go() {
			const { delay = 0, duration = 300, easing = identity, tick = noop, css } = config || null_transition;
			if (css)
				animation_name = create_rule(node, 1, 0, duration, delay, easing, css);
			const start_time = now() + delay;
			const end_time = start_time + duration;
			add_render_callback(() => dispatch(node, false, 'start'));
			loop(now => {
				if (running) {
					if (now >= end_time) {
						tick(0, 1);
						dispatch(node, false, 'end');
						if (!--group.r) {
							// this will result in `end()` being called,
							// so we don't need to clean up here
							run_all(group.c);
						}
						return false;
					}
					if (now >= start_time) {
						const t = easing((now - start_time) / duration);
						tick(1 - t, t);
					}
				}
				return running;
			});
		}
		if (is_function(config)) {
			wait().then(() => {
				// @ts-ignore
				config = config();
				go();
			});
		}
		else {
			go();
		}
		return {
			end(reset) {
				if (reset && config.tick) {
					config.tick(1, 0);
				}
				if (running) {
					if (animation_name)
						delete_rule(node, animation_name);
					running = false;
				}
			}
		};
	}

	const globals = (typeof window !== 'undefined'
		? window
		: typeof globalThis !== 'undefined'
			? globalThis
			: global);
	function outro_and_destroy_block(block, lookup) {
		transition_out(block, 1, 1, () => {
			lookup.delete(block.key);
		});
	}
	function update_keyed_each(old_blocks, dirty, get_key, dynamic, ctx, list, lookup, node, destroy, create_each_block, next, get_context) {
		let o = old_blocks.length;
		let n = list.length;
		let i = o;
		const old_indexes = {};
		while (i--)
			old_indexes[old_blocks[i].key] = i;
		const new_blocks = [];
		const new_lookup = new Map();
		const deltas = new Map();
		i = n;
		while (i--) {
			const child_ctx = get_context(ctx, list, i);
			const key = get_key(child_ctx);
			let block = lookup.get(key);
			if (!block) {
				block = create_each_block(key, child_ctx);
				block.c();
			}
			else if (dynamic) {
				block.p(child_ctx, dirty);
			}
			new_lookup.set(key, new_blocks[i] = block);
			if (key in old_indexes)
				deltas.set(key, Math.abs(i - old_indexes[key]));
		}
		const will_move = new Set();
		const did_move = new Set();
		function insert(block) {
			transition_in(block, 1);
			block.m(node, next);
			lookup.set(block.key, block);
			next = block.first;
			n--;
		}
		while (o && n) {
			const new_block = new_blocks[n - 1];
			const old_block = old_blocks[o - 1];
			const new_key = new_block.key;
			const old_key = old_block.key;
			if (new_block === old_block) {
				// do nothing
				next = new_block.first;
				o--;
				n--;
			}
			else if (!new_lookup.has(old_key)) {
				// remove old block
				destroy(old_block, lookup);
				o--;
			}
			else if (!lookup.has(new_key) || will_move.has(new_key)) {
				insert(new_block);
			}
			else if (did_move.has(old_key)) {
				o--;
			}
			else if (deltas.get(new_key) > deltas.get(old_key)) {
				did_move.add(new_key);
				insert(new_block);
			}
			else {
				will_move.add(old_key);
				o--;
			}
		}
		while (o--) {
			const old_block = old_blocks[o];
			if (!new_lookup.has(old_block.key))
				destroy(old_block, lookup);
		}
		while (n)
			insert(new_blocks[n - 1]);
		return new_blocks;
	}
	function validate_each_keys(ctx, list, get_context, get_key) {
		const keys = new Set();
		for (let i = 0; i < list.length; i++) {
			const key = get_key(get_context(ctx, list, i));
			if (keys.has(key)) {
				throw new Error('Cannot have duplicate keys in a keyed each');
			}
			keys.add(key);
		}
	}

	function get_spread_update(levels, updates) {
		const update = {};
		const to_null_out = {};
		const accounted_for = { $$scope: 1 };
		let i = levels.length;
		while (i--) {
			const o = levels[i];
			const n = updates[i];
			if (n) {
				for (const key in o) {
					if (!(key in n))
						to_null_out[key] = 1;
				}
				for (const key in n) {
					if (!accounted_for[key]) {
						update[key] = n[key];
						accounted_for[key] = 1;
					}
				}
				levels[i] = n;
			}
			else {
				for (const key in o) {
					accounted_for[key] = 1;
				}
			}
		}
		for (const key in to_null_out) {
			if (!(key in update))
				update[key] = undefined;
		}
		return update;
	}
	function get_spread_object(spread_props) {
		return typeof spread_props === 'object' && spread_props !== null ? spread_props : {};
	}

	function bind$1(component, name, callback) {
		const index = component.$$.props[name];
		if (index !== undefined) {
			component.$$.bound[index] = callback;
			callback(component.$$.ctx[index]);
		}
	}
	function create_component(block) {
		block && block.c();
	}
	function mount_component(component, target, anchor, customElement) {
		const { fragment, on_mount, on_destroy, after_update } = component.$$;
		fragment && fragment.m(target, anchor);
		if (!customElement) {
			// onMount happens before the initial afterUpdate
			add_render_callback(() => {
				const new_on_destroy = on_mount.map(run).filter(is_function);
				if (on_destroy) {
					on_destroy.push(...new_on_destroy);
				}
				else {
					// Edge case - component was destroyed immediately,
					// most likely as a result of a binding initialising
					run_all(new_on_destroy);
				}
				component.$$.on_mount = [];
			});
		}
		after_update.forEach(add_render_callback);
	}
	function destroy_component(component, detaching) {
		const $$ = component.$$;
		if ($$.fragment !== null) {
			run_all($$.on_destroy);
			$$.fragment && $$.fragment.d(detaching);
			// TODO null out other refs, including component.$$ (but need to
			// preserve final state?)
			$$.on_destroy = $$.fragment = null;
			$$.ctx = [];
		}
	}
	function make_dirty(component, i) {
		if (component.$$.dirty[0] === -1) {
			dirty_components.push(component);
			schedule_update();
			component.$$.dirty.fill(0);
		}
		component.$$.dirty[(i / 31) | 0] |= (1 << (i % 31));
	}
	function init(component, options, instance, create_fragment, not_equal, props, append_styles, dirty = [-1]) {
		const parent_component = current_component;
		set_current_component(component);
		const $$ = component.$$ = {
			fragment: null,
			ctx: null,
			// state
			props,
			update: noop,
			not_equal,
			bound: blank_object(),
			// lifecycle
			on_mount: [],
			on_destroy: [],
			on_disconnect: [],
			before_update: [],
			after_update: [],
			context: new Map(parent_component ? parent_component.$$.context : options.context || []),
			// everything else
			callbacks: blank_object(),
			dirty,
			skip_bound: false,
			root: options.target || parent_component.$$.root
		};
		append_styles && append_styles($$.root);
		let ready = false;
		$$.ctx = instance
			? instance(component, options.props || {}, (i, ret, ...rest) => {
				const value = rest.length ? rest[0] : ret;
				if ($$.ctx && not_equal($$.ctx[i], $$.ctx[i] = value)) {
					if (!$$.skip_bound && $$.bound[i])
						$$.bound[i](value);
					if (ready)
						make_dirty(component, i);
				}
				return ret;
			})
			: [];
		$$.update();
		ready = true;
		run_all($$.before_update);
		// `false` as a special case of no DOM component
		$$.fragment = create_fragment ? create_fragment($$.ctx) : false;
		if (options.target) {
			if (options.hydrate) {
				const nodes = children(options.target);
				// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
				$$.fragment && $$.fragment.l(nodes);
				nodes.forEach(detach);
			}
			else {
				// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
				$$.fragment && $$.fragment.c();
			}
			if (options.intro)
				transition_in(component.$$.fragment);
			mount_component(component, options.target, options.anchor, options.customElement);
			flush();
		}
		set_current_component(parent_component);
	}
	/**
	 * Base class for Svelte components. Used when dev=false.
	 */
	class SvelteComponent {
		$destroy() {
			destroy_component(this, 1);
			this.$destroy = noop;
		}
		$on(type, callback) {
			const callbacks = (this.$$.callbacks[type] || (this.$$.callbacks[type] = []));
			callbacks.push(callback);
			return () => {
				const index = callbacks.indexOf(callback);
				if (index !== -1)
					callbacks.splice(index, 1);
			};
		}
		$set($$props) {
			if (this.$$set && !is_empty($$props)) {
				this.$$.skip_bound = true;
				this.$$set($$props);
				this.$$.skip_bound = false;
			}
		}
	}

	function dispatch_dev(type, detail) {
		document.dispatchEvent(custom_event(type, Object.assign({ version: '3.42.3' }, detail), true));
	}
	function append_dev(target, node) {
		dispatch_dev('SvelteDOMInsert', { target, node });
		append(target, node);
	}
	function insert_dev(target, node, anchor) {
		dispatch_dev('SvelteDOMInsert', { target, node, anchor });
		insert(target, node, anchor);
	}
	function detach_dev(node) {
		dispatch_dev('SvelteDOMRemove', { node });
		detach(node);
	}
	function listen_dev(node, event, handler, options, has_prevent_default, has_stop_propagation) {
		const modifiers = options === true ? ['capture'] : options ? Array.from(Object.keys(options)) : [];
		if (has_prevent_default)
			modifiers.push('preventDefault');
		if (has_stop_propagation)
			modifiers.push('stopPropagation');
		dispatch_dev('SvelteDOMAddEventListener', { node, event, handler, modifiers });
		const dispose = listen(node, event, handler, options);
		return () => {
			dispatch_dev('SvelteDOMRemoveEventListener', { node, event, handler, modifiers });
			dispose();
		};
	}
	function attr_dev(node, attribute, value) {
		attr(node, attribute, value);
		if (value == null)
			dispatch_dev('SvelteDOMRemoveAttribute', { node, attribute });
		else
			dispatch_dev('SvelteDOMSetAttribute', { node, attribute, value });
	}
	function set_data_dev(text, data) {
		data = '' + data;
		if (text.wholeText === data)
			return;
		dispatch_dev('SvelteDOMSetData', { node: text, data });
		text.data = data;
	}
	function validate_each_argument(arg) {
		if (typeof arg !== 'string' && !(arg && typeof arg === 'object' && 'length' in arg)) {
			let msg = '{#each} only iterates over array-like objects.';
			if (typeof Symbol === 'function' && arg && Symbol.iterator in arg) {
				msg += ' You can use a spread to convert this iterable into an array.';
			}
			throw new Error(msg);
		}
	}
	function validate_slots(name, slot, keys) {
		for (const slot_key of Object.keys(slot)) {
			if (!~keys.indexOf(slot_key)) {
				console.warn(`<${name}> received an unexpected slot "${slot_key}".`);
			}
		}
	}
	/**
	 * Base class for Svelte components with some minor dev-enhancements. Used when dev=true.
	 */
	class SvelteComponentDev extends SvelteComponent {
		constructor(options) {
			if (!options || (!options.target && !options.$$inline)) {
				throw new Error("'target' is a required option");
			}
			super();
		}
		$destroy() {
			super.$destroy();
			this.$destroy = () => {
				console.warn('Component was already destroyed'); // eslint-disable-line no-console
			};
		}
		$capture_state() { }
		$inject_state() { }
	}

	/* src/Tailwindcss.svelte generated by Svelte v3.42.3 */

	function create_fragment$i(ctx) {
		const block = {
			c: noop,
			l: function claim(nodes) {
				throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			},
			m: noop,
			p: noop,
			i: noop,
			o: noop,
			d: noop
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_fragment$i.name,
			type: "component",
			source: "",
			ctx
		});

		return block;
	}

	function instance$i($$self, $$props) {
		let { $$slots: slots = {}, $$scope } = $$props;
		validate_slots('Tailwindcss', slots, []);
		const writable_props = [];

		Object.keys($$props).forEach(key => {
			if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Tailwindcss> was created with unknown prop '${key}'`);
		});

		return [];
	}

	class Tailwindcss extends SvelteComponentDev {
		constructor(options) {
			super(options);
			init(this, options, instance$i, create_fragment$i, safe_not_equal, {});

			dispatch_dev("SvelteRegisterComponent", {
				component: this,
				tagName: "Tailwindcss",
				options,
				id: create_fragment$i.name
			});
		}
	}

	/**
	 * @typedef {Object} WrappedComponent Object returned by the `wrap` method
	 * @property {SvelteComponent} component - Component to load (this is always asynchronous)
	 * @property {RoutePrecondition[]} [conditions] - Route pre-conditions to validate
	 * @property {Object} [props] - Optional dictionary of static props
	 * @property {Object} [userData] - Optional user data dictionary
	 * @property {bool} _sveltesparouter - Internal flag; always set to true
	 */

	/**
	 * @callback AsyncSvelteComponent
	 * @returns {Promise<SvelteComponent>} Returns a Promise that resolves with a Svelte component
	 */

	/**
	 * @callback RoutePrecondition
	 * @param {RouteDetail} detail - Route detail object
	 * @returns {boolean|Promise<boolean>} If the callback returns a false-y value, it's interpreted as the precondition failed, so it aborts loading the component (and won't process other pre-condition callbacks)
	 */

	/**
	 * @typedef {Object} WrapOptions Options object for the call to `wrap`
	 * @property {SvelteComponent} [component] - Svelte component to load (this is incompatible with `asyncComponent`)
	 * @property {AsyncSvelteComponent} [asyncComponent] - Function that returns a Promise that fulfills with a Svelte component (e.g. `{asyncComponent: () => import('Foo.svelte')}`)
	 * @property {SvelteComponent} [loadingComponent] - Svelte component to be displayed while the async route is loading (as a placeholder); when unset or false-y, no component is shown while component
	 * @property {object} [loadingParams] - Optional dictionary passed to the `loadingComponent` component as params (for an exported prop called `params`)
	 * @property {object} [userData] - Optional object that will be passed to events such as `routeLoading`, `routeLoaded`, `conditionsFailed`
	 * @property {object} [props] - Optional key-value dictionary of static props that will be passed to the component. The props are expanded with {...props}, so the key in the dictionary becomes the name of the prop.
	 * @property {RoutePrecondition[]|RoutePrecondition} [conditions] - Route pre-conditions to add, which will be executed in order
	 */

	/**
	 * Wraps a component to enable multiple capabilities:
	 * 1. Using dynamically-imported component, with (e.g. `{asyncComponent: () => import('Foo.svelte')}`), which also allows bundlers to do code-splitting.
	 * 2. Adding route pre-conditions (e.g. `{conditions: [...]}`)
	 * 3. Adding static props that are passed to the component
	 * 4. Adding custom userData, which is passed to route events (e.g. route loaded events) or to route pre-conditions (e.g. `{userData: {foo: 'bar}}`)
	 * 
	 * @param {WrapOptions} args - Arguments object
	 * @returns {WrappedComponent} Wrapped component
	 */
	function wrap$1(args) {
		if (!args) {
			throw Error('Parameter args is required')
		}

		// We need to have one and only one of component and asyncComponent
		// This does a "XNOR"
		if (!args.component == !args.asyncComponent) {
			throw Error('One and only one of component and asyncComponent is required')
		}

		// If the component is not async, wrap it into a function returning a Promise
		if (args.component) {
			args.asyncComponent = () => Promise.resolve(args.component);
		}

		// Parameter asyncComponent and each item of conditions must be functions
		if (typeof args.asyncComponent != 'function') {
			throw Error('Parameter asyncComponent must be a function')
		}
		if (args.conditions) {
			// Ensure it's an array
			if (!Array.isArray(args.conditions)) {
				args.conditions = [args.conditions];
			}
			for (let i = 0; i < args.conditions.length; i++) {
				if (!args.conditions[i] || typeof args.conditions[i] != 'function') {
					throw Error('Invalid parameter conditions[' + i + ']')
				}
			}
		}

		// Check if we have a placeholder component
		if (args.loadingComponent) {
			args.asyncComponent.loading = args.loadingComponent;
			args.asyncComponent.loadingParams = args.loadingParams || undefined;
		}

		// Returns an object that contains all the functions to execute too
		// The _sveltesparouter flag is to confirm the object was created by this router
		const obj = {
			component: args.asyncComponent,
			userData: args.userData,
			conditions: (args.conditions && args.conditions.length) ? args.conditions : undefined,
			props: (args.props && Object.keys(args.props).length) ? args.props : {},
			_sveltesparouter: true
		};

		return obj
	}

	const subscriber_queue = [];
	/**
	 * Creates a `Readable` store that allows reading by subscription.
	 * @param value initial value
	 * @param {StartStopNotifier}start start and stop notifications for subscriptions
	 */
	function readable(value, start) {
		return {
			subscribe: writable(value, start).subscribe
		};
	}
	/**
	 * Create a `Writable` store that allows both updating and reading by subscription.
	 * @param {*=}value initial value
	 * @param {StartStopNotifier=}start start and stop notifications for subscriptions
	 */
	function writable(value, start = noop) {
		let stop;
		const subscribers = new Set();
		function set(new_value) {
			if (safe_not_equal(value, new_value)) {
				value = new_value;
				if (stop) { // store is ready
					const run_queue = !subscriber_queue.length;
					for (const subscriber of subscribers) {
						subscriber[1]();
						subscriber_queue.push(subscriber, value);
					}
					if (run_queue) {
						for (let i = 0; i < subscriber_queue.length; i += 2) {
							subscriber_queue[i][0](subscriber_queue[i + 1]);
						}
						subscriber_queue.length = 0;
					}
				}
			}
		}
		function update(fn) {
			set(fn(value));
		}
		function subscribe(run, invalidate = noop) {
			const subscriber = [run, invalidate];
			subscribers.add(subscriber);
			if (subscribers.size === 1) {
				stop = start(set) || noop;
			}
			run(value);
			return () => {
				subscribers.delete(subscriber);
				if (subscribers.size === 0) {
					stop();
					stop = null;
				}
			};
		}
		return { set, update, subscribe };
	}
	function derived(stores, fn, initial_value) {
		const single = !Array.isArray(stores);
		const stores_array = single
			? [stores]
			: stores;
		const auto = fn.length < 2;
		return readable(initial_value, (set) => {
			let inited = false;
			const values = [];
			let pending = 0;
			let cleanup = noop;
			const sync = () => {
				if (pending) {
					return;
				}
				cleanup();
				const result = fn(single ? values[0] : values, set);
				if (auto) {
					set(result);
				}
				else {
					cleanup = is_function(result) ? result : noop;
				}
			};
			const unsubscribers = stores_array.map((store, i) => subscribe(store, (value) => {
				values[i] = value;
				pending &= ~(1 << i);
				if (inited) {
					sync();
				}
			}, () => {
				pending |= (1 << i);
			}));
			inited = true;
			sync();
			return function stop() {
				run_all(unsubscribers);
				cleanup();
			};
		});
	}

	function parse(str, loose) {
		if (str instanceof RegExp) return { keys: false, pattern: str };
		var c, o, tmp, ext, keys = [], pattern = '', arr = str.split('/');
		arr[0] || arr.shift();

		while (tmp = arr.shift()) {
			c = tmp[0];
			if (c === '*') {
				keys.push('wild');
				pattern += '/(.*)';
			} else if (c === ':') {
				o = tmp.indexOf('?', 1);
				ext = tmp.indexOf('.', 1);
				keys.push(tmp.substring(1, !!~o ? o : !!~ext ? ext : tmp.length));
				pattern += !!~o && !~ext ? '(?:/([^/]+?))?' : '/([^/]+?)';
				if (!!~ext) pattern += (!!~o ? '?' : '') + '\\' + tmp.substring(ext);
			} else {
				pattern += '/' + tmp;
			}
		}

		return {
			keys: keys,
			pattern: new RegExp('^' + pattern + (loose ? '(?=$|\/)' : '\/?$'), 'i')
		};
	}

	/* node_modules/svelte-spa-router/Router.svelte generated by Svelte v3.42.3 */

	const { Error: Error_1, Object: Object_1$1, console: console_1$2 } = globals;

	// (251:0) {:else}
	function create_else_block$4(ctx) {
		let switch_instance;
		let switch_instance_anchor;
		let current;
		const switch_instance_spread_levels = [/*props*/ ctx[2]];
		var switch_value = /*component*/ ctx[0];

		function switch_props(ctx) {
			let switch_instance_props = {};

			for (let i = 0; i < switch_instance_spread_levels.length; i += 1) {
				switch_instance_props = assign(switch_instance_props, switch_instance_spread_levels[i]);
			}

			return {
				props: switch_instance_props,
				$$inline: true
			};
		}

		if (switch_value) {
			switch_instance = new switch_value(switch_props());
			switch_instance.$on("routeEvent", /*routeEvent_handler_1*/ ctx[7]);
		}

		const block = {
			c: function create() {
				if (switch_instance) create_component(switch_instance.$$.fragment);
				switch_instance_anchor = empty();
			},
			m: function mount(target, anchor) {
				if (switch_instance) {
					mount_component(switch_instance, target, anchor);
				}

				insert_dev(target, switch_instance_anchor, anchor);
				current = true;
			},
			p: function update(ctx, dirty) {
				const switch_instance_changes = (dirty & /*props*/ 4)
					? get_spread_update(switch_instance_spread_levels, [get_spread_object(/*props*/ ctx[2])])
					: {};

				if (switch_value !== (switch_value = /*component*/ ctx[0])) {
					if (switch_instance) {
						group_outros();
						const old_component = switch_instance;

						transition_out(old_component.$$.fragment, 1, 0, () => {
							destroy_component(old_component, 1);
						});

						check_outros();
					}

					if (switch_value) {
						switch_instance = new switch_value(switch_props());
						switch_instance.$on("routeEvent", /*routeEvent_handler_1*/ ctx[7]);
						create_component(switch_instance.$$.fragment);
						transition_in(switch_instance.$$.fragment, 1);
						mount_component(switch_instance, switch_instance_anchor.parentNode, switch_instance_anchor);
					} else {
						switch_instance = null;
					}
				} else if (switch_value) {
					switch_instance.$set(switch_instance_changes);
				}
			},
			i: function intro(local) {
				if (current) return;
				if (switch_instance) transition_in(switch_instance.$$.fragment, local);
				current = true;
			},
			o: function outro(local) {
				if (switch_instance) transition_out(switch_instance.$$.fragment, local);
				current = false;
			},
			d: function destroy(detaching) {
				if (detaching) detach_dev(switch_instance_anchor);
				if (switch_instance) destroy_component(switch_instance, detaching);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_else_block$4.name,
			type: "else",
			source: "(251:0) {:else}",
			ctx
		});

		return block;
	}

	// (244:0) {#if componentParams}
	function create_if_block$6(ctx) {
		let switch_instance;
		let switch_instance_anchor;
		let current;
		const switch_instance_spread_levels = [{ params: /*componentParams*/ ctx[1] }, /*props*/ ctx[2]];
		var switch_value = /*component*/ ctx[0];

		function switch_props(ctx) {
			let switch_instance_props = {};

			for (let i = 0; i < switch_instance_spread_levels.length; i += 1) {
				switch_instance_props = assign(switch_instance_props, switch_instance_spread_levels[i]);
			}

			return {
				props: switch_instance_props,
				$$inline: true
			};
		}

		if (switch_value) {
			switch_instance = new switch_value(switch_props());
			switch_instance.$on("routeEvent", /*routeEvent_handler*/ ctx[6]);
		}

		const block = {
			c: function create() {
				if (switch_instance) create_component(switch_instance.$$.fragment);
				switch_instance_anchor = empty();
			},
			m: function mount(target, anchor) {
				if (switch_instance) {
					mount_component(switch_instance, target, anchor);
				}

				insert_dev(target, switch_instance_anchor, anchor);
				current = true;
			},
			p: function update(ctx, dirty) {
				const switch_instance_changes = (dirty & /*componentParams, props*/ 6)
					? get_spread_update(switch_instance_spread_levels, [
						dirty & /*componentParams*/ 2 && { params: /*componentParams*/ ctx[1] },
						dirty & /*props*/ 4 && get_spread_object(/*props*/ ctx[2])
					])
					: {};

				if (switch_value !== (switch_value = /*component*/ ctx[0])) {
					if (switch_instance) {
						group_outros();
						const old_component = switch_instance;

						transition_out(old_component.$$.fragment, 1, 0, () => {
							destroy_component(old_component, 1);
						});

						check_outros();
					}

					if (switch_value) {
						switch_instance = new switch_value(switch_props());
						switch_instance.$on("routeEvent", /*routeEvent_handler*/ ctx[6]);
						create_component(switch_instance.$$.fragment);
						transition_in(switch_instance.$$.fragment, 1);
						mount_component(switch_instance, switch_instance_anchor.parentNode, switch_instance_anchor);
					} else {
						switch_instance = null;
					}
				} else if (switch_value) {
					switch_instance.$set(switch_instance_changes);
				}
			},
			i: function intro(local) {
				if (current) return;
				if (switch_instance) transition_in(switch_instance.$$.fragment, local);
				current = true;
			},
			o: function outro(local) {
				if (switch_instance) transition_out(switch_instance.$$.fragment, local);
				current = false;
			},
			d: function destroy(detaching) {
				if (detaching) detach_dev(switch_instance_anchor);
				if (switch_instance) destroy_component(switch_instance, detaching);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_if_block$6.name,
			type: "if",
			source: "(244:0) {#if componentParams}",
			ctx
		});

		return block;
	}

	function create_fragment$h(ctx) {
		let current_block_type_index;
		let if_block;
		let if_block_anchor;
		let current;
		const if_block_creators = [create_if_block$6, create_else_block$4];
		const if_blocks = [];

		function select_block_type(ctx, dirty) {
			if (/*componentParams*/ ctx[1]) return 0;
			return 1;
		}

		current_block_type_index = select_block_type(ctx);
		if_block = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);

		const block = {
			c: function create() {
				if_block.c();
				if_block_anchor = empty();
			},
			l: function claim(nodes) {
				throw new Error_1("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			},
			m: function mount(target, anchor) {
				if_blocks[current_block_type_index].m(target, anchor);
				insert_dev(target, if_block_anchor, anchor);
				current = true;
			},
			p: function update(ctx, [dirty]) {
				let previous_block_index = current_block_type_index;
				current_block_type_index = select_block_type(ctx);

				if (current_block_type_index === previous_block_index) {
					if_blocks[current_block_type_index].p(ctx, dirty);
				} else {
					group_outros();

					transition_out(if_blocks[previous_block_index], 1, 1, () => {
						if_blocks[previous_block_index] = null;
					});

					check_outros();
					if_block = if_blocks[current_block_type_index];

					if (!if_block) {
						if_block = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);
						if_block.c();
					} else {
						if_block.p(ctx, dirty);
					}

					transition_in(if_block, 1);
					if_block.m(if_block_anchor.parentNode, if_block_anchor);
				}
			},
			i: function intro(local) {
				if (current) return;
				transition_in(if_block);
				current = true;
			},
			o: function outro(local) {
				transition_out(if_block);
				current = false;
			},
			d: function destroy(detaching) {
				if_blocks[current_block_type_index].d(detaching);
				if (detaching) detach_dev(if_block_anchor);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_fragment$h.name,
			type: "component",
			source: "",
			ctx
		});

		return block;
	}

	function wrap(component, userData, ...conditions) {
		// Use the new wrap method and show a deprecation warning
		// eslint-disable-next-line no-console
		console.warn('Method `wrap` from `svelte-spa-router` is deprecated and will be removed in a future version. Please use `svelte-spa-router/wrap` instead. See http://bit.ly/svelte-spa-router-upgrading');

		return wrap$1({ component, userData, conditions });
	}

	/**
	 * @typedef {Object} Location
	 * @property {string} location - Location (page/view), for example `/book`
	 * @property {string} [querystring] - Querystring from the hash, as a string not parsed
	 */
	/**
	 * Returns the current location from the hash.
	 *
	 * @returns {Location} Location object
	 * @private
	 */
	function getLocation() {
		const hashPosition = window.location.href.indexOf('#/');

		let location = hashPosition > -1
			? window.location.href.substr(hashPosition + 1)
			: '/';

		// Check if there's a querystring
		const qsPosition = location.indexOf('?');

		let querystring = '';

		if (qsPosition > -1) {
			querystring = location.substr(qsPosition + 1);
			location = location.substr(0, qsPosition);
		}

		return { location, querystring };
	}

	const loc = readable(null, // eslint-disable-next-line prefer-arrow-callback
		function start(set) {
			set(getLocation());

			const update = () => {
				set(getLocation());
			};

			window.addEventListener('hashchange', update, false);

			return function stop() {
				window.removeEventListener('hashchange', update, false);
			};
		});

	const location = derived(loc, $loc => $loc.location);
	const querystring = derived(loc, $loc => $loc.querystring);
	const params = writable(undefined);

	async function push(location) {
		if (!location || location.length < 1 || location.charAt(0) != '/' && location.indexOf('#/') !== 0) {
			throw Error('Invalid parameter location');
		}

		// Execute this code when the current call stack is complete
		await tick();

		// Note: this will include scroll state in history even when restoreScrollState is false
		history.replaceState(
			{
				...history.state,
				__svelte_spa_router_scrollX: window.scrollX,
				__svelte_spa_router_scrollY: window.scrollY
			},
			undefined,
			undefined
		);

		window.location.hash = (location.charAt(0) == '#' ? '' : '#') + location;
	}

	async function pop() {
		// Execute this code when the current call stack is complete
		await tick();

		window.history.back();
	}

	async function replace(location) {
		if (!location || location.length < 1 || location.charAt(0) != '/' && location.indexOf('#/') !== 0) {
			throw Error('Invalid parameter location');
		}

		// Execute this code when the current call stack is complete
		await tick();

		const dest = (location.charAt(0) == '#' ? '' : '#') + location;

		try {
			const newState = { ...history.state };
			delete newState['__svelte_spa_router_scrollX'];
			delete newState['__svelte_spa_router_scrollY'];
			window.history.replaceState(newState, undefined, dest);
		} catch (e) {
			// eslint-disable-next-line no-console
			console.warn('Caught exception while replacing the current page. If you\'re running this in the Svelte REPL, please note that the `replace` method might not work in this environment.');
		}

		// The method above doesn't trigger the hashchange event, so let's do that manually
		window.dispatchEvent(new Event('hashchange'));
	}

	function link(node, opts) {
		opts = linkOpts(opts);

		// Only apply to <a> tags
		if (!node || !node.tagName || node.tagName.toLowerCase() != 'a') {
			throw Error('Action "link" can only be used with <a> tags');
		}

		updateLink(node, opts);

		return {
			update(updated) {
				updated = linkOpts(updated);
				updateLink(node, updated);
			}
		};
	}

	// Internal function used by the link function
	function updateLink(node, opts) {
		let href = opts.href || node.getAttribute('href');

		// Destination must start with '/' or '#/'
		if (href && href.charAt(0) == '/') {
			// Add # to the href attribute
			href = '#' + href;
		} else if (!href || href.length < 2 || href.slice(0, 2) != '#/') {
			throw Error('Invalid value for "href" attribute: ' + href);
		}

		node.setAttribute('href', href);

		node.addEventListener('click', event => {
			// Prevent default anchor onclick behaviour
			event.preventDefault();

			if (!opts.disabled) {
				scrollstateHistoryHandler(event.currentTarget.getAttribute('href'));
			}
		});
	}

	// Internal function that ensures the argument of the link action is always an object
	function linkOpts(val) {
		if (val && typeof val == 'string') {
			return { href: val };
		} else {
			return val || {};
		}
	}

	/**
	 * The handler attached to an anchor tag responsible for updating the
	 * current history state with the current scroll state
	 *
	 * @param {string} href - Destination
	 */
	function scrollstateHistoryHandler(href) {
		// Setting the url (3rd arg) to href will break clicking for reasons, so don't try to do that
		history.replaceState(
			{
				...history.state,
				__svelte_spa_router_scrollX: window.scrollX,
				__svelte_spa_router_scrollY: window.scrollY
			},
			undefined,
			undefined
		);

		// This will force an update as desired, but this time our scroll state will be attached
		window.location.hash = href;
	}

	function instance$h($$self, $$props, $$invalidate) {
		let { $$slots: slots = {}, $$scope } = $$props;
		validate_slots('Router', slots, []);
		let { routes = {} } = $$props;
		let { prefix = '' } = $$props;
		let { restoreScrollState = false } = $$props;

		/**
	 * Container for a route: path, component
	 */
		class RouteItem {
			/**
	 * Initializes the object and creates a regular expression from the path, using regexparam.
	 *
	 * @param {string} path - Path to the route (must start with '/' or '*')
	 * @param {SvelteComponent|WrappedComponent} component - Svelte component for the route, optionally wrapped
	 */
			constructor(path, component) {
				if (!component || typeof component != 'function' && (typeof component != 'object' || component._sveltesparouter !== true)) {
					throw Error('Invalid component object');
				}

				// Path must be a regular or expression, or a string starting with '/' or '*'
				if (!path || typeof path == 'string' && (path.length < 1 || path.charAt(0) != '/' && path.charAt(0) != '*') || typeof path == 'object' && !(path instanceof RegExp)) {
					throw Error('Invalid value for "path" argument - strings must start with / or *');
				}

				const { pattern, keys } = parse(path);
				this.path = path;

				// Check if the component is wrapped and we have conditions
				if (typeof component == 'object' && component._sveltesparouter === true) {
					this.component = component.component;
					this.conditions = component.conditions || [];
					this.userData = component.userData;
					this.props = component.props || {};
				} else {
					// Convert the component to a function that returns a Promise, to normalize it
					this.component = () => Promise.resolve(component);

					this.conditions = [];
					this.props = {};
				}

				this._pattern = pattern;
				this._keys = keys;
			}

			/**
	 * Checks if `path` matches the current route.
	 * If there's a match, will return the list of parameters from the URL (if any).
	 * In case of no match, the method will return `null`.
	 *
	 * @param {string} path - Path to test
	 * @returns {null|Object.<string, string>} List of paramters from the URL if there's a match, or `null` otherwise.
	 */
			match(path) {
				// If there's a prefix, check if it matches the start of the path.
				// If not, bail early, else remove it before we run the matching.
				if (prefix) {
					if (typeof prefix == 'string') {
						if (path.startsWith(prefix)) {
							path = path.substr(prefix.length) || '/';
						} else {
							return null;
						}
					} else if (prefix instanceof RegExp) {
						const match = path.match(prefix);

						if (match && match[0]) {
							path = path.substr(match[0].length) || '/';
						} else {
							return null;
						}
					}
				}

				// Check if the pattern matches
				const matches = this._pattern.exec(path);

				if (matches === null) {
					return null;
				}

				// If the input was a regular expression, this._keys would be false, so return matches as is
				if (this._keys === false) {
					return matches;
				}

				const out = {};
				let i = 0;

				while (i < this._keys.length) {
					// In the match parameters, URL-decode all values
					try {
						out[this._keys[i]] = decodeURIComponent(matches[i + 1] || '') || null;
					} catch (e) {
						out[this._keys[i]] = null;
					}

					i++;
				}

				return out;
			}

			/**
	 * Dictionary with route details passed to the pre-conditions functions, as well as the `routeLoading`, `routeLoaded` and `conditionsFailed` events
	 * @typedef {Object} RouteDetail
	 * @property {string|RegExp} route - Route matched as defined in the route definition (could be a string or a reguar expression object)
	 * @property {string} location - Location path
	 * @property {string} querystring - Querystring from the hash
	 * @property {object} [userData] - Custom data passed by the user
	 * @property {SvelteComponent} [component] - Svelte component (only in `routeLoaded` events)
	 * @property {string} [name] - Name of the Svelte component (only in `routeLoaded` events)
	 */
			/**
	 * Executes all conditions (if any) to control whether the route can be shown. Conditions are executed in the order they are defined, and if a condition fails, the following ones aren't executed.
	 * 
	 * @param {RouteDetail} detail - Route detail
	 * @returns {boolean} Returns true if all the conditions succeeded
	 */
			async checkConditions(detail) {
				for (let i = 0; i < this.conditions.length; i++) {
					if (!await this.conditions[i](detail)) {
						return false;
					}
				}

				return true;
			}
		}

		// Set up all routes
		const routesList = [];

		if (routes instanceof Map) {
			// If it's a map, iterate on it right away
			routes.forEach((route, path) => {
				routesList.push(new RouteItem(path, route));
			});
		} else {
			// We have an object, so iterate on its own properties
			Object.keys(routes).forEach(path => {
				routesList.push(new RouteItem(path, routes[path]));
			});
		}

		// Props for the component to render
		let component = null;

		let componentParams = null;
		let props = {};

		// Event dispatcher from Svelte
		const dispatch = createEventDispatcher();

		// Just like dispatch, but executes on the next iteration of the event loop
		async function dispatchNextTick(name, detail) {
			// Execute this code when the current call stack is complete
			await tick();

			dispatch(name, detail);
		}

		// If this is set, then that means we have popped into this var the state of our last scroll position
		let previousScrollState = null;

		let popStateChanged = null;

		if (restoreScrollState) {
			popStateChanged = event => {
				// If this event was from our history.replaceState, event.state will contain
				// our scroll history. Otherwise, event.state will be null (like on forward
				// navigation)
				if (event.state && event.state.__svelte_spa_router_scrollY) {
					previousScrollState = event.state;
				} else {
					previousScrollState = null;
				}
			};

			// This is removed in the destroy() invocation below
			window.addEventListener('popstate', popStateChanged);

			afterUpdate(() => {
				// If this exists, then this is a back navigation: restore the scroll position
				if (previousScrollState) {
					window.scrollTo(previousScrollState.__svelte_spa_router_scrollX, previousScrollState.__svelte_spa_router_scrollY);
				} else {
					// Otherwise this is a forward navigation: scroll to top
					window.scrollTo(0, 0);
				}
			});
		}

		// Always have the latest value of loc
		let lastLoc = null;

		// Current object of the component loaded
		let componentObj = null;

		// Handle hash change events
		// Listen to changes in the $loc store and update the page
		// Do not use the $: syntax because it gets triggered by too many things
		const unsubscribeLoc = loc.subscribe(async newLoc => {
			lastLoc = newLoc;

			// Find a route matching the location
			let i = 0;

			while (i < routesList.length) {
				const match = routesList[i].match(newLoc.location);

				if (!match) {
					i++;
					continue;
				}

				const detail = {
					route: routesList[i].path,
					location: newLoc.location,
					querystring: newLoc.querystring,
					userData: routesList[i].userData,
					params: match && typeof match == 'object' && Object.keys(match).length
						? match
						: null
				};

				// Check if the route can be loaded - if all conditions succeed
				if (!await routesList[i].checkConditions(detail)) {
					// Don't display anything
					$$invalidate(0, component = null);

					componentObj = null;

					// Trigger an event to notify the user, then exit
					dispatchNextTick('conditionsFailed', detail);

					return;
				}

				// Trigger an event to alert that we're loading the route
				// We need to clone the object on every event invocation so we don't risk the object to be modified in the next tick
				dispatchNextTick('routeLoading', Object.assign({}, detail));

				// If there's a component to show while we're loading the route, display it
				const obj = routesList[i].component;

				// Do not replace the component if we're loading the same one as before, to avoid the route being unmounted and re-mounted
				if (componentObj != obj) {
					if (obj.loading) {
						$$invalidate(0, component = obj.loading);
						componentObj = obj;
						$$invalidate(1, componentParams = obj.loadingParams);
						$$invalidate(2, props = {});

						// Trigger the routeLoaded event for the loading component
						// Create a copy of detail so we don't modify the object for the dynamic route (and the dynamic route doesn't modify our object too)
						dispatchNextTick('routeLoaded', Object.assign({}, detail, {
							component,
							name: component.name,
							params: componentParams
						}));
					} else {
						$$invalidate(0, component = null);
						componentObj = null;
					}

					// Invoke the Promise
					const loaded = await obj();

					// Now that we're here, after the promise resolved, check if we still want this component, as the user might have navigated to another page in the meanwhile
					if (newLoc != lastLoc) {
						// Don't update the component, just exit
						return;
					}

					// If there is a "default" property, which is used by async routes, then pick that
					$$invalidate(0, component = loaded && loaded.default || loaded);

					componentObj = obj;
				}

				// Set componentParams only if we have a match, to avoid a warning similar to `<Component> was created with unknown prop 'params'`
				// Of course, this assumes that developers always add a "params" prop when they are expecting parameters
				if (match && typeof match == 'object' && Object.keys(match).length) {
					$$invalidate(1, componentParams = match);
				} else {
					$$invalidate(1, componentParams = null);
				}

				// Set static props, if any
				$$invalidate(2, props = routesList[i].props);

				// Dispatch the routeLoaded event then exit
				// We need to clone the object on every event invocation so we don't risk the object to be modified in the next tick
				dispatchNextTick('routeLoaded', Object.assign({}, detail, {
					component,
					name: component.name,
					params: componentParams
				})).then(() => {
					params.set(componentParams);
				});

				return;
			}

			// If we're still here, there was no match, so show the empty component
			$$invalidate(0, component = null);

			componentObj = null;
			params.set(undefined);
		});

		onDestroy(() => {
			unsubscribeLoc();
			popStateChanged && window.removeEventListener('popstate', popStateChanged);
		});

		const writable_props = ['routes', 'prefix', 'restoreScrollState'];

		Object_1$1.keys($$props).forEach(key => {
			if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console_1$2.warn(`<Router> was created with unknown prop '${key}'`);
		});

		function routeEvent_handler(event) {
			bubble.call(this, $$self, event);
		}

		function routeEvent_handler_1(event) {
			bubble.call(this, $$self, event);
		}

		$$self.$$set = $$props => {
			if ('routes' in $$props) $$invalidate(3, routes = $$props.routes);
			if ('prefix' in $$props) $$invalidate(4, prefix = $$props.prefix);
			if ('restoreScrollState' in $$props) $$invalidate(5, restoreScrollState = $$props.restoreScrollState);
		};

		$$self.$capture_state = () => ({
			readable,
			writable,
			derived,
			tick,
			_wrap: wrap$1,
			wrap,
			getLocation,
			loc,
			location,
			querystring,
			params,
			push,
			pop,
			replace,
			link,
			updateLink,
			linkOpts,
			scrollstateHistoryHandler,
			onDestroy,
			createEventDispatcher,
			afterUpdate,
			parse,
			routes,
			prefix,
			restoreScrollState,
			RouteItem,
			routesList,
			component,
			componentParams,
			props,
			dispatch,
			dispatchNextTick,
			previousScrollState,
			popStateChanged,
			lastLoc,
			componentObj,
			unsubscribeLoc
		});

		$$self.$inject_state = $$props => {
			if ('routes' in $$props) $$invalidate(3, routes = $$props.routes);
			if ('prefix' in $$props) $$invalidate(4, prefix = $$props.prefix);
			if ('restoreScrollState' in $$props) $$invalidate(5, restoreScrollState = $$props.restoreScrollState);
			if ('component' in $$props) $$invalidate(0, component = $$props.component);
			if ('componentParams' in $$props) $$invalidate(1, componentParams = $$props.componentParams);
			if ('props' in $$props) $$invalidate(2, props = $$props.props);
			if ('previousScrollState' in $$props) previousScrollState = $$props.previousScrollState;
			if ('popStateChanged' in $$props) popStateChanged = $$props.popStateChanged;
			if ('lastLoc' in $$props) lastLoc = $$props.lastLoc;
			if ('componentObj' in $$props) componentObj = $$props.componentObj;
		};

		if ($$props && "$$inject" in $$props) {
			$$self.$inject_state($$props.$$inject);
		}

		$$self.$$.update = () => {
			if ($$self.$$.dirty & /*restoreScrollState*/ 32) {
				// Update history.scrollRestoration depending on restoreScrollState
				history.scrollRestoration = restoreScrollState ? 'manual' : 'auto';
			}
		};

		return [
			component,
			componentParams,
			props,
			routes,
			prefix,
			restoreScrollState,
			routeEvent_handler,
			routeEvent_handler_1
		];
	}

	class Router extends SvelteComponentDev {
		constructor(options) {
			super(options);

			init(this, options, instance$h, create_fragment$h, safe_not_equal, {
				routes: 3,
				prefix: 4,
				restoreScrollState: 5
			});

			dispatch_dev("SvelteRegisterComponent", {
				component: this,
				tagName: "Router",
				options,
				id: create_fragment$h.name
			});
		}

		get routes() {
			throw new Error_1("<Router>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set routes(value) {
			throw new Error_1("<Router>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get prefix() {
			throw new Error_1("<Router>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set prefix(value) {
			throw new Error_1("<Router>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get restoreScrollState() {
			throw new Error_1("<Router>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set restoreScrollState(value) {
			throw new Error_1("<Router>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}
	}

	/* src/routes/SignUp.svelte generated by Svelte v3.42.3 */

	function create_fragment$g(ctx) {
		const block = {
			c: noop,
			l: function claim(nodes) {
				throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			},
			m: noop,
			p: noop,
			i: noop,
			o: noop,
			d: noop
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_fragment$g.name,
			type: "component",
			source: "",
			ctx
		});

		return block;
	}

	function instance$g($$self, $$props) {
		let { $$slots: slots = {}, $$scope } = $$props;
		validate_slots('SignUp', slots, []);
		const writable_props = [];

		Object.keys($$props).forEach(key => {
			if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<SignUp> was created with unknown prop '${key}'`);
		});

		return [];
	}

	class SignUp extends SvelteComponentDev {
		constructor(options) {
			super(options);
			init(this, options, instance$g, create_fragment$g, safe_not_equal, {});

			dispatch_dev("SvelteRegisterComponent", {
				component: this,
				tagName: "SignUp",
				options,
				id: create_fragment$g.name
			});
		}
	}

	/* src/components/Button.svelte generated by Svelte v3.42.3 */

	const file$f = "src/components/Button.svelte";

	function create_fragment$f(ctx) {
		let button;
		let current;
		const default_slot_template = /*#slots*/ ctx[1].default;
		const default_slot = create_slot(default_slot_template, ctx, /*$$scope*/ ctx[0], null);

		const block = {
			c: function create() {
				button = element("button");
				if (default_slot) default_slot.c();
				attr_dev(button, "class", "text-white bg-yellow-400 border-0 py-2 px-8 focus:outline-none hover:bg-yellow-500 rounded text-lg");
				add_location(button, file$f, 4, 0, 43);
			},
			l: function claim(nodes) {
				throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			},
			m: function mount(target, anchor) {
				insert_dev(target, button, anchor);

				if (default_slot) {
					default_slot.m(button, null);
				}

				current = true;
			},
			p: function update(ctx, [dirty]) {
				if (default_slot) {
					if (default_slot.p && (!current || dirty & /*$$scope*/ 1)) {
						update_slot_base(
							default_slot,
							default_slot_template,
							ctx,
    						/*$$scope*/ ctx[0],
							!current
								? get_all_dirty_from_scope(/*$$scope*/ ctx[0])
								: get_slot_changes(default_slot_template, /*$$scope*/ ctx[0], dirty, null),
							null
						);
					}
				}
			},
			i: function intro(local) {
				if (current) return;
				transition_in(default_slot, local);
				current = true;
			},
			o: function outro(local) {
				transition_out(default_slot, local);
				current = false;
			},
			d: function destroy(detaching) {
				if (detaching) detach_dev(button);
				if (default_slot) default_slot.d(detaching);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_fragment$f.name,
			type: "component",
			source: "",
			ctx
		});

		return block;
	}

	function instance$f($$self, $$props, $$invalidate) {
		let { $$slots: slots = {}, $$scope } = $$props;
		validate_slots('Button', slots, ['default']);
		const writable_props = [];

		Object.keys($$props).forEach(key => {
			if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Button> was created with unknown prop '${key}'`);
		});

		$$self.$$set = $$props => {
			if ('$$scope' in $$props) $$invalidate(0, $$scope = $$props.$$scope);
		};

		return [$$scope, slots];
	}

	class Button extends SvelteComponentDev {
		constructor(options) {
			super(options);
			init(this, options, instance$f, create_fragment$f, safe_not_equal, {});

			dispatch_dev("SvelteRegisterComponent", {
				component: this,
				tagName: "Button",
				options,
				id: create_fragment$f.name
			});
		}
	}

	/* src/components/Input.svelte generated by Svelte v3.42.3 */

	const file$e = "src/components/Input.svelte";

	function create_fragment$e(ctx) {
		let div;
		let label_1;
		let t0;
		let t1;
		let input;
		let mounted;
		let dispose;

		let input_levels = [
			{ id: /*label*/ ctx[1] },
			{ name: /*label*/ ctx[1] },
    		/*$$restProps*/ ctx[2],
			{
				class: "w-full bg-white rounded border border-gray-300 focus:border-pink-500 focus:ring-2 focus:ring-pink-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out"
			}
		];

		let input_data = {};

		for (let i = 0; i < input_levels.length; i += 1) {
			input_data = assign(input_data, input_levels[i]);
		}

		const block = {
			c: function create() {
				div = element("div");
				label_1 = element("label");
				t0 = text(/*label*/ ctx[1]);
				t1 = space();
				input = element("input");
				attr_dev(label_1, "for", /*label*/ ctx[1]);
				attr_dev(label_1, "class", "leading-7 text-sm text-gray-600");
				add_location(label_1, file$e, 6, 4, 94);
				set_attributes(input, input_data);
				add_location(input, file$e, 7, 4, 173);
				attr_dev(div, "class", "relative mb-4");
				add_location(div, file$e, 5, 0, 62);
			},
			l: function claim(nodes) {
				throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			},
			m: function mount(target, anchor) {
				insert_dev(target, div, anchor);
				append_dev(div, label_1);
				append_dev(label_1, t0);
				append_dev(div, t1);
				append_dev(div, input);
				if (input.autofocus) input.focus();
				set_input_value(input, /*value*/ ctx[0]);

				if (!mounted) {
					dispose = listen_dev(input, "input", /*input_input_handler*/ ctx[3]);
					mounted = true;
				}
			},
			p: function update(ctx, [dirty]) {
				if (dirty & /*label*/ 2) set_data_dev(t0, /*label*/ ctx[1]);

				if (dirty & /*label*/ 2) {
					attr_dev(label_1, "for", /*label*/ ctx[1]);
				}

				set_attributes(input, input_data = get_spread_update(input_levels, [
					dirty & /*label*/ 2 && { id: /*label*/ ctx[1] },
					dirty & /*label*/ 2 && { name: /*label*/ ctx[1] },
					dirty & /*$$restProps*/ 4 && /*$$restProps*/ ctx[2],
					{
						class: "w-full bg-white rounded border border-gray-300 focus:border-pink-500 focus:ring-2 focus:ring-pink-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out"
					}
				]));

				if (dirty & /*value*/ 1 && input.value !== /*value*/ ctx[0]) {
					set_input_value(input, /*value*/ ctx[0]);
				}
			},
			i: noop,
			o: noop,
			d: function destroy(detaching) {
				if (detaching) detach_dev(div);
				mounted = false;
				dispose();
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_fragment$e.name,
			type: "component",
			source: "",
			ctx
		});

		return block;
	}

	function instance$e($$self, $$props, $$invalidate) {
		const omit_props_names = ["label", "value"];
		let $$restProps = compute_rest_props($$props, omit_props_names);
		let { $$slots: slots = {}, $$scope } = $$props;
		validate_slots('Input', slots, []);
		let { label } = $$props;
		let { value } = $$props;

		function input_input_handler() {
			value = this.value;
			$$invalidate(0, value);
		}

		$$self.$$set = $$new_props => {
			$$props = assign(assign({}, $$props), exclude_internal_props($$new_props));
			$$invalidate(2, $$restProps = compute_rest_props($$props, omit_props_names));
			if ('label' in $$new_props) $$invalidate(1, label = $$new_props.label);
			if ('value' in $$new_props) $$invalidate(0, value = $$new_props.value);
		};

		$$self.$capture_state = () => ({ label, value });

		$$self.$inject_state = $$new_props => {
			if ('label' in $$props) $$invalidate(1, label = $$new_props.label);
			if ('value' in $$props) $$invalidate(0, value = $$new_props.value);
		};

		if ($$props && "$$inject" in $$props) {
			$$self.$inject_state($$props.$$inject);
		}

		return [value, label, $$restProps, input_input_handler];
	}

	class Input extends SvelteComponentDev {
		constructor(options) {
			super(options);
			init(this, options, instance$e, create_fragment$e, safe_not_equal, { label: 1, value: 0 });

			dispatch_dev("SvelteRegisterComponent", {
				component: this,
				tagName: "Input",
				options,
				id: create_fragment$e.name
			});

			const { ctx } = this.$$;
			const props = options.props || {};

			if (/*label*/ ctx[1] === undefined && !('label' in props)) {
				console.warn("<Input> was created without expected prop 'label'");
			}

			if (/*value*/ ctx[0] === undefined && !('value' in props)) {
				console.warn("<Input> was created without expected prop 'value'");
			}
		}

		get label() {
			throw new Error("<Input>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set label(value) {
			throw new Error("<Input>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get value() {
			throw new Error("<Input>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set value(value) {
			throw new Error("<Input>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}
	}

	/* src/components/Loader.svelte generated by Svelte v3.42.3 */

	const file$d = "src/components/Loader.svelte";

	function create_fragment$d(ctx) {
		let div4;
		let div0;
		let div1;
		let div2;
		let div3;

		const block = {
			c: function create() {
				div4 = element("div");
				div0 = element("div");
				div1 = element("div");
				div2 = element("div");
				div3 = element("div");
				attr_dev(div0, "class", "svelte-ka0r2");
				add_location(div0, file$d, 0, 34, 34);
				attr_dev(div1, "class", "svelte-ka0r2");
				add_location(div1, file$d, 0, 45, 45);
				attr_dev(div2, "class", "svelte-ka0r2");
				add_location(div2, file$d, 0, 56, 56);
				attr_dev(div3, "class", "svelte-ka0r2");
				add_location(div3, file$d, 0, 67, 67);
				attr_dev(div4, "id", "loader");
				attr_dev(div4, "class", "lds-ring svelte-ka0r2");
				add_location(div4, file$d, 0, 0, 0);
			},
			l: function claim(nodes) {
				throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			},
			m: function mount(target, anchor) {
				insert_dev(target, div4, anchor);
				append_dev(div4, div0);
				append_dev(div4, div1);
				append_dev(div4, div2);
				append_dev(div4, div3);
			},
			p: noop,
			i: noop,
			o: noop,
			d: function destroy(detaching) {
				if (detaching) detach_dev(div4);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_fragment$d.name,
			type: "component",
			source: "",
			ctx
		});

		return block;
	}

	function instance$d($$self, $$props) {
		let { $$slots: slots = {}, $$scope } = $$props;
		validate_slots('Loader', slots, []);
		const writable_props = [];

		Object.keys($$props).forEach(key => {
			if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Loader> was created with unknown prop '${key}'`);
		});

		return [];
	}

	class Loader extends SvelteComponentDev {
		constructor(options) {
			super(options);
			init(this, options, instance$d, create_fragment$d, safe_not_equal, {});

			dispatch_dev("SvelteRegisterComponent", {
				component: this,
				tagName: "Loader",
				options,
				id: create_fragment$d.name
			});
		}
	}

	const getCalendarPage = (month, year, dayProps, weekStart = 0) => {
		let date = new Date(year, month, 1);
		date.setDate(date.getDate() - date.getDay() + weekStart);
		let nextMonth = month === 11 ? 0 : month + 1;
		// ensure days starts on Sunday
		// and end on saturday
		let weeks = [];
		while (date.getMonth() !== nextMonth || date.getDay() !== weekStart || weeks.length !== 6) {
			if (date.getDay() === weekStart) weeks.unshift({ days: [], id: `${year}${month}${year}${weeks.length}` });
			const updated = Object.assign({
				partOfMonth: date.getMonth() === month,
				day: date.getDate(),
				month: date.getMonth(),
				year: date.getFullYear(),
				date: new Date(date)
			}, dayProps(date));
			weeks[0].days.push(updated);
			date.setDate(date.getDate() + 1);
		}
		weeks.reverse();
		return { month, year, weeks };
	};

	const getDayPropsHandler = (start, end, selectableCallback) => {
		let today = new Date();
		today.setHours(0, 0, 0, 0);
		return date => {
			const isInRange = date >= start && date <= end;
			return {
				isInRange,
				selectable: isInRange && (!selectableCallback || selectableCallback(date)),
				isToday: date.getTime() === today.getTime()
			};
		};
	};

	function getMonths(start, end, selectableCallback = null, weekStart = 0) {
		start.setHours(0, 0, 0, 0);
		end.setHours(0, 0, 0, 0);
		let endDate = new Date(end.getFullYear(), end.getMonth() + 1, 1);
		let months = [];
		let date = new Date(start.getFullYear(), start.getMonth(), 1);
		let dayPropsHandler = getDayPropsHandler(start, end, selectableCallback);
		while (date < endDate) {
			months.push(getCalendarPage(date.getMonth(), date.getFullYear(), dayPropsHandler, weekStart));
			date.setMonth(date.getMonth() + 1);
		}
		return months;
	}

	const areDatesEquivalent = (a, b) => a.getDate() === b.getDate()
		&& a.getMonth() === b.getMonth()
		&& a.getFullYear() === b.getFullYear();

	function cubicOut(t) {
		const f = t - 1.0;
		return f * f * f + 1.0;
	}

	function fade(node, { delay = 0, duration = 400, easing = identity } = {}) {
		const o = +getComputedStyle(node).opacity;
		return {
			delay,
			duration,
			easing,
			css: t => `opacity: ${t * o}`
		};
	}
	function fly(node, { delay = 0, duration = 400, easing = cubicOut, x = 0, y = 0, opacity = 0 } = {}) {
		const style = getComputedStyle(node);
		const target_opacity = +style.opacity;
		const transform = style.transform === 'none' ? '' : style.transform;
		const od = target_opacity * (1 - opacity);
		return {
			delay,
			duration,
			easing,
			css: (t, u) => `
			transform: ${transform} translate(${(1 - t) * x}px, ${(1 - t) * y}px);
			opacity: ${target_opacity - (od * u)}`
		};
	}

	/* node_modules/svelte-calendar/src/Components/Week.svelte generated by Svelte v3.42.3 */
	const file$c = "node_modules/svelte-calendar/src/Components/Week.svelte";

	function get_each_context$3(ctx, list, i) {
		const child_ctx = ctx.slice();
		child_ctx[7] = list[i];
		return child_ctx;
	}

	// (20:2) {#each days as day}
	function create_each_block$3(ctx) {
		let div;
		let button;
		let t0_value = /*day*/ ctx[7].date.getDate() + "";
		let t0;
		let t1;
		let mounted;
		let dispose;

		function click_handler() {
			return /*click_handler*/ ctx[6](/*day*/ ctx[7]);
		}

		const block = {
			c: function create() {
				div = element("div");
				button = element("button");
				t0 = text(t0_value);
				t1 = space();
				attr_dev(button, "class", "day--label svelte-1avwexm");
				attr_dev(button, "type", "button");
				toggle_class(button, "selected", areDatesEquivalent(/*day*/ ctx[7].date, /*selected*/ ctx[1]));
				toggle_class(button, "highlighted", areDatesEquivalent(/*day*/ ctx[7].date, /*highlighted*/ ctx[2]));
				toggle_class(button, "shake-date", /*shouldShakeDate*/ ctx[3] && areDatesEquivalent(/*day*/ ctx[7].date, /*shouldShakeDate*/ ctx[3]));
				toggle_class(button, "disabled", !/*day*/ ctx[7].selectable);
				add_location(button, file$c, 26, 6, 652);
				attr_dev(div, "class", "day svelte-1avwexm");
				toggle_class(div, "outside-month", !/*day*/ ctx[7].partOfMonth);
				toggle_class(div, "is-today", /*day*/ ctx[7].isToday);
				toggle_class(div, "is-disabled", !/*day*/ ctx[7].selectable);
				add_location(div, file$c, 20, 4, 493);
			},
			m: function mount(target, anchor) {
				insert_dev(target, div, anchor);
				append_dev(div, button);
				append_dev(button, t0);
				append_dev(div, t1);

				if (!mounted) {
					dispose = listen_dev(button, "click", click_handler, false, false, false);
					mounted = true;
				}
			},
			p: function update(new_ctx, dirty) {
				ctx = new_ctx;
				if (dirty & /*days*/ 1 && t0_value !== (t0_value = /*day*/ ctx[7].date.getDate() + "")) set_data_dev(t0, t0_value);

				if (dirty & /*areDatesEquivalent, days, selected*/ 3) {
					toggle_class(button, "selected", areDatesEquivalent(/*day*/ ctx[7].date, /*selected*/ ctx[1]));
				}

				if (dirty & /*areDatesEquivalent, days, highlighted*/ 5) {
					toggle_class(button, "highlighted", areDatesEquivalent(/*day*/ ctx[7].date, /*highlighted*/ ctx[2]));
				}

				if (dirty & /*shouldShakeDate, areDatesEquivalent, days*/ 9) {
					toggle_class(button, "shake-date", /*shouldShakeDate*/ ctx[3] && areDatesEquivalent(/*day*/ ctx[7].date, /*shouldShakeDate*/ ctx[3]));
				}

				if (dirty & /*days*/ 1) {
					toggle_class(button, "disabled", !/*day*/ ctx[7].selectable);
				}

				if (dirty & /*days*/ 1) {
					toggle_class(div, "outside-month", !/*day*/ ctx[7].partOfMonth);
				}

				if (dirty & /*days*/ 1) {
					toggle_class(div, "is-today", /*day*/ ctx[7].isToday);
				}

				if (dirty & /*days*/ 1) {
					toggle_class(div, "is-disabled", !/*day*/ ctx[7].selectable);
				}
			},
			d: function destroy(detaching) {
				if (detaching) detach_dev(div);
				mounted = false;
				dispose();
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_each_block$3.name,
			type: "each",
			source: "(20:2) {#each days as day}",
			ctx
		});

		return block;
	}

	function create_fragment$c(ctx) {
		let div;
		let div_intro;
		let div_outro;
		let current;
		let each_value = /*days*/ ctx[0];
		validate_each_argument(each_value);
		let each_blocks = [];

		for (let i = 0; i < each_value.length; i += 1) {
			each_blocks[i] = create_each_block$3(get_each_context$3(ctx, each_value, i));
		}

		const block = {
			c: function create() {
				div = element("div");

				for (let i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].c();
				}

				attr_dev(div, "class", "week svelte-1avwexm");
				add_location(div, file$c, 14, 0, 341);
			},
			l: function claim(nodes) {
				throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			},
			m: function mount(target, anchor) {
				insert_dev(target, div, anchor);

				for (let i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].m(div, null);
				}

				current = true;
			},
			p: function update(new_ctx, [dirty]) {
				ctx = new_ctx;

				if (dirty & /*days, areDatesEquivalent, selected, highlighted, shouldShakeDate, dispatch*/ 47) {
					each_value = /*days*/ ctx[0];
					validate_each_argument(each_value);
					let i;

					for (i = 0; i < each_value.length; i += 1) {
						const child_ctx = get_each_context$3(ctx, each_value, i);

						if (each_blocks[i]) {
							each_blocks[i].p(child_ctx, dirty);
						} else {
							each_blocks[i] = create_each_block$3(child_ctx);
							each_blocks[i].c();
							each_blocks[i].m(div, null);
						}
					}

					for (; i < each_blocks.length; i += 1) {
						each_blocks[i].d(1);
					}

					each_blocks.length = each_value.length;
				}
			},
			i: function intro(local) {
				if (current) return;

				if (local) {
					add_render_callback(() => {
						if (div_outro) div_outro.end(1);

						div_intro = create_in_transition(div, fly, {
							x: /*direction*/ ctx[4] * 50,
							duration: 180,
							delay: 90
						});

						div_intro.start();
					});
				}

				current = true;
			},
			o: function outro(local) {
				if (div_intro) div_intro.invalidate();

				if (local) {
					div_outro = create_out_transition(div, fade, { duration: 180 });
				}

				current = false;
			},
			d: function destroy(detaching) {
				if (detaching) detach_dev(div);
				destroy_each(each_blocks, detaching);
				if (detaching && div_outro) div_outro.end();
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_fragment$c.name,
			type: "component",
			source: "",
			ctx
		});

		return block;
	}

	function instance$c($$self, $$props, $$invalidate) {
		let { $$slots: slots = {}, $$scope } = $$props;
		validate_slots('Week', slots, []);
		const dispatch = createEventDispatcher();
		let { days } = $$props;
		let { selected } = $$props;
		let { highlighted } = $$props;
		let { shouldShakeDate } = $$props;
		let { direction } = $$props;
		const writable_props = ['days', 'selected', 'highlighted', 'shouldShakeDate', 'direction'];

		Object.keys($$props).forEach(key => {
			if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Week> was created with unknown prop '${key}'`);
		});

		const click_handler = day => dispatch('dateSelected', day.date);

		$$self.$$set = $$props => {
			if ('days' in $$props) $$invalidate(0, days = $$props.days);
			if ('selected' in $$props) $$invalidate(1, selected = $$props.selected);
			if ('highlighted' in $$props) $$invalidate(2, highlighted = $$props.highlighted);
			if ('shouldShakeDate' in $$props) $$invalidate(3, shouldShakeDate = $$props.shouldShakeDate);
			if ('direction' in $$props) $$invalidate(4, direction = $$props.direction);
		};

		$$self.$capture_state = () => ({
			areDatesEquivalent,
			fly,
			fade,
			createEventDispatcher,
			dispatch,
			days,
			selected,
			highlighted,
			shouldShakeDate,
			direction
		});

		$$self.$inject_state = $$props => {
			if ('days' in $$props) $$invalidate(0, days = $$props.days);
			if ('selected' in $$props) $$invalidate(1, selected = $$props.selected);
			if ('highlighted' in $$props) $$invalidate(2, highlighted = $$props.highlighted);
			if ('shouldShakeDate' in $$props) $$invalidate(3, shouldShakeDate = $$props.shouldShakeDate);
			if ('direction' in $$props) $$invalidate(4, direction = $$props.direction);
		};

		if ($$props && "$$inject" in $$props) {
			$$self.$inject_state($$props.$$inject);
		}

		return [
			days,
			selected,
			highlighted,
			shouldShakeDate,
			direction,
			dispatch,
			click_handler
		];
	}

	class Week extends SvelteComponentDev {
		constructor(options) {
			super(options);

			init(this, options, instance$c, create_fragment$c, safe_not_equal, {
				days: 0,
				selected: 1,
				highlighted: 2,
				shouldShakeDate: 3,
				direction: 4
			});

			dispatch_dev("SvelteRegisterComponent", {
				component: this,
				tagName: "Week",
				options,
				id: create_fragment$c.name
			});

			const { ctx } = this.$$;
			const props = options.props || {};

			if (/*days*/ ctx[0] === undefined && !('days' in props)) {
				console.warn("<Week> was created without expected prop 'days'");
			}

			if (/*selected*/ ctx[1] === undefined && !('selected' in props)) {
				console.warn("<Week> was created without expected prop 'selected'");
			}

			if (/*highlighted*/ ctx[2] === undefined && !('highlighted' in props)) {
				console.warn("<Week> was created without expected prop 'highlighted'");
			}

			if (/*shouldShakeDate*/ ctx[3] === undefined && !('shouldShakeDate' in props)) {
				console.warn("<Week> was created without expected prop 'shouldShakeDate'");
			}

			if (/*direction*/ ctx[4] === undefined && !('direction' in props)) {
				console.warn("<Week> was created without expected prop 'direction'");
			}
		}

		get days() {
			throw new Error("<Week>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set days(value) {
			throw new Error("<Week>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get selected() {
			throw new Error("<Week>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set selected(value) {
			throw new Error("<Week>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get highlighted() {
			throw new Error("<Week>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set highlighted(value) {
			throw new Error("<Week>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get shouldShakeDate() {
			throw new Error("<Week>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set shouldShakeDate(value) {
			throw new Error("<Week>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get direction() {
			throw new Error("<Week>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set direction(value) {
			throw new Error("<Week>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}
	}

	/* node_modules/svelte-calendar/src/Components/Month.svelte generated by Svelte v3.42.3 */
	const file$b = "node_modules/svelte-calendar/src/Components/Month.svelte";

	function get_each_context$2(ctx, list, i) {
		const child_ctx = ctx.slice();
		child_ctx[8] = list[i];
		return child_ctx;
	}

	// (20:2) {#each visibleMonth.weeks as week (week.id) }
	function create_each_block$2(key_1, ctx) {
		let first;
		let week;
		let current;

		week = new Week({
			props: {
				days: /*week*/ ctx[8].days,
				selected: /*selected*/ ctx[1],
				highlighted: /*highlighted*/ ctx[2],
				shouldShakeDate: /*shouldShakeDate*/ ctx[3],
				direction: /*direction*/ ctx[4]
			},
			$$inline: true
		});

		week.$on("dateSelected", /*dateSelected_handler*/ ctx[7]);

		const block = {
			key: key_1,
			first: null,
			c: function create() {
				first = empty();
				create_component(week.$$.fragment);
				this.first = first;
			},
			m: function mount(target, anchor) {
				insert_dev(target, first, anchor);
				mount_component(week, target, anchor);
				current = true;
			},
			p: function update(new_ctx, dirty) {
				ctx = new_ctx;
				const week_changes = {};
				if (dirty & /*visibleMonth*/ 1) week_changes.days = /*week*/ ctx[8].days;
				if (dirty & /*selected*/ 2) week_changes.selected = /*selected*/ ctx[1];
				if (dirty & /*highlighted*/ 4) week_changes.highlighted = /*highlighted*/ ctx[2];
				if (dirty & /*shouldShakeDate*/ 8) week_changes.shouldShakeDate = /*shouldShakeDate*/ ctx[3];
				if (dirty & /*direction*/ 16) week_changes.direction = /*direction*/ ctx[4];
				week.$set(week_changes);
			},
			i: function intro(local) {
				if (current) return;
				transition_in(week.$$.fragment, local);
				current = true;
			},
			o: function outro(local) {
				transition_out(week.$$.fragment, local);
				current = false;
			},
			d: function destroy(detaching) {
				if (detaching) detach_dev(first);
				destroy_component(week, detaching);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_each_block$2.name,
			type: "each",
			source: "(20:2) {#each visibleMonth.weeks as week (week.id) }",
			ctx
		});

		return block;
	}

	function create_fragment$b(ctx) {
		let div;
		let each_blocks = [];
		let each_1_lookup = new Map();
		let current;
		let each_value = /*visibleMonth*/ ctx[0].weeks;
		validate_each_argument(each_value);
		const get_key = ctx => /*week*/ ctx[8].id;
		validate_each_keys(ctx, each_value, get_each_context$2, get_key);

		for (let i = 0; i < each_value.length; i += 1) {
			let child_ctx = get_each_context$2(ctx, each_value, i);
			let key = get_key(child_ctx);
			each_1_lookup.set(key, each_blocks[i] = create_each_block$2(key, child_ctx));
		}

		const block = {
			c: function create() {
				div = element("div");

				for (let i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].c();
				}

				attr_dev(div, "class", "month-container svelte-l3zhme");
				add_location(div, file$b, 18, 0, 284);
			},
			l: function claim(nodes) {
				throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			},
			m: function mount(target, anchor) {
				insert_dev(target, div, anchor);

				for (let i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].m(div, null);
				}

				current = true;
			},
			p: function update(ctx, [dirty]) {
				if (dirty & /*visibleMonth, selected, highlighted, shouldShakeDate, direction*/ 31) {
					each_value = /*visibleMonth*/ ctx[0].weeks;
					validate_each_argument(each_value);
					group_outros();
					validate_each_keys(ctx, each_value, get_each_context$2, get_key);
					each_blocks = update_keyed_each(each_blocks, dirty, get_key, 1, ctx, each_value, each_1_lookup, div, outro_and_destroy_block, create_each_block$2, null, get_each_context$2);
					check_outros();
				}
			},
			i: function intro(local) {
				if (current) return;

				for (let i = 0; i < each_value.length; i += 1) {
					transition_in(each_blocks[i]);
				}

				current = true;
			},
			o: function outro(local) {
				for (let i = 0; i < each_blocks.length; i += 1) {
					transition_out(each_blocks[i]);
				}

				current = false;
			},
			d: function destroy(detaching) {
				if (detaching) detach_dev(div);

				for (let i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].d();
				}
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_fragment$b.name,
			type: "component",
			source: "",
			ctx
		});

		return block;
	}

	function instance$b($$self, $$props, $$invalidate) {
		let { $$slots: slots = {}, $$scope } = $$props;
		validate_slots('Month', slots, []);
		let { id } = $$props;
		let { visibleMonth } = $$props;
		let { selected } = $$props;
		let { highlighted } = $$props;
		let { shouldShakeDate } = $$props;
		let lastId = id;
		let direction;
		const writable_props = ['id', 'visibleMonth', 'selected', 'highlighted', 'shouldShakeDate'];

		Object.keys($$props).forEach(key => {
			if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Month> was created with unknown prop '${key}'`);
		});

		function dateSelected_handler(event) {
			bubble.call(this, $$self, event);
		}

		$$self.$$set = $$props => {
			if ('id' in $$props) $$invalidate(5, id = $$props.id);
			if ('visibleMonth' in $$props) $$invalidate(0, visibleMonth = $$props.visibleMonth);
			if ('selected' in $$props) $$invalidate(1, selected = $$props.selected);
			if ('highlighted' in $$props) $$invalidate(2, highlighted = $$props.highlighted);
			if ('shouldShakeDate' in $$props) $$invalidate(3, shouldShakeDate = $$props.shouldShakeDate);
		};

		$$self.$capture_state = () => ({
			Week,
			id,
			visibleMonth,
			selected,
			highlighted,
			shouldShakeDate,
			lastId,
			direction
		});

		$$self.$inject_state = $$props => {
			if ('id' in $$props) $$invalidate(5, id = $$props.id);
			if ('visibleMonth' in $$props) $$invalidate(0, visibleMonth = $$props.visibleMonth);
			if ('selected' in $$props) $$invalidate(1, selected = $$props.selected);
			if ('highlighted' in $$props) $$invalidate(2, highlighted = $$props.highlighted);
			if ('shouldShakeDate' in $$props) $$invalidate(3, shouldShakeDate = $$props.shouldShakeDate);
			if ('lastId' in $$props) $$invalidate(6, lastId = $$props.lastId);
			if ('direction' in $$props) $$invalidate(4, direction = $$props.direction);
		};

		if ($$props && "$$inject" in $$props) {
			$$self.$inject_state($$props.$$inject);
		}

		$$self.$$.update = () => {
			if ($$self.$$.dirty & /*lastId, id*/ 96) {
				{
					$$invalidate(4, direction = lastId < id ? 1 : -1);
					$$invalidate(6, lastId = id);
				}
			}
		};

		return [
			visibleMonth,
			selected,
			highlighted,
			shouldShakeDate,
			direction,
			id,
			lastId,
			dateSelected_handler
		];
	}

	class Month extends SvelteComponentDev {
		constructor(options) {
			super(options);

			init(this, options, instance$b, create_fragment$b, safe_not_equal, {
				id: 5,
				visibleMonth: 0,
				selected: 1,
				highlighted: 2,
				shouldShakeDate: 3
			});

			dispatch_dev("SvelteRegisterComponent", {
				component: this,
				tagName: "Month",
				options,
				id: create_fragment$b.name
			});

			const { ctx } = this.$$;
			const props = options.props || {};

			if (/*id*/ ctx[5] === undefined && !('id' in props)) {
				console.warn("<Month> was created without expected prop 'id'");
			}

			if (/*visibleMonth*/ ctx[0] === undefined && !('visibleMonth' in props)) {
				console.warn("<Month> was created without expected prop 'visibleMonth'");
			}

			if (/*selected*/ ctx[1] === undefined && !('selected' in props)) {
				console.warn("<Month> was created without expected prop 'selected'");
			}

			if (/*highlighted*/ ctx[2] === undefined && !('highlighted' in props)) {
				console.warn("<Month> was created without expected prop 'highlighted'");
			}

			if (/*shouldShakeDate*/ ctx[3] === undefined && !('shouldShakeDate' in props)) {
				console.warn("<Month> was created without expected prop 'shouldShakeDate'");
			}
		}

		get id() {
			throw new Error("<Month>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set id(value) {
			throw new Error("<Month>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get visibleMonth() {
			throw new Error("<Month>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set visibleMonth(value) {
			throw new Error("<Month>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get selected() {
			throw new Error("<Month>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set selected(value) {
			throw new Error("<Month>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get highlighted() {
			throw new Error("<Month>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set highlighted(value) {
			throw new Error("<Month>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get shouldShakeDate() {
			throw new Error("<Month>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set shouldShakeDate(value) {
			throw new Error("<Month>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}
	}

	/* node_modules/svelte-calendar/src/Components/NavBar.svelte generated by Svelte v3.42.3 */

	const { Object: Object_1 } = globals;
	const file$a = "node_modules/svelte-calendar/src/Components/NavBar.svelte";

	function get_each_context$1(ctx, list, i) {
		const child_ctx = ctx.slice();
		child_ctx[15] = list[i];
		child_ctx[17] = i;
		return child_ctx;
	}

	// (64:4) {#each availableMonths as monthDefinition, index}
	function create_each_block$1(ctx) {
		let div;
		let span;
		let t0_value = /*monthDefinition*/ ctx[15].abbrev + "";
		let t0;
		let t1;
		let mounted;
		let dispose;

		function click_handler_2(...args) {
			return /*click_handler_2*/ ctx[14](/*monthDefinition*/ ctx[15], /*index*/ ctx[17], ...args);
		}

		const block = {
			c: function create() {
				div = element("div");
				span = element("span");
				t0 = text(t0_value);
				t1 = space();
				attr_dev(span, "class", "svelte-1clago0");
				add_location(span, file$a, 70, 8, 1952);
				attr_dev(div, "class", "month-selector--month svelte-1clago0");
				toggle_class(div, "selected", /*index*/ ctx[17] === /*month*/ ctx[0]);
				toggle_class(div, "selectable", /*monthDefinition*/ ctx[15].selectable);
				add_location(div, file$a, 64, 6, 1721);
			},
			m: function mount(target, anchor) {
				insert_dev(target, div, anchor);
				append_dev(div, span);
				append_dev(span, t0);
				append_dev(div, t1);

				if (!mounted) {
					dispose = listen_dev(div, "click", click_handler_2, false, false, false);
					mounted = true;
				}
			},
			p: function update(new_ctx, dirty) {
				ctx = new_ctx;
				if (dirty & /*availableMonths*/ 64 && t0_value !== (t0_value = /*monthDefinition*/ ctx[15].abbrev + "")) set_data_dev(t0, t0_value);

				if (dirty & /*month*/ 1) {
					toggle_class(div, "selected", /*index*/ ctx[17] === /*month*/ ctx[0]);
				}

				if (dirty & /*availableMonths*/ 64) {
					toggle_class(div, "selectable", /*monthDefinition*/ ctx[15].selectable);
				}
			},
			d: function destroy(detaching) {
				if (detaching) detach_dev(div);
				mounted = false;
				dispose();
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_each_block$1.name,
			type: "each",
			source: "(64:4) {#each availableMonths as monthDefinition, index}",
			ctx
		});

		return block;
	}

	function create_fragment$a(ctx) {
		let div5;
		let div3;
		let div0;
		let i0;
		let t0;
		let div1;
		let t1_value = /*monthsOfYear*/ ctx[4][/*month*/ ctx[0]][0] + "";
		let t1;
		let t2;
		let t3;
		let t4;
		let div2;
		let i1;
		let t5;
		let div4;
		let mounted;
		let dispose;
		let each_value = /*availableMonths*/ ctx[6];
		validate_each_argument(each_value);
		let each_blocks = [];

		for (let i = 0; i < each_value.length; i += 1) {
			each_blocks[i] = create_each_block$1(get_each_context$1(ctx, each_value, i));
		}

		const block = {
			c: function create() {
				div5 = element("div");
				div3 = element("div");
				div0 = element("div");
				i0 = element("i");
				t0 = space();
				div1 = element("div");
				t1 = text(t1_value);
				t2 = space();
				t3 = text(/*year*/ ctx[1]);
				t4 = space();
				div2 = element("div");
				i1 = element("i");
				t5 = space();
				div4 = element("div");

				for (let i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].c();
				}

				attr_dev(i0, "class", "arrow left svelte-1clago0");
				add_location(i0, file$a, 51, 6, 1279);
				attr_dev(div0, "class", "control svelte-1clago0");
				toggle_class(div0, "enabled", /*canDecrementMonth*/ ctx[3]);
				add_location(div0, file$a, 48, 4, 1156);
				attr_dev(div1, "class", "label svelte-1clago0");
				add_location(div1, file$a, 53, 4, 1321);
				attr_dev(i1, "class", "arrow right svelte-1clago0");
				add_location(i1, file$a, 59, 6, 1551);
				attr_dev(div2, "class", "control svelte-1clago0");
				toggle_class(div2, "enabled", /*canIncrementMonth*/ ctx[2]);
				add_location(div2, file$a, 56, 4, 1430);
				attr_dev(div3, "class", "heading-section svelte-1clago0");
				add_location(div3, file$a, 47, 2, 1122);
				attr_dev(div4, "class", "month-selector svelte-1clago0");
				toggle_class(div4, "open", /*monthSelectorOpen*/ ctx[5]);
				add_location(div4, file$a, 62, 2, 1601);
				attr_dev(div5, "class", "title");
				add_location(div5, file$a, 46, 0, 1100);
			},
			l: function claim(nodes) {
				throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			},
			m: function mount(target, anchor) {
				insert_dev(target, div5, anchor);
				append_dev(div5, div3);
				append_dev(div3, div0);
				append_dev(div0, i0);
				append_dev(div3, t0);
				append_dev(div3, div1);
				append_dev(div1, t1);
				append_dev(div1, t2);
				append_dev(div1, t3);
				append_dev(div3, t4);
				append_dev(div3, div2);
				append_dev(div2, i1);
				append_dev(div5, t5);
				append_dev(div5, div4);

				for (let i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].m(div4, null);
				}

				if (!mounted) {
					dispose = [
						listen_dev(div0, "click", /*click_handler*/ ctx[12], false, false, false),
						listen_dev(div1, "click", /*toggleMonthSelectorOpen*/ ctx[8], false, false, false),
						listen_dev(div2, "click", /*click_handler_1*/ ctx[13], false, false, false)
					];

					mounted = true;
				}
			},
			p: function update(ctx, [dirty]) {
				if (dirty & /*canDecrementMonth*/ 8) {
					toggle_class(div0, "enabled", /*canDecrementMonth*/ ctx[3]);
				}

				if (dirty & /*monthsOfYear, month*/ 17 && t1_value !== (t1_value = /*monthsOfYear*/ ctx[4][/*month*/ ctx[0]][0] + "")) set_data_dev(t1, t1_value);
				if (dirty & /*year*/ 2) set_data_dev(t3, /*year*/ ctx[1]);

				if (dirty & /*canIncrementMonth*/ 4) {
					toggle_class(div2, "enabled", /*canIncrementMonth*/ ctx[2]);
				}

				if (dirty & /*month, availableMonths, monthSelected*/ 577) {
					each_value = /*availableMonths*/ ctx[6];
					validate_each_argument(each_value);
					let i;

					for (i = 0; i < each_value.length; i += 1) {
						const child_ctx = get_each_context$1(ctx, each_value, i);

						if (each_blocks[i]) {
							each_blocks[i].p(child_ctx, dirty);
						} else {
							each_blocks[i] = create_each_block$1(child_ctx);
							each_blocks[i].c();
							each_blocks[i].m(div4, null);
						}
					}

					for (; i < each_blocks.length; i += 1) {
						each_blocks[i].d(1);
					}

					each_blocks.length = each_value.length;
				}

				if (dirty & /*monthSelectorOpen*/ 32) {
					toggle_class(div4, "open", /*monthSelectorOpen*/ ctx[5]);
				}
			},
			i: noop,
			o: noop,
			d: function destroy(detaching) {
				if (detaching) detach_dev(div5);
				destroy_each(each_blocks, detaching);
				mounted = false;
				run_all(dispose);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_fragment$a.name,
			type: "component",
			source: "",
			ctx
		});

		return block;
	}

	function instance$a($$self, $$props, $$invalidate) {
		let { $$slots: slots = {}, $$scope } = $$props;
		validate_slots('NavBar', slots, []);
		const dispatch = createEventDispatcher();
		let { month } = $$props;
		let { year } = $$props;
		let { start } = $$props;
		let { end } = $$props;
		let { canIncrementMonth } = $$props;
		let { canDecrementMonth } = $$props;
		let { monthsOfYear } = $$props;
		let monthSelectorOpen = false;
		let availableMonths;

		function toggleMonthSelectorOpen() {
			$$invalidate(5, monthSelectorOpen = !monthSelectorOpen);
		}

		function monthSelected(event, { m, i }) {
			event.stopPropagation();
			if (!m.selectable) return;
			dispatch('monthSelected', i);
			toggleMonthSelectorOpen();
		}

		const writable_props = [
			'month',
			'year',
			'start',
			'end',
			'canIncrementMonth',
			'canDecrementMonth',
			'monthsOfYear'
		];

		Object_1.keys($$props).forEach(key => {
			if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<NavBar> was created with unknown prop '${key}'`);
		});

		const click_handler = () => dispatch('incrementMonth', -1);
		const click_handler_1 = () => dispatch('incrementMonth', 1);
		const click_handler_2 = (monthDefinition, index, e) => monthSelected(e, { m: monthDefinition, i: index });

		$$self.$$set = $$props => {
			if ('month' in $$props) $$invalidate(0, month = $$props.month);
			if ('year' in $$props) $$invalidate(1, year = $$props.year);
			if ('start' in $$props) $$invalidate(10, start = $$props.start);
			if ('end' in $$props) $$invalidate(11, end = $$props.end);
			if ('canIncrementMonth' in $$props) $$invalidate(2, canIncrementMonth = $$props.canIncrementMonth);
			if ('canDecrementMonth' in $$props) $$invalidate(3, canDecrementMonth = $$props.canDecrementMonth);
			if ('monthsOfYear' in $$props) $$invalidate(4, monthsOfYear = $$props.monthsOfYear);
		};

		$$self.$capture_state = () => ({
			createEventDispatcher,
			dispatch,
			month,
			year,
			start,
			end,
			canIncrementMonth,
			canDecrementMonth,
			monthsOfYear,
			monthSelectorOpen,
			availableMonths,
			toggleMonthSelectorOpen,
			monthSelected
		});

		$$self.$inject_state = $$props => {
			if ('month' in $$props) $$invalidate(0, month = $$props.month);
			if ('year' in $$props) $$invalidate(1, year = $$props.year);
			if ('start' in $$props) $$invalidate(10, start = $$props.start);
			if ('end' in $$props) $$invalidate(11, end = $$props.end);
			if ('canIncrementMonth' in $$props) $$invalidate(2, canIncrementMonth = $$props.canIncrementMonth);
			if ('canDecrementMonth' in $$props) $$invalidate(3, canDecrementMonth = $$props.canDecrementMonth);
			if ('monthsOfYear' in $$props) $$invalidate(4, monthsOfYear = $$props.monthsOfYear);
			if ('monthSelectorOpen' in $$props) $$invalidate(5, monthSelectorOpen = $$props.monthSelectorOpen);
			if ('availableMonths' in $$props) $$invalidate(6, availableMonths = $$props.availableMonths);
		};

		if ($$props && "$$inject" in $$props) {
			$$self.$inject_state($$props.$$inject);
		}

		$$self.$$.update = () => {
			if ($$self.$$.dirty & /*start, year, end, monthsOfYear*/ 3090) {
				{
					let isOnLowerBoundary = start.getFullYear() === year;
					let isOnUpperBoundary = end.getFullYear() === year;

					$$invalidate(6, availableMonths = monthsOfYear.map((m, i) => {
						return Object.assign({}, { name: m[0], abbrev: m[1] }, {
							selectable: !isOnLowerBoundary && !isOnUpperBoundary || (!isOnLowerBoundary || i >= start.getMonth()) && (!isOnUpperBoundary || i <= end.getMonth())
						});
					}));
				}
			}
		};

		return [
			month,
			year,
			canIncrementMonth,
			canDecrementMonth,
			monthsOfYear,
			monthSelectorOpen,
			availableMonths,
			dispatch,
			toggleMonthSelectorOpen,
			monthSelected,
			start,
			end,
			click_handler,
			click_handler_1,
			click_handler_2
		];
	}

	class NavBar extends SvelteComponentDev {
		constructor(options) {
			super(options);

			init(this, options, instance$a, create_fragment$a, safe_not_equal, {
				month: 0,
				year: 1,
				start: 10,
				end: 11,
				canIncrementMonth: 2,
				canDecrementMonth: 3,
				monthsOfYear: 4
			});

			dispatch_dev("SvelteRegisterComponent", {
				component: this,
				tagName: "NavBar",
				options,
				id: create_fragment$a.name
			});

			const { ctx } = this.$$;
			const props = options.props || {};

			if (/*month*/ ctx[0] === undefined && !('month' in props)) {
				console.warn("<NavBar> was created without expected prop 'month'");
			}

			if (/*year*/ ctx[1] === undefined && !('year' in props)) {
				console.warn("<NavBar> was created without expected prop 'year'");
			}

			if (/*start*/ ctx[10] === undefined && !('start' in props)) {
				console.warn("<NavBar> was created without expected prop 'start'");
			}

			if (/*end*/ ctx[11] === undefined && !('end' in props)) {
				console.warn("<NavBar> was created without expected prop 'end'");
			}

			if (/*canIncrementMonth*/ ctx[2] === undefined && !('canIncrementMonth' in props)) {
				console.warn("<NavBar> was created without expected prop 'canIncrementMonth'");
			}

			if (/*canDecrementMonth*/ ctx[3] === undefined && !('canDecrementMonth' in props)) {
				console.warn("<NavBar> was created without expected prop 'canDecrementMonth'");
			}

			if (/*monthsOfYear*/ ctx[4] === undefined && !('monthsOfYear' in props)) {
				console.warn("<NavBar> was created without expected prop 'monthsOfYear'");
			}
		}

		get month() {
			throw new Error("<NavBar>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set month(value) {
			throw new Error("<NavBar>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get year() {
			throw new Error("<NavBar>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set year(value) {
			throw new Error("<NavBar>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get start() {
			throw new Error("<NavBar>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set start(value) {
			throw new Error("<NavBar>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get end() {
			throw new Error("<NavBar>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set end(value) {
			throw new Error("<NavBar>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get canIncrementMonth() {
			throw new Error("<NavBar>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set canIncrementMonth(value) {
			throw new Error("<NavBar>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get canDecrementMonth() {
			throw new Error("<NavBar>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set canDecrementMonth(value) {
			throw new Error("<NavBar>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get monthsOfYear() {
			throw new Error("<NavBar>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set monthsOfYear(value) {
			throw new Error("<NavBar>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}
	}

	/* node_modules/svelte-calendar/src/Components/Popover.svelte generated by Svelte v3.42.3 */

	const { window: window_1 } = globals;
	const file$9 = "node_modules/svelte-calendar/src/Components/Popover.svelte";
	const get_contents_slot_changes = dirty => ({});
	const get_contents_slot_context = ctx => ({});
	const get_trigger_slot_changes = dirty => ({});
	const get_trigger_slot_context = ctx => ({});

	function create_fragment$9(ctx) {
		let div4;
		let div0;
		let t;
		let div3;
		let div2;
		let div1;
		let current;
		let mounted;
		let dispose;
		add_render_callback(/*onwindowresize*/ ctx[14]);
		const trigger_slot_template = /*#slots*/ ctx[13].trigger;
		const trigger_slot = create_slot(trigger_slot_template, ctx, /*$$scope*/ ctx[12], get_trigger_slot_context);
		const contents_slot_template = /*#slots*/ ctx[13].contents;
		const contents_slot = create_slot(contents_slot_template, ctx, /*$$scope*/ ctx[12], get_contents_slot_context);

		const block = {
			c: function create() {
				div4 = element("div");
				div0 = element("div");
				if (trigger_slot) trigger_slot.c();
				t = space();
				div3 = element("div");
				div2 = element("div");
				div1 = element("div");
				if (contents_slot) contents_slot.c();
				attr_dev(div0, "class", "trigger");
				add_location(div0, file$9, 103, 2, 2358);
				attr_dev(div1, "class", "contents-inner svelte-e7yfxp");
				add_location(div1, file$9, 114, 6, 2745);
				attr_dev(div2, "class", "contents svelte-e7yfxp");
				add_location(div2, file$9, 113, 4, 2687);
				attr_dev(div3, "class", "contents-wrapper svelte-e7yfxp");
				set_style(div3, "transform", "translate(-50%,-50%) translate(" + /*translateX*/ ctx[8] + "px, " + /*translateY*/ ctx[7] + "px)");
				toggle_class(div3, "visible", /*open*/ ctx[0]);
				toggle_class(div3, "shrink", /*shrink*/ ctx[1]);
				add_location(div3, file$9, 107, 2, 2476);
				attr_dev(div4, "class", "sc-popover svelte-e7yfxp");
				add_location(div4, file$9, 102, 0, 2311);
			},
			l: function claim(nodes) {
				throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			},
			m: function mount(target, anchor) {
				insert_dev(target, div4, anchor);
				append_dev(div4, div0);

				if (trigger_slot) {
					trigger_slot.m(div0, null);
				}

    			/*div0_binding*/ ctx[15](div0);
				append_dev(div4, t);
				append_dev(div4, div3);
				append_dev(div3, div2);
				append_dev(div2, div1);

				if (contents_slot) {
					contents_slot.m(div1, null);
				}

    			/*div2_binding*/ ctx[16](div2);
    			/*div3_binding*/ ctx[17](div3);
    			/*div4_binding*/ ctx[18](div4);
				current = true;

				if (!mounted) {
					dispose = [
						listen_dev(window_1, "resize", /*onwindowresize*/ ctx[14]),
						listen_dev(div0, "click", /*doOpen*/ ctx[9], false, false, false)
					];

					mounted = true;
				}
			},
			p: function update(ctx, [dirty]) {
				if (trigger_slot) {
					if (trigger_slot.p && (!current || dirty & /*$$scope*/ 4096)) {
						update_slot_base(
							trigger_slot,
							trigger_slot_template,
							ctx,
    						/*$$scope*/ ctx[12],
							!current
								? get_all_dirty_from_scope(/*$$scope*/ ctx[12])
								: get_slot_changes(trigger_slot_template, /*$$scope*/ ctx[12], dirty, get_trigger_slot_changes),
							get_trigger_slot_context
						);
					}
				}

				if (contents_slot) {
					if (contents_slot.p && (!current || dirty & /*$$scope*/ 4096)) {
						update_slot_base(
							contents_slot,
							contents_slot_template,
							ctx,
    						/*$$scope*/ ctx[12],
							!current
								? get_all_dirty_from_scope(/*$$scope*/ ctx[12])
								: get_slot_changes(contents_slot_template, /*$$scope*/ ctx[12], dirty, get_contents_slot_changes),
							get_contents_slot_context
						);
					}
				}

				if (!current || dirty & /*translateX, translateY*/ 384) {
					set_style(div3, "transform", "translate(-50%,-50%) translate(" + /*translateX*/ ctx[8] + "px, " + /*translateY*/ ctx[7] + "px)");
				}

				if (dirty & /*open*/ 1) {
					toggle_class(div3, "visible", /*open*/ ctx[0]);
				}

				if (dirty & /*shrink*/ 2) {
					toggle_class(div3, "shrink", /*shrink*/ ctx[1]);
				}
			},
			i: function intro(local) {
				if (current) return;
				transition_in(trigger_slot, local);
				transition_in(contents_slot, local);
				current = true;
			},
			o: function outro(local) {
				transition_out(trigger_slot, local);
				transition_out(contents_slot, local);
				current = false;
			},
			d: function destroy(detaching) {
				if (detaching) detach_dev(div4);
				if (trigger_slot) trigger_slot.d(detaching);
    			/*div0_binding*/ ctx[15](null);
				if (contents_slot) contents_slot.d(detaching);
    			/*div2_binding*/ ctx[16](null);
    			/*div3_binding*/ ctx[17](null);
    			/*div4_binding*/ ctx[18](null);
				mounted = false;
				run_all(dispose);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_fragment$9.name,
			type: "component",
			source: "",
			ctx
		});

		return block;
	}

	function instance$9($$self, $$props, $$invalidate) {
		let { $$slots: slots = {}, $$scope } = $$props;
		validate_slots('Popover', slots, ['trigger', 'contents']);
		const dispatch = createEventDispatcher();

		let once = (el, evt, cb) => {
			function handler() {
				cb.apply(this, arguments);
				el.removeEventListener(evt, handler);
			}

			el.addEventListener(evt, handler);
		};

		let popover;
		let w;
		let triggerContainer;
		let contentsAnimated;
		let contentsWrapper;
		let translateY = 0;
		let translateX = 0;
		let { open = false } = $$props;
		let { shrink } = $$props;
		let { trigger } = $$props;

		const close = () => {
			$$invalidate(1, shrink = true);

			once(contentsAnimated, 'animationend', () => {
				$$invalidate(1, shrink = false);
				$$invalidate(0, open = false);
				dispatch('closed');
			});
		};

		function checkForFocusLoss(evt) {
			if (!open) return;
			let el = evt.target;

			// eslint-disable-next-line
			do {
				if (el === popover) return;
			} while (el = el.parentNode); // eslint-disable-next-line

			close();
		}

		onMount(() => {
			document.addEventListener('click', checkForFocusLoss);
			if (!trigger) return;
			triggerContainer.appendChild(trigger.parentNode.removeChild(trigger));

			// eslint-disable-next-line
			return () => {
				document.removeEventListener('click', checkForFocusLoss);
			};
		});

		const getDistanceToEdges = async () => {
			if (!open) {
				$$invalidate(0, open = true);
			}

			await tick();
			let rect = contentsWrapper.getBoundingClientRect();

			return {
				top: rect.top + -1 * translateY,
				bottom: window.innerHeight - rect.bottom + translateY,
				left: rect.left + -1 * translateX,
				right: document.body.clientWidth - rect.right + translateX
			};
		};

		const getTranslate = async () => {
			let dist = await getDistanceToEdges();
			let x;
			let y;

			if (w < 480) {
				y = dist.bottom;
			} else if (dist.top < 0) {
				y = Math.abs(dist.top);
			} else if (dist.bottom < 0) {
				y = dist.bottom;
			} else {
				y = 0;
			}

			if (dist.left < 0) {
				x = Math.abs(dist.left);
			} else if (dist.right < 0) {
				x = dist.right;
			} else {
				x = 0;
			}

			return { x, y };
		};

		const doOpen = async () => {
			const { x, y } = await getTranslate();
			$$invalidate(8, translateX = x);
			$$invalidate(7, translateY = y);
			$$invalidate(0, open = true);
			dispatch('opened');
		};

		const writable_props = ['open', 'shrink', 'trigger'];

		Object.keys($$props).forEach(key => {
			if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Popover> was created with unknown prop '${key}'`);
		});

		function onwindowresize() {
			$$invalidate(3, w = window_1.innerWidth);
		}

		function div0_binding($$value) {
			binding_callbacks[$$value ? 'unshift' : 'push'](() => {
				triggerContainer = $$value;
				$$invalidate(4, triggerContainer);
			});
		}

		function div2_binding($$value) {
			binding_callbacks[$$value ? 'unshift' : 'push'](() => {
				contentsAnimated = $$value;
				$$invalidate(5, contentsAnimated);
			});
		}

		function div3_binding($$value) {
			binding_callbacks[$$value ? 'unshift' : 'push'](() => {
				contentsWrapper = $$value;
				$$invalidate(6, contentsWrapper);
			});
		}

		function div4_binding($$value) {
			binding_callbacks[$$value ? 'unshift' : 'push'](() => {
				popover = $$value;
				$$invalidate(2, popover);
			});
		}

		$$self.$$set = $$props => {
			if ('open' in $$props) $$invalidate(0, open = $$props.open);
			if ('shrink' in $$props) $$invalidate(1, shrink = $$props.shrink);
			if ('trigger' in $$props) $$invalidate(10, trigger = $$props.trigger);
			if ('$$scope' in $$props) $$invalidate(12, $$scope = $$props.$$scope);
		};

		$$self.$capture_state = () => ({
			onMount,
			createEventDispatcher,
			tick,
			dispatch,
			once,
			popover,
			w,
			triggerContainer,
			contentsAnimated,
			contentsWrapper,
			translateY,
			translateX,
			open,
			shrink,
			trigger,
			close,
			checkForFocusLoss,
			getDistanceToEdges,
			getTranslate,
			doOpen
		});

		$$self.$inject_state = $$props => {
			if ('once' in $$props) once = $$props.once;
			if ('popover' in $$props) $$invalidate(2, popover = $$props.popover);
			if ('w' in $$props) $$invalidate(3, w = $$props.w);
			if ('triggerContainer' in $$props) $$invalidate(4, triggerContainer = $$props.triggerContainer);
			if ('contentsAnimated' in $$props) $$invalidate(5, contentsAnimated = $$props.contentsAnimated);
			if ('contentsWrapper' in $$props) $$invalidate(6, contentsWrapper = $$props.contentsWrapper);
			if ('translateY' in $$props) $$invalidate(7, translateY = $$props.translateY);
			if ('translateX' in $$props) $$invalidate(8, translateX = $$props.translateX);
			if ('open' in $$props) $$invalidate(0, open = $$props.open);
			if ('shrink' in $$props) $$invalidate(1, shrink = $$props.shrink);
			if ('trigger' in $$props) $$invalidate(10, trigger = $$props.trigger);
		};

		if ($$props && "$$inject" in $$props) {
			$$self.$inject_state($$props.$$inject);
		}

		return [
			open,
			shrink,
			popover,
			w,
			triggerContainer,
			contentsAnimated,
			contentsWrapper,
			translateY,
			translateX,
			doOpen,
			trigger,
			close,
			$$scope,
			slots,
			onwindowresize,
			div0_binding,
			div2_binding,
			div3_binding,
			div4_binding
		];
	}

	class Popover extends SvelteComponentDev {
		constructor(options) {
			super(options);

			init(this, options, instance$9, create_fragment$9, safe_not_equal, {
				open: 0,
				shrink: 1,
				trigger: 10,
				close: 11
			});

			dispatch_dev("SvelteRegisterComponent", {
				component: this,
				tagName: "Popover",
				options,
				id: create_fragment$9.name
			});

			const { ctx } = this.$$;
			const props = options.props || {};

			if (/*shrink*/ ctx[1] === undefined && !('shrink' in props)) {
				console.warn("<Popover> was created without expected prop 'shrink'");
			}

			if (/*trigger*/ ctx[10] === undefined && !('trigger' in props)) {
				console.warn("<Popover> was created without expected prop 'trigger'");
			}
		}

		get open() {
			throw new Error("<Popover>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set open(value) {
			throw new Error("<Popover>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get shrink() {
			throw new Error("<Popover>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set shrink(value) {
			throw new Error("<Popover>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get trigger() {
			throw new Error("<Popover>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set trigger(value) {
			throw new Error("<Popover>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get close() {
			return this.$$.ctx[11];
		}

		set close(value) {
			throw new Error("<Popover>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}
	}

	/**
	 * generic function to inject data into token-laden string
	 * @param str {String} Required
	 * @param name {String} Required
	 * @param value {String|Integer} Required
	 * @returns {String}
	 *
	 * @example
	 * injectStringData("The following is a token: #{tokenName}", "tokenName", 123); 
	 * @returns {String} "The following is a token: 123"
	 *
	 */
	const injectStringData = (str, name, value) => str
		.replace(new RegExp('#{' + name + '}', 'g'), value);

	/**
	 * Generic function to enforce length of string. 
	 * 
	 * Pass a string or number to this function and specify the desired length.
	 * This function will either pad the # with leading 0's (if str.length < length)
	 * or remove data from the end (@fromBack==false) or beginning (@fromBack==true)
	 * of the string when str.length > length.
	 *
	 * When length == str.length or typeof length == 'undefined', this function
	 * returns the original @str parameter.
	 * 
	 * @param str {String} Required
	 * @param length {Integer} Required
	 * @param fromBack {Boolean} Optional
	 * @returns {String}
	 *
	 */
	const enforceLength = function (str, length, fromBack) {
		str = str.toString();
		if (typeof length == 'undefined') return str;
		if (str.length == length) return str;
		fromBack = (typeof fromBack == 'undefined') ? false : fromBack;
		if (str.length < length) {
			// pad the beginning of the string w/ enough 0's to reach desired length:
			while (length - str.length > 0) str = '0' + str;
		} else if (str.length > length) {
			if (fromBack) {
				// grab the desired #/chars from end of string: ex: '2015' -> '15'
				str = str.substring(str.length - length);
			} else {
				// grab the desired #/chars from beginning of string: ex: '2015' -> '20'
				str = str.substring(0, length);
			}
		}
		return str;
	};

	const daysOfWeek = [
		['Sunday', 'Sun'],
		['Monday', 'Mon'],
		['Tuesday', 'Tue'],
		['Wednesday', 'Wed'],
		['Thursday', 'Thu'],
		['Friday', 'Fri'],
		['Saturday', 'Sat']
	];

	const monthsOfYear = [
		['January', 'Jan'],
		['February', 'Feb'],
		['March', 'Mar'],
		['April', 'Apr'],
		['May', 'May'],
		['June', 'Jun'],
		['July', 'Jul'],
		['August', 'Aug'],
		['September', 'Sep'],
		['October', 'Oct'],
		['November', 'Nov'],
		['December', 'Dec']
	];

	let dictionary = {
		daysOfWeek,
		monthsOfYear
	};

	const extendDictionary = (conf) =>
		Object.keys(conf).forEach(key => {
			if (dictionary[key] && dictionary[key].length == conf[key].length) {
				dictionary[key] = conf[key];
			}
		});

	var acceptedDateTokens = [
		{
			// d: day of the month, 2 digits with leading zeros:
			key: 'd',
			method: function (date) { return enforceLength(date.getDate(), 2); }
		}, {
			// D: textual representation of day, 3 letters: Sun thru Sat
			key: 'D',
			method: function (date) { return dictionary.daysOfWeek[date.getDay()][1]; }
		}, {
			// j: day of month without leading 0's
			key: 'j',
			method: function (date) { return date.getDate(); }
		}, {
			// l: full textual representation of day of week: Sunday thru Saturday
			key: 'l',
			method: function (date) { return dictionary.daysOfWeek[date.getDay()][0]; }
		}, {
			// F: full text month: 'January' thru 'December'
			key: 'F',
			method: function (date) { return dictionary.monthsOfYear[date.getMonth()][0]; }
		}, {
			// m: 2 digit numeric month: '01' - '12':
			key: 'm',
			method: function (date) { return enforceLength(date.getMonth() + 1, 2); }
		}, {
			// M: a short textual representation of the month, 3 letters: 'Jan' - 'Dec'
			key: 'M',
			method: function (date) { return dictionary.monthsOfYear[date.getMonth()][1]; }
		}, {
			// n: numeric represetation of month w/o leading 0's, '1' - '12':
			key: 'n',
			method: function (date) { return date.getMonth() + 1; }
		}, {
			// Y: Full numeric year, 4 digits
			key: 'Y',
			method: function (date) { return date.getFullYear(); }
		}, {
			// y: 2 digit numeric year:
			key: 'y',
			method: function (date) { return enforceLength(date.getFullYear(), 2, true); }
		}
	];

	var acceptedTimeTokens = [
		{
			// a: lowercase ante meridiem and post meridiem 'am' or 'pm'
			key: 'a',
			method: function (date) { return (date.getHours() > 11) ? 'pm' : 'am'; }
		}, {
			// A: uppercase ante merdiiem and post meridiem 'AM' or 'PM'
			key: 'A',
			method: function (date) { return (date.getHours() > 11) ? 'PM' : 'AM'; }
		}, {
			// g: 12-hour format of an hour without leading zeros 1-12
			key: 'g',
			method: function (date) { return date.getHours() % 12 || 12; }
		}, {
			// G: 24-hour format of an hour without leading zeros 0-23
			key: 'G',
			method: function (date) { return date.getHours(); }
		}, {
			// h: 12-hour format of an hour with leading zeros 01-12
			key: 'h',
			method: function (date) { return enforceLength(date.getHours() % 12 || 12, 2); }
		}, {
			// H: 24-hour format of an hour with leading zeros: 00-23
			key: 'H',
			method: function (date) { return enforceLength(date.getHours(), 2); }
		}, {
			// i: Minutes with leading zeros 00-59
			key: 'i',
			method: function (date) { return enforceLength(date.getMinutes(), 2); }
		}, {
			// s: Seconds with leading zeros 00-59
			key: 's',
			method: function (date) { return enforceLength(date.getSeconds(), 2); }
		}
	];

	/**
	 * Internationalization object for timeUtils.internationalize().
	 * @typedef internationalizeObj
	 * @property {Array} [daysOfWeek=[ 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday' ]] daysOfWeek Weekday labels as strings, starting with Sunday.
	 * @property {Array} [monthsOfYear=[ 'January','February','March','April','May','June','July','August','September','October','November','December' ]] monthsOfYear Month labels as strings, starting with January.
	 */

	/**
	 * This function can be used to support additional languages by passing an object with 
	 * `daysOfWeek` and `monthsOfYear` attributes.  Each attribute should be an array of
	 * strings (ex: `daysOfWeek: ['monday', 'tuesday', 'wednesday'...]`)
	 *
	 * @param {internationalizeObj} conf
	 */
	const internationalize = (conf = {}) => {
		extendDictionary(conf);
	};

	/**
	 * generic formatDate function which accepts dynamic templates
	 * @param date {Date} Required
	 * @param template {String} Optional
	 * @returns {String}
	 *
	 * @example
	 * formatDate(new Date(), '#{M}. #{j}, #{Y}')
	 * @returns {Number} Returns a formatted date
	 *
	 */
	const formatDate = (date, template = '#{m}/#{d}/#{Y}') => {
		acceptedDateTokens.forEach(token => {
			if (template.indexOf(`#{${token.key}}`) == -1) return;
			template = injectStringData(template, token.key, token.method(date));
		});
		acceptedTimeTokens.forEach(token => {
			if (template.indexOf(`#{${token.key}}`) == -1) return;
			template = injectStringData(template, token.key, token.method(date));
		});
		return template;
	};

	const keyCodes = {
		left: 37,
		up: 38,
		right: 39,
		down: 40,
		pgup: 33,
		pgdown: 34,
		enter: 13,
		escape: 27,
		tab: 9
	};

	const keyCodesArray = Object.keys(keyCodes).map(k => keyCodes[k]);

	/* node_modules/svelte-calendar/src/Components/Datepicker.svelte generated by Svelte v3.42.3 */
	const file$8 = "node_modules/svelte-calendar/src/Components/Datepicker.svelte";

	const get_default_slot_changes$1 = dirty => ({
		selected: dirty[0] & /*selected*/ 1,
		formattedSelected: dirty[0] & /*formattedSelected*/ 4
	});

	const get_default_slot_context$1 = ctx => ({
		selected: /*selected*/ ctx[0],
		formattedSelected: /*formattedSelected*/ ctx[2]
	});

	function get_each_context(ctx, list, i) {
		const child_ctx = ctx.slice();
		child_ctx[63] = list[i];
		return child_ctx;
	}

	// (277:8) {#if !trigger}
	function create_if_block$5(ctx) {
		let button;
		let t;

		const block = {
			c: function create() {
				button = element("button");
				t = text(/*formattedSelected*/ ctx[2]);
				attr_dev(button, "class", "calendar-button svelte-1p5ghly");
				attr_dev(button, "type", "button");
				add_location(button, file$8, 277, 8, 7754);
			},
			m: function mount(target, anchor) {
				insert_dev(target, button, anchor);
				append_dev(button, t);
			},
			p: function update(ctx, dirty) {
				if (dirty[0] & /*formattedSelected*/ 4) set_data_dev(t, /*formattedSelected*/ ctx[2]);
			},
			d: function destroy(detaching) {
				if (detaching) detach_dev(button);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_if_block$5.name,
			type: "if",
			source: "(277:8) {#if !trigger}",
			ctx
		});

		return block;
	}

	// (276:43)          
	function fallback_block(ctx) {
		let if_block_anchor;
		let if_block = !/*trigger*/ ctx[1] && create_if_block$5(ctx);

		const block = {
			c: function create() {
				if (if_block) if_block.c();
				if_block_anchor = empty();
			},
			m: function mount(target, anchor) {
				if (if_block) if_block.m(target, anchor);
				insert_dev(target, if_block_anchor, anchor);
			},
			p: function update(ctx, dirty) {
				if (!/*trigger*/ ctx[1]) {
					if (if_block) {
						if_block.p(ctx, dirty);
					} else {
						if_block = create_if_block$5(ctx);
						if_block.c();
						if_block.m(if_block_anchor.parentNode, if_block_anchor);
					}
				} else if (if_block) {
					if_block.d(1);
					if_block = null;
				}
			},
			d: function destroy(detaching) {
				if (if_block) if_block.d(detaching);
				if (detaching) detach_dev(if_block_anchor);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: fallback_block.name,
			type: "fallback",
			source: "(276:43)          ",
			ctx
		});

		return block;
	}

	// (275:4) 
	function create_trigger_slot(ctx) {
		let div;
		let current;
		const default_slot_template = /*#slots*/ ctx[40].default;
		const default_slot = create_slot(default_slot_template, ctx, /*$$scope*/ ctx[47], get_default_slot_context$1);
		const default_slot_or_fallback = default_slot || fallback_block(ctx);

		const block = {
			c: function create() {
				div = element("div");
				if (default_slot_or_fallback) default_slot_or_fallback.c();
				attr_dev(div, "slot", "trigger");
				attr_dev(div, "class", "svelte-1p5ghly");
				add_location(div, file$8, 274, 4, 7658);
			},
			m: function mount(target, anchor) {
				insert_dev(target, div, anchor);

				if (default_slot_or_fallback) {
					default_slot_or_fallback.m(div, null);
				}

				current = true;
			},
			p: function update(ctx, dirty) {
				if (default_slot) {
					if (default_slot.p && (!current || dirty[0] & /*selected, formattedSelected*/ 5 | dirty[1] & /*$$scope*/ 65536)) {
						update_slot_base(
							default_slot,
							default_slot_template,
							ctx,
    						/*$$scope*/ ctx[47],
							!current
								? get_all_dirty_from_scope(/*$$scope*/ ctx[47])
								: get_slot_changes(default_slot_template, /*$$scope*/ ctx[47], dirty, get_default_slot_changes$1),
							get_default_slot_context$1
						);
					}
				} else {
					if (default_slot_or_fallback && default_slot_or_fallback.p && (!current || dirty[0] & /*formattedSelected, trigger*/ 6)) {
						default_slot_or_fallback.p(ctx, !current ? [-1, -1, -1] : dirty);
					}
				}
			},
			i: function intro(local) {
				if (current) return;
				transition_in(default_slot_or_fallback, local);
				current = true;
			},
			o: function outro(local) {
				transition_out(default_slot_or_fallback, local);
				current = false;
			},
			d: function destroy(detaching) {
				if (detaching) detach_dev(div);
				if (default_slot_or_fallback) default_slot_or_fallback.d(detaching);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_trigger_slot.name,
			type: "slot",
			source: "(275:4) ",
			ctx
		});

		return block;
	}

	// (298:10) {#each sortedDaysOfWeek as day}
	function create_each_block(ctx) {
		let span;
		let t_value = /*day*/ ctx[63][1] + "";
		let t;

		const block = {
			c: function create() {
				span = element("span");
				t = text(t_value);
				attr_dev(span, "class", "svelte-1p5ghly");
				add_location(span, file$8, 298, 10, 8321);
			},
			m: function mount(target, anchor) {
				insert_dev(target, span, anchor);
				append_dev(span, t);
			},
			p: noop,
			d: function destroy(detaching) {
				if (detaching) detach_dev(span);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_each_block.name,
			type: "each",
			source: "(298:10) {#each sortedDaysOfWeek as day}",
			ctx
		});

		return block;
	}

	// (284:4) 
	function create_contents_slot(ctx) {
		let div2;
		let div1;
		let navbar;
		let t0;
		let div0;
		let t1;
		let month_1;
		let current;

		navbar = new NavBar({
			props: {
				month: /*month*/ ctx[6],
				year: /*year*/ ctx[7],
				canIncrementMonth: /*canIncrementMonth*/ ctx[15],
				canDecrementMonth: /*canDecrementMonth*/ ctx[14],
				start: /*start*/ ctx[3],
				end: /*end*/ ctx[4],
				monthsOfYear: /*monthsOfYear*/ ctx[5]
			},
			$$inline: true
		});

		navbar.$on("monthSelected", /*monthSelected_handler*/ ctx[41]);
		navbar.$on("incrementMonth", /*incrementMonth_handler*/ ctx[42]);
		let each_value = /*sortedDaysOfWeek*/ ctx[18];
		validate_each_argument(each_value);
		let each_blocks = [];

		for (let i = 0; i < each_value.length; i += 1) {
			each_blocks[i] = create_each_block(get_each_context(ctx, each_value, i));
		}

		month_1 = new Month({
			props: {
				visibleMonth: /*visibleMonth*/ ctx[8],
				selected: /*selected*/ ctx[0],
				highlighted: /*highlighted*/ ctx[10],
				shouldShakeDate: /*shouldShakeDate*/ ctx[11],
				id: /*visibleMonthId*/ ctx[17]
			},
			$$inline: true
		});

		month_1.$on("dateSelected", /*dateSelected_handler*/ ctx[43]);

		const block = {
			c: function create() {
				div2 = element("div");
				div1 = element("div");
				create_component(navbar.$$.fragment);
				t0 = space();
				div0 = element("div");

				for (let i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].c();
				}

				t1 = space();
				create_component(month_1.$$.fragment);
				attr_dev(div0, "class", "legend svelte-1p5ghly");
				add_location(div0, file$8, 296, 8, 8248);
				attr_dev(div1, "class", "calendar svelte-1p5ghly");
				add_location(div1, file$8, 284, 6, 7920);
				attr_dev(div2, "slot", "contents");
				attr_dev(div2, "class", "svelte-1p5ghly");
				add_location(div2, file$8, 283, 4, 7892);
			},
			m: function mount(target, anchor) {
				insert_dev(target, div2, anchor);
				append_dev(div2, div1);
				mount_component(navbar, div1, null);
				append_dev(div1, t0);
				append_dev(div1, div0);

				for (let i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].m(div0, null);
				}

				append_dev(div1, t1);
				mount_component(month_1, div1, null);
				current = true;
			},
			p: function update(ctx, dirty) {
				const navbar_changes = {};
				if (dirty[0] & /*month*/ 64) navbar_changes.month = /*month*/ ctx[6];
				if (dirty[0] & /*year*/ 128) navbar_changes.year = /*year*/ ctx[7];
				if (dirty[0] & /*canIncrementMonth*/ 32768) navbar_changes.canIncrementMonth = /*canIncrementMonth*/ ctx[15];
				if (dirty[0] & /*canDecrementMonth*/ 16384) navbar_changes.canDecrementMonth = /*canDecrementMonth*/ ctx[14];
				if (dirty[0] & /*start*/ 8) navbar_changes.start = /*start*/ ctx[3];
				if (dirty[0] & /*end*/ 16) navbar_changes.end = /*end*/ ctx[4];
				if (dirty[0] & /*monthsOfYear*/ 32) navbar_changes.monthsOfYear = /*monthsOfYear*/ ctx[5];
				navbar.$set(navbar_changes);

				if (dirty[0] & /*sortedDaysOfWeek*/ 262144) {
					each_value = /*sortedDaysOfWeek*/ ctx[18];
					validate_each_argument(each_value);
					let i;

					for (i = 0; i < each_value.length; i += 1) {
						const child_ctx = get_each_context(ctx, each_value, i);

						if (each_blocks[i]) {
							each_blocks[i].p(child_ctx, dirty);
						} else {
							each_blocks[i] = create_each_block(child_ctx);
							each_blocks[i].c();
							each_blocks[i].m(div0, null);
						}
					}

					for (; i < each_blocks.length; i += 1) {
						each_blocks[i].d(1);
					}

					each_blocks.length = each_value.length;
				}

				const month_1_changes = {};
				if (dirty[0] & /*visibleMonth*/ 256) month_1_changes.visibleMonth = /*visibleMonth*/ ctx[8];
				if (dirty[0] & /*selected*/ 1) month_1_changes.selected = /*selected*/ ctx[0];
				if (dirty[0] & /*highlighted*/ 1024) month_1_changes.highlighted = /*highlighted*/ ctx[10];
				if (dirty[0] & /*shouldShakeDate*/ 2048) month_1_changes.shouldShakeDate = /*shouldShakeDate*/ ctx[11];
				if (dirty[0] & /*visibleMonthId*/ 131072) month_1_changes.id = /*visibleMonthId*/ ctx[17];
				month_1.$set(month_1_changes);
			},
			i: function intro(local) {
				if (current) return;
				transition_in(navbar.$$.fragment, local);
				transition_in(month_1.$$.fragment, local);
				current = true;
			},
			o: function outro(local) {
				transition_out(navbar.$$.fragment, local);
				transition_out(month_1.$$.fragment, local);
				current = false;
			},
			d: function destroy(detaching) {
				if (detaching) detach_dev(div2);
				destroy_component(navbar);
				destroy_each(each_blocks, detaching);
				destroy_component(month_1);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_contents_slot.name,
			type: "slot",
			source: "(284:4) ",
			ctx
		});

		return block;
	}

	function create_fragment$8(ctx) {
		let div;
		let popover_1;
		let updating_open;
		let updating_shrink;
		let current;

		function popover_1_open_binding(value) {
    		/*popover_1_open_binding*/ ctx[45](value);
		}

		function popover_1_shrink_binding(value) {
    		/*popover_1_shrink_binding*/ ctx[46](value);
		}

		let popover_1_props = {
			trigger: /*trigger*/ ctx[1],
			$$slots: {
				contents: [create_contents_slot],
				trigger: [create_trigger_slot]
			},
			$$scope: { ctx }
		};

		if (/*isOpen*/ ctx[12] !== void 0) {
			popover_1_props.open = /*isOpen*/ ctx[12];
		}

		if (/*isClosing*/ ctx[13] !== void 0) {
			popover_1_props.shrink = /*isClosing*/ ctx[13];
		}

		popover_1 = new Popover({ props: popover_1_props, $$inline: true });
    	/*popover_1_binding*/ ctx[44](popover_1);
		binding_callbacks.push(() => bind$1(popover_1, 'open', popover_1_open_binding));
		binding_callbacks.push(() => bind$1(popover_1, 'shrink', popover_1_shrink_binding));
		popover_1.$on("opened", /*registerOpen*/ ctx[23]);
		popover_1.$on("closed", /*registerClose*/ ctx[22]);

		const block = {
			c: function create() {
				div = element("div");
				create_component(popover_1.$$.fragment);
				attr_dev(div, "class", "datepicker svelte-1p5ghly");
				attr_dev(div, "style", /*wrapperStyle*/ ctx[16]);
				toggle_class(div, "open", /*isOpen*/ ctx[12]);
				toggle_class(div, "closing", /*isClosing*/ ctx[13]);
				add_location(div, file$8, 260, 0, 7376);
			},
			l: function claim(nodes) {
				throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			},
			m: function mount(target, anchor) {
				insert_dev(target, div, anchor);
				mount_component(popover_1, div, null);
				current = true;
			},
			p: function update(ctx, dirty) {
				const popover_1_changes = {};
				if (dirty[0] & /*trigger*/ 2) popover_1_changes.trigger = /*trigger*/ ctx[1];

				if (dirty[0] & /*visibleMonth, selected, highlighted, shouldShakeDate, visibleMonthId, month, year, canIncrementMonth, canDecrementMonth, start, end, monthsOfYear, formattedSelected, trigger*/ 183807 | dirty[1] & /*$$scope*/ 65536) {
					popover_1_changes.$$scope = { dirty, ctx };
				}

				if (!updating_open && dirty[0] & /*isOpen*/ 4096) {
					updating_open = true;
					popover_1_changes.open = /*isOpen*/ ctx[12];
					add_flush_callback(() => updating_open = false);
				}

				if (!updating_shrink && dirty[0] & /*isClosing*/ 8192) {
					updating_shrink = true;
					popover_1_changes.shrink = /*isClosing*/ ctx[13];
					add_flush_callback(() => updating_shrink = false);
				}

				popover_1.$set(popover_1_changes);

				if (!current || dirty[0] & /*wrapperStyle*/ 65536) {
					attr_dev(div, "style", /*wrapperStyle*/ ctx[16]);
				}

				if (dirty[0] & /*isOpen*/ 4096) {
					toggle_class(div, "open", /*isOpen*/ ctx[12]);
				}

				if (dirty[0] & /*isClosing*/ 8192) {
					toggle_class(div, "closing", /*isClosing*/ ctx[13]);
				}
			},
			i: function intro(local) {
				if (current) return;
				transition_in(popover_1.$$.fragment, local);
				current = true;
			},
			o: function outro(local) {
				transition_out(popover_1.$$.fragment, local);
				current = false;
			},
			d: function destroy(detaching) {
				if (detaching) detach_dev(div);
    			/*popover_1_binding*/ ctx[44](null);
				destroy_component(popover_1);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_fragment$8.name,
			type: "component",
			source: "",
			ctx
		});

		return block;
	}

	function instance$8($$self, $$props, $$invalidate) {
		let months;
		let visibleMonth;
		let visibleMonthId;
		let lastVisibleDate;
		let firstVisibleDate;
		let canIncrementMonth;
		let canDecrementMonth;
		let wrapperStyle;
		let { $$slots: slots = {}, $$scope } = $$props;
		validate_slots('Datepicker', slots, ['default']);
		const dispatch = createEventDispatcher();
		const today = new Date();
		const oneYear = 1000 * 60 * 60 * 24 * 365;
		let popover;
		let { format = '#{m}/#{d}/#{Y}' } = $$props;
		let { start = new Date(Date.now() - oneYear) } = $$props;
		let { end = new Date(Date.now() + oneYear) } = $$props;
		let { selected = today } = $$props;
		let { dateChosen = false } = $$props;
		let { trigger = null } = $$props;
		let { selectableCallback = null } = $$props;
		let { weekStart = 0 } = $$props;

		let { daysOfWeek = [
			['Sunday', 'Sun'],
			['Monday', 'Mon'],
			['Tuesday', 'Tue'],
			['Wednesday', 'Wed'],
			['Thursday', 'Thu'],
			['Friday', 'Fri'],
			['Saturday', 'Sat']
		] } = $$props;

		let { monthsOfYear = [
			['January', 'Jan'],
			['February', 'Feb'],
			['March', 'Mar'],
			['April', 'Apr'],
			['May', 'May'],
			['June', 'Jun'],
			['July', 'Jul'],
			['August', 'Aug'],
			['September', 'Sep'],
			['October', 'Oct'],
			['November', 'Nov'],
			['December', 'Dec']
		] } = $$props;

		selected = selected.getTime() < start.getTime() || selected.getTime() > end.getTime()
			? start
			: selected;

		let { style = '' } = $$props;
		let { buttonBackgroundColor = '#fff' } = $$props;
		let { buttonBorderColor = '#eee' } = $$props;
		let { buttonTextColor = '#333' } = $$props;
		let { highlightColor = '#f7901e' } = $$props;
		let { dayBackgroundColor = 'none' } = $$props;
		let { dayTextColor = '#4a4a4a' } = $$props;
		let { dayHighlightedBackgroundColor = '#efefef' } = $$props;
		let { dayHighlightedTextColor = '#4a4a4a' } = $$props;
		internationalize({ daysOfWeek, monthsOfYear });

		let sortedDaysOfWeek = weekStart === 0
			? daysOfWeek
			: (() => {
				let dow = daysOfWeek.slice();
				dow.push(dow.shift());
				return dow;
			})();

		let highlighted = today;
		let shouldShakeDate = false;
		let shakeHighlightTimeout;
		let month = today.getMonth();
		let year = today.getFullYear();
		let isOpen = false;
		let isClosing = false;
		today.setHours(0, 0, 0, 0);

		function assignmentHandler(formatted) {
			if (!trigger) return;
			$$invalidate(1, trigger.innerHTML = formatted, trigger);
		}

		let monthIndex = 0;
		let { formattedSelected } = $$props;

		onMount(() => {
			$$invalidate(6, month = selected.getMonth());
			$$invalidate(7, year = selected.getFullYear());
		});

		function changeMonth(selectedMonth) {
			$$invalidate(6, month = selectedMonth);
			$$invalidate(10, highlighted = new Date(year, month, 1));
		}

		function incrementMonth(direction, day = 1) {
			if (direction === 1 && !canIncrementMonth) return;
			if (direction === -1 && !canDecrementMonth) return;
			let current = new Date(year, month, 1);
			current.setMonth(current.getMonth() + direction);
			$$invalidate(6, month = current.getMonth());
			$$invalidate(7, year = current.getFullYear());
			$$invalidate(10, highlighted = new Date(year, month, day));
		}

		function getDefaultHighlighted() {
			return new Date(selected);
		}

		const getDay = (m, d, y) => {
			let theMonth = months.find(aMonth => aMonth.month === m && aMonth.year === y);
			if (!theMonth) return null;

			// eslint-disable-next-line
			for (let i = 0; i < theMonth.weeks.length; ++i) {
				// eslint-disable-next-line
				for (let j = 0; j < theMonth.weeks[i].days.length; ++j) {
					let aDay = theMonth.weeks[i].days[j];
					if (aDay.month === m && aDay.day === d && aDay.year === y) return aDay;
				}
			}

			return null;
		};

		function incrementDayHighlighted(amount) {
			let proposedDate = new Date(highlighted);
			proposedDate.setDate(highlighted.getDate() + amount);
			let correspondingDayObj = getDay(proposedDate.getMonth(), proposedDate.getDate(), proposedDate.getFullYear());
			if (!correspondingDayObj || !correspondingDayObj.isInRange) return;
			$$invalidate(10, highlighted = proposedDate);

			if (amount > 0 && highlighted > lastVisibleDate) {
				incrementMonth(1, highlighted.getDate());
			}

			if (amount < 0 && highlighted < firstVisibleDate) {
				incrementMonth(-1, highlighted.getDate());
			}
		}

		function checkIfVisibleDateIsSelectable(date) {
			const proposedDay = getDay(date.getMonth(), date.getDate(), date.getFullYear());
			return proposedDay && proposedDay.selectable;
		}

		function shakeDate(date) {
			clearTimeout(shakeHighlightTimeout);
			$$invalidate(11, shouldShakeDate = date);

			shakeHighlightTimeout = setTimeout(
				() => {
					$$invalidate(11, shouldShakeDate = false);
				},
				700
			);
		}

		function assignValueToTrigger(formatted) {
			assignmentHandler(formatted);
		}

		function registerSelection(chosen) {
			if (!checkIfVisibleDateIsSelectable(chosen)) return shakeDate(chosen);

			// eslint-disable-next-line
			close();

			$$invalidate(0, selected = chosen);
			$$invalidate(24, dateChosen = true);
			assignValueToTrigger(formattedSelected);
			return dispatch('dateSelected', { date: chosen });
		}

		function handleKeyPress(evt) {
			if (keyCodesArray.indexOf(evt.keyCode) === -1) return;
			evt.preventDefault();

			switch (evt.keyCode) {
				case keyCodes.left:
					incrementDayHighlighted(-1);
					break;
				case keyCodes.up:
					incrementDayHighlighted(-7);
					break;
				case keyCodes.right:
					incrementDayHighlighted(1);
					break;
				case keyCodes.down:
					incrementDayHighlighted(7);
					break;
				case keyCodes.pgup:
					incrementMonth(-1);
					break;
				case keyCodes.pgdown:
					incrementMonth(1);
					break;
				case keyCodes.escape:
					// eslint-disable-next-line
					close();
					break;
				case keyCodes.enter:
					registerSelection(highlighted);
					break;
			}
		}

		function registerClose() {
			document.removeEventListener('keydown', handleKeyPress);
			dispatch('close');
		}

		function close() {
			popover.close();
			registerClose();
		}

		function registerOpen() {
			$$invalidate(10, highlighted = getDefaultHighlighted());
			$$invalidate(6, month = selected.getMonth());
			$$invalidate(7, year = selected.getFullYear());
			document.addEventListener('keydown', handleKeyPress);
			dispatch('open');
		}

		const writable_props = [
			'format',
			'start',
			'end',
			'selected',
			'dateChosen',
			'trigger',
			'selectableCallback',
			'weekStart',
			'daysOfWeek',
			'monthsOfYear',
			'style',
			'buttonBackgroundColor',
			'buttonBorderColor',
			'buttonTextColor',
			'highlightColor',
			'dayBackgroundColor',
			'dayTextColor',
			'dayHighlightedBackgroundColor',
			'dayHighlightedTextColor',
			'formattedSelected'
		];

		Object.keys($$props).forEach(key => {
			if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Datepicker> was created with unknown prop '${key}'`);
		});

		const monthSelected_handler = e => changeMonth(e.detail);
		const incrementMonth_handler = e => incrementMonth(e.detail);
		const dateSelected_handler = e => registerSelection(e.detail);

		function popover_1_binding($$value) {
			binding_callbacks[$$value ? 'unshift' : 'push'](() => {
				popover = $$value;
				$$invalidate(9, popover);
			});
		}

		function popover_1_open_binding(value) {
			isOpen = value;
			$$invalidate(12, isOpen);
		}

		function popover_1_shrink_binding(value) {
			isClosing = value;
			$$invalidate(13, isClosing);
		}

		$$self.$$set = $$props => {
			if ('format' in $$props) $$invalidate(25, format = $$props.format);
			if ('start' in $$props) $$invalidate(3, start = $$props.start);
			if ('end' in $$props) $$invalidate(4, end = $$props.end);
			if ('selected' in $$props) $$invalidate(0, selected = $$props.selected);
			if ('dateChosen' in $$props) $$invalidate(24, dateChosen = $$props.dateChosen);
			if ('trigger' in $$props) $$invalidate(1, trigger = $$props.trigger);
			if ('selectableCallback' in $$props) $$invalidate(26, selectableCallback = $$props.selectableCallback);
			if ('weekStart' in $$props) $$invalidate(27, weekStart = $$props.weekStart);
			if ('daysOfWeek' in $$props) $$invalidate(28, daysOfWeek = $$props.daysOfWeek);
			if ('monthsOfYear' in $$props) $$invalidate(5, monthsOfYear = $$props.monthsOfYear);
			if ('style' in $$props) $$invalidate(29, style = $$props.style);
			if ('buttonBackgroundColor' in $$props) $$invalidate(30, buttonBackgroundColor = $$props.buttonBackgroundColor);
			if ('buttonBorderColor' in $$props) $$invalidate(31, buttonBorderColor = $$props.buttonBorderColor);
			if ('buttonTextColor' in $$props) $$invalidate(32, buttonTextColor = $$props.buttonTextColor);
			if ('highlightColor' in $$props) $$invalidate(33, highlightColor = $$props.highlightColor);
			if ('dayBackgroundColor' in $$props) $$invalidate(34, dayBackgroundColor = $$props.dayBackgroundColor);
			if ('dayTextColor' in $$props) $$invalidate(35, dayTextColor = $$props.dayTextColor);
			if ('dayHighlightedBackgroundColor' in $$props) $$invalidate(36, dayHighlightedBackgroundColor = $$props.dayHighlightedBackgroundColor);
			if ('dayHighlightedTextColor' in $$props) $$invalidate(37, dayHighlightedTextColor = $$props.dayHighlightedTextColor);
			if ('formattedSelected' in $$props) $$invalidate(2, formattedSelected = $$props.formattedSelected);
			if ('$$scope' in $$props) $$invalidate(47, $$scope = $$props.$$scope);
		};

		$$self.$capture_state = () => ({
			Month,
			NavBar,
			Popover,
			getMonths,
			formatDate,
			internationalize,
			keyCodes,
			keyCodesArray,
			onMount,
			createEventDispatcher,
			dispatch,
			today,
			oneYear,
			popover,
			format,
			start,
			end,
			selected,
			dateChosen,
			trigger,
			selectableCallback,
			weekStart,
			daysOfWeek,
			monthsOfYear,
			style,
			buttonBackgroundColor,
			buttonBorderColor,
			buttonTextColor,
			highlightColor,
			dayBackgroundColor,
			dayTextColor,
			dayHighlightedBackgroundColor,
			dayHighlightedTextColor,
			sortedDaysOfWeek,
			highlighted,
			shouldShakeDate,
			shakeHighlightTimeout,
			month,
			year,
			isOpen,
			isClosing,
			assignmentHandler,
			monthIndex,
			formattedSelected,
			changeMonth,
			incrementMonth,
			getDefaultHighlighted,
			getDay,
			incrementDayHighlighted,
			checkIfVisibleDateIsSelectable,
			shakeDate,
			assignValueToTrigger,
			registerSelection,
			handleKeyPress,
			registerClose,
			close,
			registerOpen,
			firstVisibleDate,
			lastVisibleDate,
			months,
			canDecrementMonth,
			canIncrementMonth,
			wrapperStyle,
			visibleMonth,
			visibleMonthId
		});

		$$self.$inject_state = $$props => {
			if ('popover' in $$props) $$invalidate(9, popover = $$props.popover);
			if ('format' in $$props) $$invalidate(25, format = $$props.format);
			if ('start' in $$props) $$invalidate(3, start = $$props.start);
			if ('end' in $$props) $$invalidate(4, end = $$props.end);
			if ('selected' in $$props) $$invalidate(0, selected = $$props.selected);
			if ('dateChosen' in $$props) $$invalidate(24, dateChosen = $$props.dateChosen);
			if ('trigger' in $$props) $$invalidate(1, trigger = $$props.trigger);
			if ('selectableCallback' in $$props) $$invalidate(26, selectableCallback = $$props.selectableCallback);
			if ('weekStart' in $$props) $$invalidate(27, weekStart = $$props.weekStart);
			if ('daysOfWeek' in $$props) $$invalidate(28, daysOfWeek = $$props.daysOfWeek);
			if ('monthsOfYear' in $$props) $$invalidate(5, monthsOfYear = $$props.monthsOfYear);
			if ('style' in $$props) $$invalidate(29, style = $$props.style);
			if ('buttonBackgroundColor' in $$props) $$invalidate(30, buttonBackgroundColor = $$props.buttonBackgroundColor);
			if ('buttonBorderColor' in $$props) $$invalidate(31, buttonBorderColor = $$props.buttonBorderColor);
			if ('buttonTextColor' in $$props) $$invalidate(32, buttonTextColor = $$props.buttonTextColor);
			if ('highlightColor' in $$props) $$invalidate(33, highlightColor = $$props.highlightColor);
			if ('dayBackgroundColor' in $$props) $$invalidate(34, dayBackgroundColor = $$props.dayBackgroundColor);
			if ('dayTextColor' in $$props) $$invalidate(35, dayTextColor = $$props.dayTextColor);
			if ('dayHighlightedBackgroundColor' in $$props) $$invalidate(36, dayHighlightedBackgroundColor = $$props.dayHighlightedBackgroundColor);
			if ('dayHighlightedTextColor' in $$props) $$invalidate(37, dayHighlightedTextColor = $$props.dayHighlightedTextColor);
			if ('sortedDaysOfWeek' in $$props) $$invalidate(18, sortedDaysOfWeek = $$props.sortedDaysOfWeek);
			if ('highlighted' in $$props) $$invalidate(10, highlighted = $$props.highlighted);
			if ('shouldShakeDate' in $$props) $$invalidate(11, shouldShakeDate = $$props.shouldShakeDate);
			if ('shakeHighlightTimeout' in $$props) shakeHighlightTimeout = $$props.shakeHighlightTimeout;
			if ('month' in $$props) $$invalidate(6, month = $$props.month);
			if ('year' in $$props) $$invalidate(7, year = $$props.year);
			if ('isOpen' in $$props) $$invalidate(12, isOpen = $$props.isOpen);
			if ('isClosing' in $$props) $$invalidate(13, isClosing = $$props.isClosing);
			if ('monthIndex' in $$props) $$invalidate(38, monthIndex = $$props.monthIndex);
			if ('formattedSelected' in $$props) $$invalidate(2, formattedSelected = $$props.formattedSelected);
			if ('firstVisibleDate' in $$props) firstVisibleDate = $$props.firstVisibleDate;
			if ('lastVisibleDate' in $$props) lastVisibleDate = $$props.lastVisibleDate;
			if ('months' in $$props) $$invalidate(39, months = $$props.months);
			if ('canDecrementMonth' in $$props) $$invalidate(14, canDecrementMonth = $$props.canDecrementMonth);
			if ('canIncrementMonth' in $$props) $$invalidate(15, canIncrementMonth = $$props.canIncrementMonth);
			if ('wrapperStyle' in $$props) $$invalidate(16, wrapperStyle = $$props.wrapperStyle);
			if ('visibleMonth' in $$props) $$invalidate(8, visibleMonth = $$props.visibleMonth);
			if ('visibleMonthId' in $$props) $$invalidate(17, visibleMonthId = $$props.visibleMonthId);
		};

		if ($$props && "$$inject" in $$props) {
			$$self.$inject_state($$props.$$inject);
		}

		$$self.$$.update = () => {
			if ($$self.$$.dirty[0] & /*start, end, selectableCallback, weekStart*/ 201326616) {
				$$invalidate(39, months = getMonths(start, end, selectableCallback, weekStart));
			}

			if ($$self.$$.dirty[0] & /*month, year*/ 192 | $$self.$$.dirty[1] & /*months*/ 256) {
				{
					$$invalidate(38, monthIndex = 0);

					for (let i = 0; i < months.length; i += 1) {
						if (months[i].month === month && months[i].year === year) {
							$$invalidate(38, monthIndex = i);
						}
					}
				}
			}

			if ($$self.$$.dirty[1] & /*months, monthIndex*/ 384) {
				$$invalidate(8, visibleMonth = months[monthIndex]);
			}

			if ($$self.$$.dirty[0] & /*year, month*/ 192) {
				$$invalidate(17, visibleMonthId = year + month / 100);
			}

			if ($$self.$$.dirty[0] & /*visibleMonth*/ 256) {
				lastVisibleDate = visibleMonth.weeks[visibleMonth.weeks.length - 1].days[6].date;
			}

			if ($$self.$$.dirty[0] & /*visibleMonth*/ 256) {
				firstVisibleDate = visibleMonth.weeks[0].days[0].date;
			}

			if ($$self.$$.dirty[1] & /*monthIndex, months*/ 384) {
				$$invalidate(15, canIncrementMonth = monthIndex < months.length - 1);
			}

			if ($$self.$$.dirty[1] & /*monthIndex*/ 128) {
				$$invalidate(14, canDecrementMonth = monthIndex > 0);
			}

			if ($$self.$$.dirty[0] & /*buttonBackgroundColor, style*/ 1610612736 | $$self.$$.dirty[1] & /*buttonBorderColor, buttonTextColor, highlightColor, dayBackgroundColor, dayTextColor, dayHighlightedBackgroundColor, dayHighlightedTextColor*/ 127) {
				$$invalidate(16, wrapperStyle = `
    --button-background-color: ${buttonBackgroundColor};
    --button-border-color: ${buttonBorderColor};
    --button-text-color: ${buttonTextColor};
    --highlight-color: ${highlightColor};
    --day-background-color: ${dayBackgroundColor};
    --day-text-color: ${dayTextColor};
    --day-highlighted-background-color: ${dayHighlightedBackgroundColor};
    --day-highlighted-text-color: ${dayHighlightedTextColor};
    ${style}
  `);
			}

			if ($$self.$$.dirty[0] & /*format, selected*/ 33554433) {
				{
					$$invalidate(2, formattedSelected = typeof format === 'function'
						? format(selected)
						: formatDate(selected, format));
				}
			}
		};

		return [
			selected,
			trigger,
			formattedSelected,
			start,
			end,
			monthsOfYear,
			month,
			year,
			visibleMonth,
			popover,
			highlighted,
			shouldShakeDate,
			isOpen,
			isClosing,
			canDecrementMonth,
			canIncrementMonth,
			wrapperStyle,
			visibleMonthId,
			sortedDaysOfWeek,
			changeMonth,
			incrementMonth,
			registerSelection,
			registerClose,
			registerOpen,
			dateChosen,
			format,
			selectableCallback,
			weekStart,
			daysOfWeek,
			style,
			buttonBackgroundColor,
			buttonBorderColor,
			buttonTextColor,
			highlightColor,
			dayBackgroundColor,
			dayTextColor,
			dayHighlightedBackgroundColor,
			dayHighlightedTextColor,
			monthIndex,
			months,
			slots,
			monthSelected_handler,
			incrementMonth_handler,
			dateSelected_handler,
			popover_1_binding,
			popover_1_open_binding,
			popover_1_shrink_binding,
			$$scope
		];
	}

	class Datepicker extends SvelteComponentDev {
		constructor(options) {
			super(options);

			init(
				this,
				options,
				instance$8,
				create_fragment$8,
				safe_not_equal,
				{
					format: 25,
					start: 3,
					end: 4,
					selected: 0,
					dateChosen: 24,
					trigger: 1,
					selectableCallback: 26,
					weekStart: 27,
					daysOfWeek: 28,
					monthsOfYear: 5,
					style: 29,
					buttonBackgroundColor: 30,
					buttonBorderColor: 31,
					buttonTextColor: 32,
					highlightColor: 33,
					dayBackgroundColor: 34,
					dayTextColor: 35,
					dayHighlightedBackgroundColor: 36,
					dayHighlightedTextColor: 37,
					formattedSelected: 2
				},
				null,
				[-1, -1, -1]
			);

			dispatch_dev("SvelteRegisterComponent", {
				component: this,
				tagName: "Datepicker",
				options,
				id: create_fragment$8.name
			});

			const { ctx } = this.$$;
			const props = options.props || {};

			if (/*formattedSelected*/ ctx[2] === undefined && !('formattedSelected' in props)) {
				console.warn("<Datepicker> was created without expected prop 'formattedSelected'");
			}
		}

		get format() {
			throw new Error("<Datepicker>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set format(value) {
			throw new Error("<Datepicker>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get start() {
			throw new Error("<Datepicker>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set start(value) {
			throw new Error("<Datepicker>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get end() {
			throw new Error("<Datepicker>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set end(value) {
			throw new Error("<Datepicker>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get selected() {
			throw new Error("<Datepicker>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set selected(value) {
			throw new Error("<Datepicker>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get dateChosen() {
			throw new Error("<Datepicker>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set dateChosen(value) {
			throw new Error("<Datepicker>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get trigger() {
			throw new Error("<Datepicker>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set trigger(value) {
			throw new Error("<Datepicker>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get selectableCallback() {
			throw new Error("<Datepicker>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set selectableCallback(value) {
			throw new Error("<Datepicker>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get weekStart() {
			throw new Error("<Datepicker>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set weekStart(value) {
			throw new Error("<Datepicker>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get daysOfWeek() {
			throw new Error("<Datepicker>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set daysOfWeek(value) {
			throw new Error("<Datepicker>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get monthsOfYear() {
			throw new Error("<Datepicker>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set monthsOfYear(value) {
			throw new Error("<Datepicker>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get style() {
			throw new Error("<Datepicker>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set style(value) {
			throw new Error("<Datepicker>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get buttonBackgroundColor() {
			throw new Error("<Datepicker>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set buttonBackgroundColor(value) {
			throw new Error("<Datepicker>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get buttonBorderColor() {
			throw new Error("<Datepicker>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set buttonBorderColor(value) {
			throw new Error("<Datepicker>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get buttonTextColor() {
			throw new Error("<Datepicker>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set buttonTextColor(value) {
			throw new Error("<Datepicker>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get highlightColor() {
			throw new Error("<Datepicker>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set highlightColor(value) {
			throw new Error("<Datepicker>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get dayBackgroundColor() {
			throw new Error("<Datepicker>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set dayBackgroundColor(value) {
			throw new Error("<Datepicker>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get dayTextColor() {
			throw new Error("<Datepicker>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set dayTextColor(value) {
			throw new Error("<Datepicker>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get dayHighlightedBackgroundColor() {
			throw new Error("<Datepicker>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set dayHighlightedBackgroundColor(value) {
			throw new Error("<Datepicker>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get dayHighlightedTextColor() {
			throw new Error("<Datepicker>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set dayHighlightedTextColor(value) {
			throw new Error("<Datepicker>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get formattedSelected() {
			throw new Error("<Datepicker>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set formattedSelected(value) {
			throw new Error("<Datepicker>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}
	}

	/* src/components/PickDateFrom.svelte generated by Svelte v3.42.3 */
	const file$7 = "src/components/PickDateFrom.svelte";

	// (12:6) {:else}
	function create_else_block$3(ctx) {
		let t;

		const block = {
			c: function create() {
				t = text("From:");
			},
			m: function mount(target, anchor) {
				insert_dev(target, t, anchor);
			},
			p: noop,
			d: function destroy(detaching) {
				if (detaching) detach_dev(t);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_else_block$3.name,
			type: "else",
			source: "(12:6) {:else}",
			ctx
		});

		return block;
	}

	// (10:6) {#if dateChosen}
	function create_if_block$4(ctx) {
		let t0;
		let t1;

		const block = {
			c: function create() {
				t0 = text("From: ");
				t1 = text(/*formattedSelected*/ ctx[0]);
			},
			m: function mount(target, anchor) {
				insert_dev(target, t0, anchor);
				insert_dev(target, t1, anchor);
			},
			p: function update(ctx, dirty) {
				if (dirty & /*formattedSelected*/ 1) set_data_dev(t1, /*formattedSelected*/ ctx[0]);
			},
			d: function destroy(detaching) {
				if (detaching) detach_dev(t0);
				if (detaching) detach_dev(t1);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_if_block$4.name,
			type: "if",
			source: "(10:6) {#if dateChosen}",
			ctx
		});

		return block;
	}

	// (8:0) <Datepicker bind:formattedSelected bind:dateChosen>
	function create_default_slot$4(ctx) {
		let button;

		function select_block_type(ctx, dirty) {
			if (/*dateChosen*/ ctx[1]) return create_if_block$4;
			return create_else_block$3;
		}

		let current_block_type = select_block_type(ctx);
		let if_block = current_block_type(ctx);

		const block = {
			c: function create() {
				button = element("button");
				if_block.c();
				attr_dev(button, "class", "custom-button");
				add_location(button, file$7, 8, 2, 167);
			},
			m: function mount(target, anchor) {
				insert_dev(target, button, anchor);
				if_block.m(button, null);
			},
			p: function update(ctx, dirty) {
				if (current_block_type === (current_block_type = select_block_type(ctx)) && if_block) {
					if_block.p(ctx, dirty);
				} else {
					if_block.d(1);
					if_block = current_block_type(ctx);

					if (if_block) {
						if_block.c();
						if_block.m(button, null);
					}
				}
			},
			d: function destroy(detaching) {
				if (detaching) detach_dev(button);
				if_block.d();
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_default_slot$4.name,
			type: "slot",
			source: "(8:0) <Datepicker bind:formattedSelected bind:dateChosen>",
			ctx
		});

		return block;
	}

	function create_fragment$7(ctx) {
		let datepicker;
		let updating_formattedSelected;
		let updating_dateChosen;
		let current;

		function datepicker_formattedSelected_binding(value) {
    		/*datepicker_formattedSelected_binding*/ ctx[2](value);
		}

		function datepicker_dateChosen_binding(value) {
    		/*datepicker_dateChosen_binding*/ ctx[3](value);
		}

		let datepicker_props = {
			$$slots: { default: [create_default_slot$4] },
			$$scope: { ctx }
		};

		if (/*formattedSelected*/ ctx[0] !== void 0) {
			datepicker_props.formattedSelected = /*formattedSelected*/ ctx[0];
		}

		if (/*dateChosen*/ ctx[1] !== void 0) {
			datepicker_props.dateChosen = /*dateChosen*/ ctx[1];
		}

		datepicker = new Datepicker({ props: datepicker_props, $$inline: true });
		binding_callbacks.push(() => bind$1(datepicker, 'formattedSelected', datepicker_formattedSelected_binding));
		binding_callbacks.push(() => bind$1(datepicker, 'dateChosen', datepicker_dateChosen_binding));

		const block = {
			c: function create() {
				create_component(datepicker.$$.fragment);
			},
			l: function claim(nodes) {
				throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			},
			m: function mount(target, anchor) {
				mount_component(datepicker, target, anchor);
				current = true;
			},
			p: function update(ctx, [dirty]) {
				const datepicker_changes = {};

				if (dirty & /*$$scope, formattedSelected, dateChosen*/ 19) {
					datepicker_changes.$$scope = { dirty, ctx };
				}

				if (!updating_formattedSelected && dirty & /*formattedSelected*/ 1) {
					updating_formattedSelected = true;
					datepicker_changes.formattedSelected = /*formattedSelected*/ ctx[0];
					add_flush_callback(() => updating_formattedSelected = false);
				}

				if (!updating_dateChosen && dirty & /*dateChosen*/ 2) {
					updating_dateChosen = true;
					datepicker_changes.dateChosen = /*dateChosen*/ ctx[1];
					add_flush_callback(() => updating_dateChosen = false);
				}

				datepicker.$set(datepicker_changes);
			},
			i: function intro(local) {
				if (current) return;
				transition_in(datepicker.$$.fragment, local);
				current = true;
			},
			o: function outro(local) {
				transition_out(datepicker.$$.fragment, local);
				current = false;
			},
			d: function destroy(detaching) {
				destroy_component(datepicker, detaching);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_fragment$7.name,
			type: "component",
			source: "",
			ctx
		});

		return block;
	}

	function instance$7($$self, $$props, $$invalidate) {
		let { $$slots: slots = {}, $$scope } = $$props;
		validate_slots('PickDateFrom', slots, []);
		let formattedSelected;
		let dateChosen = true;
		const writable_props = [];

		Object.keys($$props).forEach(key => {
			if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<PickDateFrom> was created with unknown prop '${key}'`);
		});

		function datepicker_formattedSelected_binding(value) {
			formattedSelected = value;
			$$invalidate(0, formattedSelected);
		}

		function datepicker_dateChosen_binding(value) {
			dateChosen = value;
			$$invalidate(1, dateChosen);
		}

		$$self.$capture_state = () => ({
			Datepicker,
			formattedSelected,
			dateChosen
		});

		$$self.$inject_state = $$props => {
			if ('formattedSelected' in $$props) $$invalidate(0, formattedSelected = $$props.formattedSelected);
			if ('dateChosen' in $$props) $$invalidate(1, dateChosen = $$props.dateChosen);
		};

		if ($$props && "$$inject" in $$props) {
			$$self.$inject_state($$props.$$inject);
		}

		return [
			formattedSelected,
			dateChosen,
			datepicker_formattedSelected_binding,
			datepicker_dateChosen_binding
		];
	}

	class PickDateFrom extends SvelteComponentDev {
		constructor(options) {
			super(options);
			init(this, options, instance$7, create_fragment$7, safe_not_equal, {});

			dispatch_dev("SvelteRegisterComponent", {
				component: this,
				tagName: "PickDateFrom",
				options,
				id: create_fragment$7.name
			});
		}
	}

	/* src/components/PickDateTo.svelte generated by Svelte v3.42.3 */
	const file$6 = "src/components/PickDateTo.svelte";

	// (10:45) {:else}
	function create_else_block$2(ctx) {
		let t;

		const block = {
			c: function create() {
				t = text("To:");
			},
			m: function mount(target, anchor) {
				insert_dev(target, t, anchor);
			},
			p: noop,
			d: function destroy(detaching) {
				if (detaching) detach_dev(t);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_else_block$2.name,
			type: "else",
			source: "(10:45) {:else}",
			ctx
		});

		return block;
	}

	// (10:4) {#if dateChosen}
	function create_if_block$3(ctx) {
		let t0;
		let t1;

		const block = {
			c: function create() {
				t0 = text("To: ");
				t1 = text(/*formattedSelected*/ ctx[0]);
			},
			m: function mount(target, anchor) {
				insert_dev(target, t0, anchor);
				insert_dev(target, t1, anchor);
			},
			p: function update(ctx, dirty) {
				if (dirty & /*formattedSelected*/ 1) set_data_dev(t1, /*formattedSelected*/ ctx[0]);
			},
			d: function destroy(detaching) {
				if (detaching) detach_dev(t0);
				if (detaching) detach_dev(t1);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_if_block$3.name,
			type: "if",
			source: "(10:4) {#if dateChosen}",
			ctx
		});

		return block;
	}

	// (8:0) <Datepicker bind:formattedSelected bind:dateChosen >
	function create_default_slot$3(ctx) {
		let button;

		function select_block_type(ctx, dirty) {
			if (/*dateChosen*/ ctx[1]) return create_if_block$3;
			return create_else_block$2;
		}

		let current_block_type = select_block_type(ctx);
		let if_block = current_block_type(ctx);

		const block = {
			c: function create() {
				button = element("button");
				if_block.c();
				attr_dev(button, "class", "custom-button");
				add_location(button, file$6, 8, 2, 168);
			},
			m: function mount(target, anchor) {
				insert_dev(target, button, anchor);
				if_block.m(button, null);
			},
			p: function update(ctx, dirty) {
				if (current_block_type === (current_block_type = select_block_type(ctx)) && if_block) {
					if_block.p(ctx, dirty);
				} else {
					if_block.d(1);
					if_block = current_block_type(ctx);

					if (if_block) {
						if_block.c();
						if_block.m(button, null);
					}
				}
			},
			d: function destroy(detaching) {
				if (detaching) detach_dev(button);
				if_block.d();
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_default_slot$3.name,
			type: "slot",
			source: "(8:0) <Datepicker bind:formattedSelected bind:dateChosen >",
			ctx
		});

		return block;
	}

	function create_fragment$6(ctx) {
		let datepicker;
		let updating_formattedSelected;
		let updating_dateChosen;
		let current;

		function datepicker_formattedSelected_binding(value) {
    		/*datepicker_formattedSelected_binding*/ ctx[2](value);
		}

		function datepicker_dateChosen_binding(value) {
    		/*datepicker_dateChosen_binding*/ ctx[3](value);
		}

		let datepicker_props = {
			$$slots: { default: [create_default_slot$3] },
			$$scope: { ctx }
		};

		if (/*formattedSelected*/ ctx[0] !== void 0) {
			datepicker_props.formattedSelected = /*formattedSelected*/ ctx[0];
		}

		if (/*dateChosen*/ ctx[1] !== void 0) {
			datepicker_props.dateChosen = /*dateChosen*/ ctx[1];
		}

		datepicker = new Datepicker({ props: datepicker_props, $$inline: true });
		binding_callbacks.push(() => bind$1(datepicker, 'formattedSelected', datepicker_formattedSelected_binding));
		binding_callbacks.push(() => bind$1(datepicker, 'dateChosen', datepicker_dateChosen_binding));

		const block = {
			c: function create() {
				create_component(datepicker.$$.fragment);
			},
			l: function claim(nodes) {
				throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			},
			m: function mount(target, anchor) {
				mount_component(datepicker, target, anchor);
				current = true;
			},
			p: function update(ctx, [dirty]) {
				const datepicker_changes = {};

				if (dirty & /*$$scope, formattedSelected, dateChosen*/ 19) {
					datepicker_changes.$$scope = { dirty, ctx };
				}

				if (!updating_formattedSelected && dirty & /*formattedSelected*/ 1) {
					updating_formattedSelected = true;
					datepicker_changes.formattedSelected = /*formattedSelected*/ ctx[0];
					add_flush_callback(() => updating_formattedSelected = false);
				}

				if (!updating_dateChosen && dirty & /*dateChosen*/ 2) {
					updating_dateChosen = true;
					datepicker_changes.dateChosen = /*dateChosen*/ ctx[1];
					add_flush_callback(() => updating_dateChosen = false);
				}

				datepicker.$set(datepicker_changes);
			},
			i: function intro(local) {
				if (current) return;
				transition_in(datepicker.$$.fragment, local);
				current = true;
			},
			o: function outro(local) {
				transition_out(datepicker.$$.fragment, local);
				current = false;
			},
			d: function destroy(detaching) {
				destroy_component(datepicker, detaching);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_fragment$6.name,
			type: "component",
			source: "",
			ctx
		});

		return block;
	}

	function instance$6($$self, $$props, $$invalidate) {
		let { $$slots: slots = {}, $$scope } = $$props;
		validate_slots('PickDateTo', slots, []);
		let formattedSelected;
		let dateChosen = true;
		const writable_props = [];

		Object.keys($$props).forEach(key => {
			if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<PickDateTo> was created with unknown prop '${key}'`);
		});

		function datepicker_formattedSelected_binding(value) {
			formattedSelected = value;
			$$invalidate(0, formattedSelected);
		}

		function datepicker_dateChosen_binding(value) {
			dateChosen = value;
			$$invalidate(1, dateChosen);
		}

		$$self.$capture_state = () => ({
			Datepicker,
			formattedSelected,
			dateChosen
		});

		$$self.$inject_state = $$props => {
			if ('formattedSelected' in $$props) $$invalidate(0, formattedSelected = $$props.formattedSelected);
			if ('dateChosen' in $$props) $$invalidate(1, dateChosen = $$props.dateChosen);
		};

		if ($$props && "$$inject" in $$props) {
			$$self.$inject_state($$props.$$inject);
		}

		return [
			formattedSelected,
			dateChosen,
			datepicker_formattedSelected_binding,
			datepicker_dateChosen_binding
		];
	}

	class PickDateTo extends SvelteComponentDev {
		constructor(options) {
			super(options);
			init(this, options, instance$6, create_fragment$6, safe_not_equal, {});

			dispatch_dev("SvelteRegisterComponent", {
				component: this,
				tagName: "PickDateTo",
				options,
				id: create_fragment$6.name
			});
		}
	}

	function is_date(obj) {
		return Object.prototype.toString.call(obj) === '[object Date]';
	}

	function tick_spring(ctx, last_value, current_value, target_value) {
		if (typeof current_value === 'number' || is_date(current_value)) {
			// @ts-ignore
			const delta = target_value - current_value;
			// @ts-ignore
			const velocity = (current_value - last_value) / (ctx.dt || 1 / 60); // guard div by 0
			const spring = ctx.opts.stiffness * delta;
			const damper = ctx.opts.damping * velocity;
			const acceleration = (spring - damper) * ctx.inv_mass;
			const d = (velocity + acceleration) * ctx.dt;
			if (Math.abs(d) < ctx.opts.precision && Math.abs(delta) < ctx.opts.precision) {
				return target_value; // settled
			}
			else {
				ctx.settled = false; // signal loop to keep ticking
				// @ts-ignore
				return is_date(current_value) ?
					new Date(current_value.getTime() + d) : current_value + d;
			}
		}
		else if (Array.isArray(current_value)) {
			// @ts-ignore
			return current_value.map((_, i) => tick_spring(ctx, last_value[i], current_value[i], target_value[i]));
		}
		else if (typeof current_value === 'object') {
			const next_value = {};
			for (const k in current_value) {
				// @ts-ignore
				next_value[k] = tick_spring(ctx, last_value[k], current_value[k], target_value[k]);
			}
			// @ts-ignore
			return next_value;
		}
		else {
			throw new Error(`Cannot spring ${typeof current_value} values`);
		}
	}
	function spring(value, opts = {}) {
		const store = writable(value);
		const { stiffness = 0.15, damping = 0.8, precision = 0.01 } = opts;
		let last_time;
		let task;
		let current_token;
		let last_value = value;
		let target_value = value;
		let inv_mass = 1;
		let inv_mass_recovery_rate = 0;
		let cancel_task = false;
		function set(new_value, opts = {}) {
			target_value = new_value;
			const token = current_token = {};
			if (value == null || opts.hard || (spring.stiffness >= 1 && spring.damping >= 1)) {
				cancel_task = true; // cancel any running animation
				last_time = now();
				last_value = new_value;
				store.set(value = target_value);
				return Promise.resolve();
			}
			else if (opts.soft) {
				const rate = opts.soft === true ? .5 : +opts.soft;
				inv_mass_recovery_rate = 1 / (rate * 60);
				inv_mass = 0; // infinite mass, unaffected by spring forces
			}
			if (!task) {
				last_time = now();
				cancel_task = false;
				task = loop(now => {
					if (cancel_task) {
						cancel_task = false;
						task = null;
						return false;
					}
					inv_mass = Math.min(inv_mass + inv_mass_recovery_rate, 1);
					const ctx = {
						inv_mass,
						opts: spring,
						settled: true,
						dt: (now - last_time) * 60 / 1000
					};
					const next_value = tick_spring(ctx, last_value, value, target_value);
					last_time = now;
					last_value = value;
					store.set(value = next_value);
					if (ctx.settled) {
						task = null;
					}
					return !ctx.settled;
				});
			}
			return new Promise(fulfil => {
				task.promise.then(() => {
					if (token === current_token)
						fulfil();
				});
			});
		}
		const spring = {
			set,
			update: (fn, opts) => set(fn(target_value, value), opts),
			subscribe: store.subscribe,
			stiffness,
			damping,
			precision
		};
		return spring;
	}

	function pannable(node) {
		let x;
		let y;

		function handleMousedown(event) {
			x = event.clientX;
			y = event.clientY;

			node.dispatchEvent(new CustomEvent('panstart', {
				detail: { x, y }
			}));

			window.addEventListener('mousemove', handleMousemove);
			window.addEventListener('mouseup', handleMouseup);
		}

		function handleMousemove(event) {
			const dx = event.clientX - x;
			const dy = event.clientY - y;
			x = event.clientX;
			y = event.clientY;

			node.dispatchEvent(new CustomEvent('panmove', {
				detail: { x, y, dx, dy }
			}));
		}

		function handleMouseup(event) {
			x = event.clientX;
			y = event.clientY;

			node.dispatchEvent(new CustomEvent('panend', {
				detail: { x, y }
			}));

			window.removeEventListener('mousemove', handleMousemove);
			window.removeEventListener('mouseup', handleMouseup);
		}

		node.addEventListener('mousedown', handleMousedown);

		return {
			destroy() {
				node.removeEventListener('mousedown', handleMousedown);
			}
		};
	}

	/* src/components/PannableBox.svelte generated by Svelte v3.42.3 */
	const file$5 = "src/components/PannableBox.svelte";
	const get_default_slot_changes = dirty => ({});
	const get_default_slot_context = ctx => ({ class: "slot svelte-i1c677" });

	function create_fragment$5(ctx) {
		let div;
		let current;
		let mounted;
		let dispose;
		const default_slot_template = /*#slots*/ ctx[6].default;
		const default_slot = create_slot(default_slot_template, ctx, /*$$scope*/ ctx[5], get_default_slot_context);

		const block = {
			c: function create() {
				div = element("div");
				if (default_slot) default_slot.c();
				attr_dev(div, "class", "box svelte-i1c677");
				set_style(div, "transform", "translate(" + /*$coords*/ ctx[1].x + "px," + /*$coords*/ ctx[1].y + "px) rotate(" + /*$coords*/ ctx[1].x * 0.2 + "deg)");
				add_location(div, file$5, 27, 0, 526);
			},
			l: function claim(nodes) {
				throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			},
			m: function mount(target, anchor) {
				insert_dev(target, div, anchor);

				if (default_slot) {
					default_slot.m(div, null);
				}

				current = true;

				if (!mounted) {
					dispose = [
						action_destroyer(pannable.call(null, div)),
						listen_dev(div, "panstart", /*handlePanStart*/ ctx[2], false, false, false),
						listen_dev(div, "panmove", /*handlePanMove*/ ctx[3], false, false, false),
						listen_dev(div, "panend", /*handlePanEnd*/ ctx[4], false, false, false)
					];

					mounted = true;
				}
			},
			p: function update(ctx, [dirty]) {
				if (default_slot) {
					if (default_slot.p && (!current || dirty & /*$$scope*/ 32)) {
						update_slot_base(
							default_slot,
							default_slot_template,
							ctx,
    						/*$$scope*/ ctx[5],
							!current
								? get_all_dirty_from_scope(/*$$scope*/ ctx[5])
								: get_slot_changes(default_slot_template, /*$$scope*/ ctx[5], dirty, get_default_slot_changes),
							get_default_slot_context
						);
					}
				}

				if (!current || dirty & /*$coords*/ 2) {
					set_style(div, "transform", "translate(" + /*$coords*/ ctx[1].x + "px," + /*$coords*/ ctx[1].y + "px) rotate(" + /*$coords*/ ctx[1].x * 0.2 + "deg)");
				}
			},
			i: function intro(local) {
				if (current) return;
				transition_in(default_slot, local);
				current = true;
			},
			o: function outro(local) {
				transition_out(default_slot, local);
				current = false;
			},
			d: function destroy(detaching) {
				if (detaching) detach_dev(div);
				if (default_slot) default_slot.d(detaching);
				mounted = false;
				run_all(dispose);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_fragment$5.name,
			type: "component",
			source: "",
			ctx
		});

		return block;
	}

	function instance$5($$self, $$props, $$invalidate) {
		let $coords;
		let { $$slots: slots = {}, $$scope } = $$props;
		validate_slots('PannableBox', slots, ['default']);
		const coords = spring({ x: 0, y: 0 }, { stiffness: 0.2, damping: 0.4 });
		validate_store(coords, 'coords');
		component_subscribe($$self, coords, value => $$invalidate(1, $coords = value));

		function handlePanStart() {
			$$invalidate(0, coords.stiffness = $$invalidate(0, coords.damping = 1, coords), coords);
		}

		function handlePanMove(event) {
			coords.update($coords => ({
				x: $coords.x + event.detail.dx,
				y: $coords.y + event.detail.dy
			}));
		}

		function handlePanEnd(event) {
			$$invalidate(0, coords.stiffness = 0.2, coords);
			$$invalidate(0, coords.damping = 0.4, coords);
			coords.set({ x: 0, y: 0 });
		}

		const writable_props = [];

		Object.keys($$props).forEach(key => {
			if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<PannableBox> was created with unknown prop '${key}'`);
		});

		$$self.$$set = $$props => {
			if ('$$scope' in $$props) $$invalidate(5, $$scope = $$props.$$scope);
		};

		$$self.$capture_state = () => ({
			spring,
			pannable,
			coords,
			handlePanStart,
			handlePanMove,
			handlePanEnd,
			$coords
		});

		return [coords, $coords, handlePanStart, handlePanMove, handlePanEnd, $$scope, slots];
	}

	class PannableBox extends SvelteComponentDev {
		constructor(options) {
			super(options);
			init(this, options, instance$5, create_fragment$5, safe_not_equal, {});

			dispatch_dev("SvelteRegisterComponent", {
				component: this,
				tagName: "PannableBox",
				options,
				id: create_fragment$5.name
			});
		}
	}

	var bind = function bind(fn, thisArg) {
		return function wrap() {
			var args = new Array(arguments.length);
			for (var i = 0; i < args.length; i++) {
				args[i] = arguments[i];
			}
			return fn.apply(thisArg, args);
		};
	};

	/*global toString:true*/

	// utils is a library of generic helper functions non-specific to axios

	var toString = Object.prototype.toString;

	/**
	 * Determine if a value is an Array
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is an Array, otherwise false
	 */
	function isArray(val) {
		return toString.call(val) === '[object Array]';
	}

	/**
	 * Determine if a value is undefined
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if the value is undefined, otherwise false
	 */
	function isUndefined(val) {
		return typeof val === 'undefined';
	}

	/**
	 * Determine if a value is a Buffer
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is a Buffer, otherwise false
	 */
	function isBuffer(val) {
		return val !== null && !isUndefined(val) && val.constructor !== null && !isUndefined(val.constructor)
			&& typeof val.constructor.isBuffer === 'function' && val.constructor.isBuffer(val);
	}

	/**
	 * Determine if a value is an ArrayBuffer
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is an ArrayBuffer, otherwise false
	 */
	function isArrayBuffer(val) {
		return toString.call(val) === '[object ArrayBuffer]';
	}

	/**
	 * Determine if a value is a FormData
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is an FormData, otherwise false
	 */
	function isFormData(val) {
		return (typeof FormData !== 'undefined') && (val instanceof FormData);
	}

	/**
	 * Determine if a value is a view on an ArrayBuffer
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is a view on an ArrayBuffer, otherwise false
	 */
	function isArrayBufferView(val) {
		var result;
		if ((typeof ArrayBuffer !== 'undefined') && (ArrayBuffer.isView)) {
			result = ArrayBuffer.isView(val);
		} else {
			result = (val) && (val.buffer) && (val.buffer instanceof ArrayBuffer);
		}
		return result;
	}

	/**
	 * Determine if a value is a String
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is a String, otherwise false
	 */
	function isString(val) {
		return typeof val === 'string';
	}

	/**
	 * Determine if a value is a Number
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is a Number, otherwise false
	 */
	function isNumber(val) {
		return typeof val === 'number';
	}

	/**
	 * Determine if a value is an Object
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is an Object, otherwise false
	 */
	function isObject(val) {
		return val !== null && typeof val === 'object';
	}

	/**
	 * Determine if a value is a plain Object
	 *
	 * @param {Object} val The value to test
	 * @return {boolean} True if value is a plain Object, otherwise false
	 */
	function isPlainObject(val) {
		if (toString.call(val) !== '[object Object]') {
			return false;
		}

		var prototype = Object.getPrototypeOf(val);
		return prototype === null || prototype === Object.prototype;
	}

	/**
	 * Determine if a value is a Date
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is a Date, otherwise false
	 */
	function isDate(val) {
		return toString.call(val) === '[object Date]';
	}

	/**
	 * Determine if a value is a File
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is a File, otherwise false
	 */
	function isFile(val) {
		return toString.call(val) === '[object File]';
	}

	/**
	 * Determine if a value is a Blob
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is a Blob, otherwise false
	 */
	function isBlob(val) {
		return toString.call(val) === '[object Blob]';
	}

	/**
	 * Determine if a value is a Function
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is a Function, otherwise false
	 */
	function isFunction(val) {
		return toString.call(val) === '[object Function]';
	}

	/**
	 * Determine if a value is a Stream
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is a Stream, otherwise false
	 */
	function isStream(val) {
		return isObject(val) && isFunction(val.pipe);
	}

	/**
	 * Determine if a value is a URLSearchParams object
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is a URLSearchParams object, otherwise false
	 */
	function isURLSearchParams(val) {
		return typeof URLSearchParams !== 'undefined' && val instanceof URLSearchParams;
	}

	/**
	 * Trim excess whitespace off the beginning and end of a string
	 *
	 * @param {String} str The String to trim
	 * @returns {String} The String freed of excess whitespace
	 */
	function trim(str) {
		return str.replace(/^\s*/, '').replace(/\s*$/, '');
	}

	/**
	 * Determine if we're running in a standard browser environment
	 *
	 * This allows axios to run in a web worker, and react-native.
	 * Both environments support XMLHttpRequest, but not fully standard globals.
	 *
	 * web workers:
	 *  typeof window -> undefined
	 *  typeof document -> undefined
	 *
	 * react-native:
	 *  navigator.product -> 'ReactNative'
	 * nativescript
	 *  navigator.product -> 'NativeScript' or 'NS'
	 */
	function isStandardBrowserEnv() {
		if (typeof navigator !== 'undefined' && (navigator.product === 'ReactNative' ||
			navigator.product === 'NativeScript' ||
			navigator.product === 'NS')) {
			return false;
		}
		return (
			typeof window !== 'undefined' &&
			typeof document !== 'undefined'
		);
	}

	/**
	 * Iterate over an Array or an Object invoking a function for each item.
	 *
	 * If `obj` is an Array callback will be called passing
	 * the value, index, and complete array for each item.
	 *
	 * If 'obj' is an Object callback will be called passing
	 * the value, key, and complete object for each property.
	 *
	 * @param {Object|Array} obj The object to iterate
	 * @param {Function} fn The callback to invoke for each item
	 */
	function forEach(obj, fn) {
		// Don't bother if no value provided
		if (obj === null || typeof obj === 'undefined') {
			return;
		}

		// Force an array if not already something iterable
		if (typeof obj !== 'object') {
			/*eslint no-param-reassign:0*/
			obj = [obj];
		}

		if (isArray(obj)) {
			// Iterate over array values
			for (var i = 0, l = obj.length; i < l; i++) {
				fn.call(null, obj[i], i, obj);
			}
		} else {
			// Iterate over object keys
			for (var key in obj) {
				if (Object.prototype.hasOwnProperty.call(obj, key)) {
					fn.call(null, obj[key], key, obj);
				}
			}
		}
	}

	/**
	 * Accepts varargs expecting each argument to be an object, then
	 * immutably merges the properties of each object and returns result.
	 *
	 * When multiple objects contain the same key the later object in
	 * the arguments list will take precedence.
	 *
	 * Example:
	 *
	 * ```js
	 * var result = merge({foo: 123}, {foo: 456});
	 * console.log(result.foo); // outputs 456
	 * ```
	 *
	 * @param {Object} obj1 Object to merge
	 * @returns {Object} Result of all merge properties
	 */
	function merge(/* obj1, obj2, obj3, ... */) {
		var result = {};
		function assignValue(val, key) {
			if (isPlainObject(result[key]) && isPlainObject(val)) {
				result[key] = merge(result[key], val);
			} else if (isPlainObject(val)) {
				result[key] = merge({}, val);
			} else if (isArray(val)) {
				result[key] = val.slice();
			} else {
				result[key] = val;
			}
		}

		for (var i = 0, l = arguments.length; i < l; i++) {
			forEach(arguments[i], assignValue);
		}
		return result;
	}

	/**
	 * Extends object a by mutably adding to it the properties of object b.
	 *
	 * @param {Object} a The object to be extended
	 * @param {Object} b The object to copy properties from
	 * @param {Object} thisArg The object to bind function to
	 * @return {Object} The resulting value of object a
	 */
	function extend(a, b, thisArg) {
		forEach(b, function assignValue(val, key) {
			if (thisArg && typeof val === 'function') {
				a[key] = bind(val, thisArg);
			} else {
				a[key] = val;
			}
		});
		return a;
	}

	/**
	 * Remove byte order marker. This catches EF BB BF (the UTF-8 BOM)
	 *
	 * @param {string} content with BOM
	 * @return {string} content value without BOM
	 */
	function stripBOM(content) {
		if (content.charCodeAt(0) === 0xFEFF) {
			content = content.slice(1);
		}
		return content;
	}

	var utils = {
		isArray: isArray,
		isArrayBuffer: isArrayBuffer,
		isBuffer: isBuffer,
		isFormData: isFormData,
		isArrayBufferView: isArrayBufferView,
		isString: isString,
		isNumber: isNumber,
		isObject: isObject,
		isPlainObject: isPlainObject,
		isUndefined: isUndefined,
		isDate: isDate,
		isFile: isFile,
		isBlob: isBlob,
		isFunction: isFunction,
		isStream: isStream,
		isURLSearchParams: isURLSearchParams,
		isStandardBrowserEnv: isStandardBrowserEnv,
		forEach: forEach,
		merge: merge,
		extend: extend,
		trim: trim,
		stripBOM: stripBOM
	};

	function encode(val) {
		return encodeURIComponent(val).
			replace(/%3A/gi, ':').
			replace(/%24/g, '$').
			replace(/%2C/gi, ',').
			replace(/%20/g, '+').
			replace(/%5B/gi, '[').
			replace(/%5D/gi, ']');
	}

	/**
	 * Build a URL by appending params to the end
	 *
	 * @param {string} url The base of the url (e.g., http://www.google.com)
	 * @param {object} [params] The params to be appended
	 * @returns {string} The formatted url
	 */
	var buildURL = function buildURL(url, params, paramsSerializer) {
		/*eslint no-param-reassign:0*/
		if (!params) {
			return url;
		}

		var serializedParams;
		if (paramsSerializer) {
			serializedParams = paramsSerializer(params);
		} else if (utils.isURLSearchParams(params)) {
			serializedParams = params.toString();
		} else {
			var parts = [];

			utils.forEach(params, function serialize(val, key) {
				if (val === null || typeof val === 'undefined') {
					return;
				}

				if (utils.isArray(val)) {
					key = key + '[]';
				} else {
					val = [val];
				}

				utils.forEach(val, function parseValue(v) {
					if (utils.isDate(v)) {
						v = v.toISOString();
					} else if (utils.isObject(v)) {
						v = JSON.stringify(v);
					}
					parts.push(encode(key) + '=' + encode(v));
				});
			});

			serializedParams = parts.join('&');
		}

		if (serializedParams) {
			var hashmarkIndex = url.indexOf('#');
			if (hashmarkIndex !== -1) {
				url = url.slice(0, hashmarkIndex);
			}

			url += (url.indexOf('?') === -1 ? '?' : '&') + serializedParams;
		}

		return url;
	};

	function InterceptorManager() {
		this.handlers = [];
	}

	/**
	 * Add a new interceptor to the stack
	 *
	 * @param {Function} fulfilled The function to handle `then` for a `Promise`
	 * @param {Function} rejected The function to handle `reject` for a `Promise`
	 *
	 * @return {Number} An ID used to remove interceptor later
	 */
	InterceptorManager.prototype.use = function use(fulfilled, rejected) {
		this.handlers.push({
			fulfilled: fulfilled,
			rejected: rejected
		});
		return this.handlers.length - 1;
	};

	/**
	 * Remove an interceptor from the stack
	 *
	 * @param {Number} id The ID that was returned by `use`
	 */
	InterceptorManager.prototype.eject = function eject(id) {
		if (this.handlers[id]) {
			this.handlers[id] = null;
		}
	};

	/**
	 * Iterate over all the registered interceptors
	 *
	 * This method is particularly useful for skipping over any
	 * interceptors that may have become `null` calling `eject`.
	 *
	 * @param {Function} fn The function to call for each interceptor
	 */
	InterceptorManager.prototype.forEach = function forEach(fn) {
		utils.forEach(this.handlers, function forEachHandler(h) {
			if (h !== null) {
				fn(h);
			}
		});
	};

	var InterceptorManager_1 = InterceptorManager;

	/**
	 * Transform the data for a request or a response
	 *
	 * @param {Object|String} data The data to be transformed
	 * @param {Array} headers The headers for the request or response
	 * @param {Array|Function} fns A single function or Array of functions
	 * @returns {*} The resulting transformed data
	 */
	var transformData = function transformData(data, headers, fns) {
		/*eslint no-param-reassign:0*/
		utils.forEach(fns, function transform(fn) {
			data = fn(data, headers);
		});

		return data;
	};

	var isCancel = function isCancel(value) {
		return !!(value && value.__CANCEL__);
	};

	var normalizeHeaderName = function normalizeHeaderName(headers, normalizedName) {
		utils.forEach(headers, function processHeader(value, name) {
			if (name !== normalizedName && name.toUpperCase() === normalizedName.toUpperCase()) {
				headers[normalizedName] = value;
				delete headers[name];
			}
		});
	};

	/**
	 * Update an Error with the specified config, error code, and response.
	 *
	 * @param {Error} error The error to update.
	 * @param {Object} config The config.
	 * @param {string} [code] The error code (for example, 'ECONNABORTED').
	 * @param {Object} [request] The request.
	 * @param {Object} [response] The response.
	 * @returns {Error} The error.
	 */
	var enhanceError = function enhanceError(error, config, code, request, response) {
		error.config = config;
		if (code) {
			error.code = code;
		}

		error.request = request;
		error.response = response;
		error.isAxiosError = true;

		error.toJSON = function toJSON() {
			return {
				// Standard
				message: this.message,
				name: this.name,
				// Microsoft
				description: this.description,
				number: this.number,
				// Mozilla
				fileName: this.fileName,
				lineNumber: this.lineNumber,
				columnNumber: this.columnNumber,
				stack: this.stack,
				// Axios
				config: this.config,
				code: this.code
			};
		};
		return error;
	};

	/**
	 * Create an Error with the specified message, config, error code, request and response.
	 *
	 * @param {string} message The error message.
	 * @param {Object} config The config.
	 * @param {string} [code] The error code (for example, 'ECONNABORTED').
	 * @param {Object} [request] The request.
	 * @param {Object} [response] The response.
	 * @returns {Error} The created error.
	 */
	var createError = function createError(message, config, code, request, response) {
		var error = new Error(message);
		return enhanceError(error, config, code, request, response);
	};

	/**
	 * Resolve or reject a Promise based on response status.
	 *
	 * @param {Function} resolve A function that resolves the promise.
	 * @param {Function} reject A function that rejects the promise.
	 * @param {object} response The response.
	 */
	var settle = function settle(resolve, reject, response) {
		var validateStatus = response.config.validateStatus;
		if (!response.status || !validateStatus || validateStatus(response.status)) {
			resolve(response);
		} else {
			reject(createError(
				'Request failed with status code ' + response.status,
				response.config,
				null,
				response.request,
				response
			));
		}
	};

	var cookies = (
		utils.isStandardBrowserEnv() ?

			// Standard browser envs support document.cookie
			(function standardBrowserEnv() {
				return {
					write: function write(name, value, expires, path, domain, secure) {
						var cookie = [];
						cookie.push(name + '=' + encodeURIComponent(value));

						if (utils.isNumber(expires)) {
							cookie.push('expires=' + new Date(expires).toGMTString());
						}

						if (utils.isString(path)) {
							cookie.push('path=' + path);
						}

						if (utils.isString(domain)) {
							cookie.push('domain=' + domain);
						}

						if (secure === true) {
							cookie.push('secure');
						}

						document.cookie = cookie.join('; ');
					},

					read: function read(name) {
						var match = document.cookie.match(new RegExp('(^|;\\s*)(' + name + ')=([^;]*)'));
						return (match ? decodeURIComponent(match[3]) : null);
					},

					remove: function remove(name) {
						this.write(name, '', Date.now() - 86400000);
					}
				};
			})() :

			// Non standard browser env (web workers, react-native) lack needed support.
			(function nonStandardBrowserEnv() {
				return {
					write: function write() { },
					read: function read() { return null; },
					remove: function remove() { }
				};
			})()
	);

	/**
	 * Determines whether the specified URL is absolute
	 *
	 * @param {string} url The URL to test
	 * @returns {boolean} True if the specified URL is absolute, otherwise false
	 */
	var isAbsoluteURL = function isAbsoluteURL(url) {
		// A URL is considered absolute if it begins with "<scheme>://" or "//" (protocol-relative URL).
		// RFC 3986 defines scheme name as a sequence of characters beginning with a letter and followed
		// by any combination of letters, digits, plus, period, or hyphen.
		return /^([a-z][a-z\d\+\-\.]*:)?\/\//i.test(url);
	};

	/**
	 * Creates a new URL by combining the specified URLs
	 *
	 * @param {string} baseURL The base URL
	 * @param {string} relativeURL The relative URL
	 * @returns {string} The combined URL
	 */
	var combineURLs = function combineURLs(baseURL, relativeURL) {
		return relativeURL
			? baseURL.replace(/\/+$/, '') + '/' + relativeURL.replace(/^\/+/, '')
			: baseURL;
	};

	/**
	 * Creates a new URL by combining the baseURL with the requestedURL,
	 * only when the requestedURL is not already an absolute URL.
	 * If the requestURL is absolute, this function returns the requestedURL untouched.
	 *
	 * @param {string} baseURL The base URL
	 * @param {string} requestedURL Absolute or relative URL to combine
	 * @returns {string} The combined full path
	 */
	var buildFullPath = function buildFullPath(baseURL, requestedURL) {
		if (baseURL && !isAbsoluteURL(requestedURL)) {
			return combineURLs(baseURL, requestedURL);
		}
		return requestedURL;
	};

	// Headers whose duplicates are ignored by node
	// c.f. https://nodejs.org/api/http.html#http_message_headers
	var ignoreDuplicateOf = [
		'age', 'authorization', 'content-length', 'content-type', 'etag',
		'expires', 'from', 'host', 'if-modified-since', 'if-unmodified-since',
		'last-modified', 'location', 'max-forwards', 'proxy-authorization',
		'referer', 'retry-after', 'user-agent'
	];

	/**
	 * Parse headers into an object
	 *
	 * ```
	 * Date: Wed, 27 Aug 2014 08:58:49 GMT
	 * Content-Type: application/json
	 * Connection: keep-alive
	 * Transfer-Encoding: chunked
	 * ```
	 *
	 * @param {String} headers Headers needing to be parsed
	 * @returns {Object} Headers parsed into an object
	 */
	var parseHeaders = function parseHeaders(headers) {
		var parsed = {};
		var key;
		var val;
		var i;

		if (!headers) { return parsed; }

		utils.forEach(headers.split('\n'), function parser(line) {
			i = line.indexOf(':');
			key = utils.trim(line.substr(0, i)).toLowerCase();
			val = utils.trim(line.substr(i + 1));

			if (key) {
				if (parsed[key] && ignoreDuplicateOf.indexOf(key) >= 0) {
					return;
				}
				if (key === 'set-cookie') {
					parsed[key] = (parsed[key] ? parsed[key] : []).concat([val]);
				} else {
					parsed[key] = parsed[key] ? parsed[key] + ', ' + val : val;
				}
			}
		});

		return parsed;
	};

	var isURLSameOrigin = (
		utils.isStandardBrowserEnv() ?

			// Standard browser envs have full support of the APIs needed to test
			// whether the request URL is of the same origin as current location.
			(function standardBrowserEnv() {
				var msie = /(msie|trident)/i.test(navigator.userAgent);
				var urlParsingNode = document.createElement('a');
				var originURL;

				/**
			  * Parse a URL to discover it's components
			  *
			  * @param {String} url The URL to be parsed
			  * @returns {Object}
			  */
				function resolveURL(url) {
					var href = url;

					if (msie) {
						// IE needs attribute set twice to normalize properties
						urlParsingNode.setAttribute('href', href);
						href = urlParsingNode.href;
					}

					urlParsingNode.setAttribute('href', href);

					// urlParsingNode provides the UrlUtils interface - http://url.spec.whatwg.org/#urlutils
					return {
						href: urlParsingNode.href,
						protocol: urlParsingNode.protocol ? urlParsingNode.protocol.replace(/:$/, '') : '',
						host: urlParsingNode.host,
						search: urlParsingNode.search ? urlParsingNode.search.replace(/^\?/, '') : '',
						hash: urlParsingNode.hash ? urlParsingNode.hash.replace(/^#/, '') : '',
						hostname: urlParsingNode.hostname,
						port: urlParsingNode.port,
						pathname: (urlParsingNode.pathname.charAt(0) === '/') ?
							urlParsingNode.pathname :
							'/' + urlParsingNode.pathname
					};
				}

				originURL = resolveURL(window.location.href);

				/**
			  * Determine if a URL shares the same origin as the current location
			  *
			  * @param {String} requestURL The URL to test
			  * @returns {boolean} True if URL shares the same origin, otherwise false
			  */
				return function isURLSameOrigin(requestURL) {
					var parsed = (utils.isString(requestURL)) ? resolveURL(requestURL) : requestURL;
					return (parsed.protocol === originURL.protocol &&
						parsed.host === originURL.host);
				};
			})() :

			// Non standard browser envs (web workers, react-native) lack needed support.
			(function nonStandardBrowserEnv() {
				return function isURLSameOrigin() {
					return true;
				};
			})()
	);

	var xhr = function xhrAdapter(config) {
		return new Promise(function dispatchXhrRequest(resolve, reject) {
			var requestData = config.data;
			var requestHeaders = config.headers;

			if (utils.isFormData(requestData)) {
				delete requestHeaders['Content-Type']; // Let the browser set it
			}

			var request = new XMLHttpRequest();

			// HTTP basic authentication
			if (config.auth) {
				var username = config.auth.username || '';
				var password = config.auth.password ? unescape(encodeURIComponent(config.auth.password)) : '';
				requestHeaders.Authorization = 'Basic ' + btoa(username + ':' + password);
			}

			var fullPath = buildFullPath(config.baseURL, config.url);
			request.open(config.method.toUpperCase(), buildURL(fullPath, config.params, config.paramsSerializer), true);

			// Set the request timeout in MS
			request.timeout = config.timeout;

			// Listen for ready state
			request.onreadystatechange = function handleLoad() {
				if (!request || request.readyState !== 4) {
					return;
				}

				// The request errored out and we didn't get a response, this will be
				// handled by onerror instead
				// With one exception: request that using file: protocol, most browsers
				// will return status as 0 even though it's a successful request
				if (request.status === 0 && !(request.responseURL && request.responseURL.indexOf('file:') === 0)) {
					return;
				}

				// Prepare the response
				var responseHeaders = 'getAllResponseHeaders' in request ? parseHeaders(request.getAllResponseHeaders()) : null;
				var responseData = !config.responseType || config.responseType === 'text' ? request.responseText : request.response;
				var response = {
					data: responseData,
					status: request.status,
					statusText: request.statusText,
					headers: responseHeaders,
					config: config,
					request: request
				};

				settle(resolve, reject, response);

				// Clean up request
				request = null;
			};

			// Handle browser request cancellation (as opposed to a manual cancellation)
			request.onabort = function handleAbort() {
				if (!request) {
					return;
				}

				reject(createError('Request aborted', config, 'ECONNABORTED', request));

				// Clean up request
				request = null;
			};

			// Handle low level network errors
			request.onerror = function handleError() {
				// Real errors are hidden from us by the browser
				// onerror should only fire if it's a network error
				reject(createError('Network Error', config, null, request));

				// Clean up request
				request = null;
			};

			// Handle timeout
			request.ontimeout = function handleTimeout() {
				var timeoutErrorMessage = 'timeout of ' + config.timeout + 'ms exceeded';
				if (config.timeoutErrorMessage) {
					timeoutErrorMessage = config.timeoutErrorMessage;
				}
				reject(createError(timeoutErrorMessage, config, 'ECONNABORTED',
					request));

				// Clean up request
				request = null;
			};

			// Add xsrf header
			// This is only done if running in a standard browser environment.
			// Specifically not if we're in a web worker, or react-native.
			if (utils.isStandardBrowserEnv()) {
				// Add xsrf header
				var xsrfValue = (config.withCredentials || isURLSameOrigin(fullPath)) && config.xsrfCookieName ?
					cookies.read(config.xsrfCookieName) :
					undefined;

				if (xsrfValue) {
					requestHeaders[config.xsrfHeaderName] = xsrfValue;
				}
			}

			// Add headers to the request
			if ('setRequestHeader' in request) {
				utils.forEach(requestHeaders, function setRequestHeader(val, key) {
					if (typeof requestData === 'undefined' && key.toLowerCase() === 'content-type') {
						// Remove Content-Type if data is undefined
						delete requestHeaders[key];
					} else {
						// Otherwise add header to the request
						request.setRequestHeader(key, val);
					}
				});
			}

			// Add withCredentials to request if needed
			if (!utils.isUndefined(config.withCredentials)) {
				request.withCredentials = !!config.withCredentials;
			}

			// Add responseType to request if needed
			if (config.responseType) {
				try {
					request.responseType = config.responseType;
				} catch (e) {
					// Expected DOMException thrown by browsers not compatible XMLHttpRequest Level 2.
					// But, this can be suppressed for 'json' type as it can be parsed by default 'transformResponse' function.
					if (config.responseType !== 'json') {
						throw e;
					}
				}
			}

			// Handle progress if needed
			if (typeof config.onDownloadProgress === 'function') {
				request.addEventListener('progress', config.onDownloadProgress);
			}

			// Not all browsers support upload events
			if (typeof config.onUploadProgress === 'function' && request.upload) {
				request.upload.addEventListener('progress', config.onUploadProgress);
			}

			if (config.cancelToken) {
				// Handle cancellation
				config.cancelToken.promise.then(function onCanceled(cancel) {
					if (!request) {
						return;
					}

					request.abort();
					reject(cancel);
					// Clean up request
					request = null;
				});
			}

			if (!requestData) {
				requestData = null;
			}

			// Send the request
			request.send(requestData);
		});
	};

	var DEFAULT_CONTENT_TYPE = {
		'Content-Type': 'application/x-www-form-urlencoded'
	};

	function setContentTypeIfUnset(headers, value) {
		if (!utils.isUndefined(headers) && utils.isUndefined(headers['Content-Type'])) {
			headers['Content-Type'] = value;
		}
	}

	function getDefaultAdapter() {
		var adapter;
		if (typeof XMLHttpRequest !== 'undefined') {
			// For browsers use XHR adapter
			adapter = xhr;
		} else if (typeof process !== 'undefined' && Object.prototype.toString.call(process) === '[object process]') {
			// For node use HTTP adapter
			adapter = xhr;
		}
		return adapter;
	}

	var defaults = {
		adapter: getDefaultAdapter(),

		transformRequest: [function transformRequest(data, headers) {
			normalizeHeaderName(headers, 'Accept');
			normalizeHeaderName(headers, 'Content-Type');
			if (utils.isFormData(data) ||
				utils.isArrayBuffer(data) ||
				utils.isBuffer(data) ||
				utils.isStream(data) ||
				utils.isFile(data) ||
				utils.isBlob(data)
			) {
				return data;
			}
			if (utils.isArrayBufferView(data)) {
				return data.buffer;
			}
			if (utils.isURLSearchParams(data)) {
				setContentTypeIfUnset(headers, 'application/x-www-form-urlencoded;charset=utf-8');
				return data.toString();
			}
			if (utils.isObject(data)) {
				setContentTypeIfUnset(headers, 'application/json;charset=utf-8');
				return JSON.stringify(data);
			}
			return data;
		}],

		transformResponse: [function transformResponse(data) {
			/*eslint no-param-reassign:0*/
			if (typeof data === 'string') {
				try {
					data = JSON.parse(data);
				} catch (e) { /* Ignore */ }
			}
			return data;
		}],

		/**
		 * A timeout in milliseconds to abort a request. If set to 0 (default) a
		 * timeout is not created.
		 */
		timeout: 0,

		xsrfCookieName: 'XSRF-TOKEN',
		xsrfHeaderName: 'X-XSRF-TOKEN',

		maxContentLength: -1,
		maxBodyLength: -1,

		validateStatus: function validateStatus(status) {
			return status >= 200 && status < 300;
		}
	};

	defaults.headers = {
		common: {
			'Accept': 'application/json, text/plain, */*'
		}
	};

	utils.forEach(['delete', 'get', 'head'], function forEachMethodNoData(method) {
		defaults.headers[method] = {};
	});

	utils.forEach(['post', 'put', 'patch'], function forEachMethodWithData(method) {
		defaults.headers[method] = utils.merge(DEFAULT_CONTENT_TYPE);
	});

	var defaults_1 = defaults;

	/**
	 * Throws a `Cancel` if cancellation has been requested.
	 */
	function throwIfCancellationRequested(config) {
		if (config.cancelToken) {
			config.cancelToken.throwIfRequested();
		}
	}

	/**
	 * Dispatch a request to the server using the configured adapter.
	 *
	 * @param {object} config The config that is to be used for the request
	 * @returns {Promise} The Promise to be fulfilled
	 */
	var dispatchRequest = function dispatchRequest(config) {
		throwIfCancellationRequested(config);

		// Ensure headers exist
		config.headers = config.headers || {};

		// Transform request data
		config.data = transformData(
			config.data,
			config.headers,
			config.transformRequest
		);

		// Flatten headers
		config.headers = utils.merge(
			config.headers.common || {},
			config.headers[config.method] || {},
			config.headers
		);

		utils.forEach(
			['delete', 'get', 'head', 'post', 'put', 'patch', 'common'],
			function cleanHeaderConfig(method) {
				delete config.headers[method];
			}
		);

		var adapter = config.adapter || defaults_1.adapter;

		return adapter(config).then(function onAdapterResolution(response) {
			throwIfCancellationRequested(config);

			// Transform response data
			response.data = transformData(
				response.data,
				response.headers,
				config.transformResponse
			);

			return response;
		}, function onAdapterRejection(reason) {
			if (!isCancel(reason)) {
				throwIfCancellationRequested(config);

				// Transform response data
				if (reason && reason.response) {
					reason.response.data = transformData(
						reason.response.data,
						reason.response.headers,
						config.transformResponse
					);
				}
			}

			return Promise.reject(reason);
		});
	};

	/**
	 * Config-specific merge-function which creates a new config-object
	 * by merging two configuration objects together.
	 *
	 * @param {Object} config1
	 * @param {Object} config2
	 * @returns {Object} New object resulting from merging config2 to config1
	 */
	var mergeConfig = function mergeConfig(config1, config2) {
		// eslint-disable-next-line no-param-reassign
		config2 = config2 || {};
		var config = {};

		var valueFromConfig2Keys = ['url', 'method', 'data'];
		var mergeDeepPropertiesKeys = ['headers', 'auth', 'proxy', 'params'];
		var defaultToConfig2Keys = [
			'baseURL', 'transformRequest', 'transformResponse', 'paramsSerializer',
			'timeout', 'timeoutMessage', 'withCredentials', 'adapter', 'responseType', 'xsrfCookieName',
			'xsrfHeaderName', 'onUploadProgress', 'onDownloadProgress', 'decompress',
			'maxContentLength', 'maxBodyLength', 'maxRedirects', 'transport', 'httpAgent',
			'httpsAgent', 'cancelToken', 'socketPath', 'responseEncoding'
		];
		var directMergeKeys = ['validateStatus'];

		function getMergedValue(target, source) {
			if (utils.isPlainObject(target) && utils.isPlainObject(source)) {
				return utils.merge(target, source);
			} else if (utils.isPlainObject(source)) {
				return utils.merge({}, source);
			} else if (utils.isArray(source)) {
				return source.slice();
			}
			return source;
		}

		function mergeDeepProperties(prop) {
			if (!utils.isUndefined(config2[prop])) {
				config[prop] = getMergedValue(config1[prop], config2[prop]);
			} else if (!utils.isUndefined(config1[prop])) {
				config[prop] = getMergedValue(undefined, config1[prop]);
			}
		}

		utils.forEach(valueFromConfig2Keys, function valueFromConfig2(prop) {
			if (!utils.isUndefined(config2[prop])) {
				config[prop] = getMergedValue(undefined, config2[prop]);
			}
		});

		utils.forEach(mergeDeepPropertiesKeys, mergeDeepProperties);

		utils.forEach(defaultToConfig2Keys, function defaultToConfig2(prop) {
			if (!utils.isUndefined(config2[prop])) {
				config[prop] = getMergedValue(undefined, config2[prop]);
			} else if (!utils.isUndefined(config1[prop])) {
				config[prop] = getMergedValue(undefined, config1[prop]);
			}
		});

		utils.forEach(directMergeKeys, function merge(prop) {
			if (prop in config2) {
				config[prop] = getMergedValue(config1[prop], config2[prop]);
			} else if (prop in config1) {
				config[prop] = getMergedValue(undefined, config1[prop]);
			}
		});

		var axiosKeys = valueFromConfig2Keys
			.concat(mergeDeepPropertiesKeys)
			.concat(defaultToConfig2Keys)
			.concat(directMergeKeys);

		var otherKeys = Object
			.keys(config1)
			.concat(Object.keys(config2))
			.filter(function filterAxiosKeys(key) {
				return axiosKeys.indexOf(key) === -1;
			});

		utils.forEach(otherKeys, mergeDeepProperties);

		return config;
	};

	/**
	 * Create a new instance of Axios
	 *
	 * @param {Object} instanceConfig The default config for the instance
	 */
	function Axios(instanceConfig) {
		this.defaults = instanceConfig;
		this.interceptors = {
			request: new InterceptorManager_1(),
			response: new InterceptorManager_1()
		};
	}

	/**
	 * Dispatch a request
	 *
	 * @param {Object} config The config specific for this request (merged with this.defaults)
	 */
	Axios.prototype.request = function request(config) {
		/*eslint no-param-reassign:0*/
		// Allow for axios('example/url'[, config]) a la fetch API
		if (typeof config === 'string') {
			config = arguments[1] || {};
			config.url = arguments[0];
		} else {
			config = config || {};
		}

		config = mergeConfig(this.defaults, config);

		// Set config.method
		if (config.method) {
			config.method = config.method.toLowerCase();
		} else if (this.defaults.method) {
			config.method = this.defaults.method.toLowerCase();
		} else {
			config.method = 'get';
		}

		// Hook up interceptors middleware
		var chain = [dispatchRequest, undefined];
		var promise = Promise.resolve(config);

		this.interceptors.request.forEach(function unshiftRequestInterceptors(interceptor) {
			chain.unshift(interceptor.fulfilled, interceptor.rejected);
		});

		this.interceptors.response.forEach(function pushResponseInterceptors(interceptor) {
			chain.push(interceptor.fulfilled, interceptor.rejected);
		});

		while (chain.length) {
			promise = promise.then(chain.shift(), chain.shift());
		}

		return promise;
	};

	Axios.prototype.getUri = function getUri(config) {
		config = mergeConfig(this.defaults, config);
		return buildURL(config.url, config.params, config.paramsSerializer).replace(/^\?/, '');
	};

	// Provide aliases for supported request methods
	utils.forEach(['delete', 'get', 'head', 'options'], function forEachMethodNoData(method) {
		/*eslint func-names:0*/
		Axios.prototype[method] = function (url, config) {
			return this.request(mergeConfig(config || {}, {
				method: method,
				url: url,
				data: (config || {}).data
			}));
		};
	});

	utils.forEach(['post', 'put', 'patch'], function forEachMethodWithData(method) {
		/*eslint func-names:0*/
		Axios.prototype[method] = function (url, data, config) {
			return this.request(mergeConfig(config || {}, {
				method: method,
				url: url,
				data: data
			}));
		};
	});

	var Axios_1 = Axios;

	/**
	 * A `Cancel` is an object that is thrown when an operation is canceled.
	 *
	 * @class
	 * @param {string=} message The message.
	 */
	function Cancel(message) {
		this.message = message;
	}

	Cancel.prototype.toString = function toString() {
		return 'Cancel' + (this.message ? ': ' + this.message : '');
	};

	Cancel.prototype.__CANCEL__ = true;

	var Cancel_1 = Cancel;

	/**
	 * A `CancelToken` is an object that can be used to request cancellation of an operation.
	 *
	 * @class
	 * @param {Function} executor The executor function.
	 */
	function CancelToken(executor) {
		if (typeof executor !== 'function') {
			throw new TypeError('executor must be a function.');
		}

		var resolvePromise;
		this.promise = new Promise(function promiseExecutor(resolve) {
			resolvePromise = resolve;
		});

		var token = this;
		executor(function cancel(message) {
			if (token.reason) {
				// Cancellation has already been requested
				return;
			}

			token.reason = new Cancel_1(message);
			resolvePromise(token.reason);
		});
	}

	/**
	 * Throws a `Cancel` if cancellation has been requested.
	 */
	CancelToken.prototype.throwIfRequested = function throwIfRequested() {
		if (this.reason) {
			throw this.reason;
		}
	};

	/**
	 * Returns an object that contains a new `CancelToken` and a function that, when called,
	 * cancels the `CancelToken`.
	 */
	CancelToken.source = function source() {
		var cancel;
		var token = new CancelToken(function executor(c) {
			cancel = c;
		});
		return {
			token: token,
			cancel: cancel
		};
	};

	var CancelToken_1 = CancelToken;

	/**
	 * Syntactic sugar for invoking a function and expanding an array for arguments.
	 *
	 * Common use case would be to use `Function.prototype.apply`.
	 *
	 *  ```js
	 *  function f(x, y, z) {}
	 *  var args = [1, 2, 3];
	 *  f.apply(null, args);
	 *  ```
	 *
	 * With `spread` this example can be re-written.
	 *
	 *  ```js
	 *  spread(function(x, y, z) {})([1, 2, 3]);
	 *  ```
	 *
	 * @param {Function} callback
	 * @returns {Function}
	 */
	var spread = function spread(callback) {
		return function wrap(arr) {
			return callback.apply(null, arr);
		};
	};

	/**
	 * Determines whether the payload is an error thrown by Axios
	 *
	 * @param {*} payload The value to test
	 * @returns {boolean} True if the payload is an error thrown by Axios, otherwise false
	 */
	var isAxiosError = function isAxiosError(payload) {
		return (typeof payload === 'object') && (payload.isAxiosError === true);
	};

	/**
	 * Create an instance of Axios
	 *
	 * @param {Object} defaultConfig The default config for the instance
	 * @return {Axios} A new instance of Axios
	 */
	function createInstance(defaultConfig) {
		var context = new Axios_1(defaultConfig);
		var instance = bind(Axios_1.prototype.request, context);

		// Copy axios.prototype to instance
		utils.extend(instance, Axios_1.prototype, context);

		// Copy context to instance
		utils.extend(instance, context);

		return instance;
	}

	// Create the default instance to be exported
	var axios$1 = createInstance(defaults_1);

	// Expose Axios class to allow class inheritance
	axios$1.Axios = Axios_1;

	// Factory for creating new instances
	axios$1.create = function create(instanceConfig) {
		return createInstance(mergeConfig(axios$1.defaults, instanceConfig));
	};

	// Expose Cancel & CancelToken
	axios$1.Cancel = Cancel_1;
	axios$1.CancelToken = CancelToken_1;
	axios$1.isCancel = isCancel;

	// Expose all/spread
	axios$1.all = function all(promises) {
		return Promise.all(promises);
	};
	axios$1.spread = spread;

	// Expose isAxiosError
	axios$1.isAxiosError = isAxiosError;

	var axios_1 = axios$1;

	// Allow use of default import syntax in TypeScript
	var _default = axios$1;
	axios_1.default = _default;

	var axios = axios_1;

	/* src/components/Banner.svelte generated by Svelte v3.42.3 */
	const file$4 = "src/components/Banner.svelte";

	// (14:0) {#if show == true}
	function create_if_block$2(ctx) {
		let pannablebox;
		let current;

		pannablebox = new PannableBox({
			props: {
				$$slots: { default: [create_default_slot$2] },
				$$scope: { ctx }
			},
			$$inline: true
		});

		const block = {
			c: function create() {
				create_component(pannablebox.$$.fragment);
			},
			m: function mount(target, anchor) {
				mount_component(pannablebox, target, anchor);
				current = true;
			},
			p: function update(ctx, dirty) {
				const pannablebox_changes = {};

				if (dirty & /*$$scope, message, onClose, titleColor, title*/ 55) {
					pannablebox_changes.$$scope = { dirty, ctx };
				}

				pannablebox.$set(pannablebox_changes);
			},
			i: function intro(local) {
				if (current) return;
				transition_in(pannablebox.$$.fragment, local);
				current = true;
			},
			o: function outro(local) {
				transition_out(pannablebox.$$.fragment, local);
				current = false;
			},
			d: function destroy(detaching) {
				destroy_component(pannablebox, detaching);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_if_block$2.name,
			type: "if",
			source: "(14:0) {#if show == true}",
			ctx
		});

		return block;
	}

	// (16:0) <PannableBox>
	function create_default_slot$2(ctx) {
		let div3;
		let div2;
		let div0;
		let h1;
		let t0;
		let t1;
		let div1;
		let button;
		let t3;
		let p;
		let t4;
		let mounted;
		let dispose;

		const block = {
			c: function create() {
				div3 = element("div");
				div2 = element("div");
				div0 = element("div");
				h1 = element("h1");
				t0 = text(/*title*/ ctx[0]);
				t1 = space();
				div1 = element("div");
				button = element("button");
				button.textContent = "x";
				t3 = space();
				p = element("p");
				t4 = text(/*message*/ ctx[2]);
				add_location(h1, file$4, 23, 16, 625);
				attr_dev(div0, "class", "text-xl font-bold svelte-1w2fjet");
				toggle_class(div0, "b-title-red", /*titleColor*/ ctx[1] == 'red');
				toggle_class(div0, "b-title-green", /*titleColor*/ ctx[1] == 'green');
				add_location(div0, file$4, 18, 12, 427);
				add_location(button, file$4, 26, 16, 750);
				attr_dev(div1, "class", "flex flex-col justify-start leading-none h-1/4");
				add_location(div1, file$4, 25, 12, 673);
				attr_dev(div2, "class", "flex flex-row justify-between");
				add_location(div2, file$4, 17, 8, 371);
				attr_dev(p, "class", "mt-2");
				add_location(p, file$4, 29, 8, 832);
				attr_dev(div3, "class", "p-3 bg-gray-100 rounded-lg no-select svelte-1w2fjet");
				add_location(div3, file$4, 16, 4, 312);
			},
			m: function mount(target, anchor) {
				insert_dev(target, div3, anchor);
				append_dev(div3, div2);
				append_dev(div2, div0);
				append_dev(div0, h1);
				append_dev(h1, t0);
				append_dev(div2, t1);
				append_dev(div2, div1);
				append_dev(div1, button);
				append_dev(div3, t3);
				append_dev(div3, p);
				append_dev(p, t4);

				if (!mounted) {
					dispose = listen_dev(
						button,
						"click",
						function () {
							if (is_function(/*onClose*/ ctx[4])) /*onClose*/ ctx[4].apply(this, arguments);
						},
						false,
						false,
						false
					);

					mounted = true;
				}
			},
			p: function update(new_ctx, dirty) {
				ctx = new_ctx;
				if (dirty & /*title*/ 1) set_data_dev(t0, /*title*/ ctx[0]);

				if (dirty & /*titleColor*/ 2) {
					toggle_class(div0, "b-title-red", /*titleColor*/ ctx[1] == 'red');
				}

				if (dirty & /*titleColor*/ 2) {
					toggle_class(div0, "b-title-green", /*titleColor*/ ctx[1] == 'green');
				}

				if (dirty & /*message*/ 4) set_data_dev(t4, /*message*/ ctx[2]);
			},
			d: function destroy(detaching) {
				if (detaching) detach_dev(div3);
				mounted = false;
				dispose();
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_default_slot$2.name,
			type: "slot",
			source: "(16:0) <PannableBox>",
			ctx
		});

		return block;
	}

	function create_fragment$4(ctx) {
		let if_block_anchor;
		let current;
		let if_block = /*show*/ ctx[3] == true && create_if_block$2(ctx);

		const block = {
			c: function create() {
				if (if_block) if_block.c();
				if_block_anchor = empty();
			},
			l: function claim(nodes) {
				throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			},
			m: function mount(target, anchor) {
				if (if_block) if_block.m(target, anchor);
				insert_dev(target, if_block_anchor, anchor);
				current = true;
			},
			p: function update(ctx, [dirty]) {
				if (/*show*/ ctx[3] == true) {
					if (if_block) {
						if_block.p(ctx, dirty);

						if (dirty & /*show*/ 8) {
							transition_in(if_block, 1);
						}
					} else {
						if_block = create_if_block$2(ctx);
						if_block.c();
						transition_in(if_block, 1);
						if_block.m(if_block_anchor.parentNode, if_block_anchor);
					}
				} else if (if_block) {
					group_outros();

					transition_out(if_block, 1, 1, () => {
						if_block = null;
					});

					check_outros();
				}
			},
			i: function intro(local) {
				if (current) return;
				transition_in(if_block);
				current = true;
			},
			o: function outro(local) {
				transition_out(if_block);
				current = false;
			},
			d: function destroy(detaching) {
				if (if_block) if_block.d(detaching);
				if (detaching) detach_dev(if_block_anchor);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_fragment$4.name,
			type: "component",
			source: "",
			ctx
		});

		return block;
	}

	function instance$4($$self, $$props, $$invalidate) {
		let { $$slots: slots = {}, $$scope } = $$props;
		validate_slots('Banner', slots, []);
		let { title } = $$props;
		let { titleColor } = $$props;
		let { message } = $$props;
		let { show } = $$props;
		let { onClose } = $$props;
		const writable_props = ['title', 'titleColor', 'message', 'show', 'onClose'];

		Object.keys($$props).forEach(key => {
			if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Banner> was created with unknown prop '${key}'`);
		});

		$$self.$$set = $$props => {
			if ('title' in $$props) $$invalidate(0, title = $$props.title);
			if ('titleColor' in $$props) $$invalidate(1, titleColor = $$props.titleColor);
			if ('message' in $$props) $$invalidate(2, message = $$props.message);
			if ('show' in $$props) $$invalidate(3, show = $$props.show);
			if ('onClose' in $$props) $$invalidate(4, onClose = $$props.onClose);
		};

		$$self.$capture_state = () => ({
			PannableBox,
			Button,
			onDestroy,
			title,
			titleColor,
			message,
			show,
			onClose
		});

		$$self.$inject_state = $$props => {
			if ('title' in $$props) $$invalidate(0, title = $$props.title);
			if ('titleColor' in $$props) $$invalidate(1, titleColor = $$props.titleColor);
			if ('message' in $$props) $$invalidate(2, message = $$props.message);
			if ('show' in $$props) $$invalidate(3, show = $$props.show);
			if ('onClose' in $$props) $$invalidate(4, onClose = $$props.onClose);
		};

		if ($$props && "$$inject" in $$props) {
			$$self.$inject_state($$props.$$inject);
		}

		return [title, titleColor, message, show, onClose];
	}

	class Banner extends SvelteComponentDev {
		constructor(options) {
			super(options);

			init(this, options, instance$4, create_fragment$4, safe_not_equal, {
				title: 0,
				titleColor: 1,
				message: 2,
				show: 3,
				onClose: 4
			});

			dispatch_dev("SvelteRegisterComponent", {
				component: this,
				tagName: "Banner",
				options,
				id: create_fragment$4.name
			});

			const { ctx } = this.$$;
			const props = options.props || {};

			if (/*title*/ ctx[0] === undefined && !('title' in props)) {
				console.warn("<Banner> was created without expected prop 'title'");
			}

			if (/*titleColor*/ ctx[1] === undefined && !('titleColor' in props)) {
				console.warn("<Banner> was created without expected prop 'titleColor'");
			}

			if (/*message*/ ctx[2] === undefined && !('message' in props)) {
				console.warn("<Banner> was created without expected prop 'message'");
			}

			if (/*show*/ ctx[3] === undefined && !('show' in props)) {
				console.warn("<Banner> was created without expected prop 'show'");
			}

			if (/*onClose*/ ctx[4] === undefined && !('onClose' in props)) {
				console.warn("<Banner> was created without expected prop 'onClose'");
			}
		}

		get title() {
			throw new Error("<Banner>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set title(value) {
			throw new Error("<Banner>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get titleColor() {
			throw new Error("<Banner>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set titleColor(value) {
			throw new Error("<Banner>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get message() {
			throw new Error("<Banner>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set message(value) {
			throw new Error("<Banner>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get show() {
			throw new Error("<Banner>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set show(value) {
			throw new Error("<Banner>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		get onClose() {
			throw new Error("<Banner>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set onClose(value) {
			throw new Error("<Banner>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}
	}

	/* src/routes/Home.svelte generated by Svelte v3.42.3 */

	const { console: console_1$1 } = globals;
	const file$3 = "src/routes/Home.svelte";

	// (87:0) {:else}
	function create_else_block$1(ctx) {
		let div1;
		let div0;
		let t1;
		let button;
		let t3;
		let t4;
		let t5;
		let t6;
		let div2;
		let pannablebox0;
		let t7;
		let pannablebox1;
		let current;
		let mounted;
		let dispose;
		let if_block0 = /*loader*/ ctx[3] && create_if_block_5(ctx);
		let if_block1 = /*importError*/ ctx[7] && create_if_block_4(ctx);
		let if_block2 = /*banner*/ ctx[4] && create_if_block_3(ctx);

		pannablebox0 = new PannableBox({
			props: {
				$$slots: { default: [create_default_slot_1$1] },
				$$scope: { ctx }
			},
			$$inline: true
		});

		pannablebox1 = new PannableBox({
			props: {
				$$slots: { default: [create_default_slot$1] },
				$$scope: { ctx }
			},
			$$inline: true
		});

		const block = {
			c: function create() {
				div1 = element("div");
				div0 = element("div");
				div0.textContent = "HELLO";
				t1 = space();
				button = element("button");
				button.textContent = "LOG OUT";
				t3 = space();
				if (if_block0) if_block0.c();
				t4 = space();
				if (if_block1) if_block1.c();
				t5 = space();
				if (if_block2) if_block2.c();
				t6 = space();
				div2 = element("div");
				create_component(pannablebox0.$$.fragment);
				t7 = space();
				create_component(pannablebox1.$$.fragment);
				attr_dev(div0, "class", "pl-5 text-2xl font-bold");
				add_location(div0, file$3, 90, 8, 2544);
				attr_dev(button, "class", "logout float-right hover:bg-yellow-500 p-3 font-bold text-2xl pl-5 pr-5 svelte-1s68xjz");
				add_location(button, file$3, 91, 8, 2601);
				attr_dev(div1, "class", "header md:w-full text-white bg-yellow-400 border-0 py-2 px-8 focus:outline-none rounded text-lg no-select svelte-1s68xjz");
				add_location(div1, file$3, 87, 4, 2403);
				attr_dev(div2, "class", "flex justify-center justify-evenly w-full no-select svelte-1s68xjz");
				add_location(div2, file$3, 117, 4, 3227);
			},
			m: function mount(target, anchor) {
				insert_dev(target, div1, anchor);
				append_dev(div1, div0);
				append_dev(div1, t1);
				append_dev(div1, button);
				insert_dev(target, t3, anchor);
				if (if_block0) if_block0.m(target, anchor);
				insert_dev(target, t4, anchor);
				if (if_block1) if_block1.m(target, anchor);
				insert_dev(target, t5, anchor);
				if (if_block2) if_block2.m(target, anchor);
				insert_dev(target, t6, anchor);
				insert_dev(target, div2, anchor);
				mount_component(pannablebox0, div2, null);
				append_dev(div2, t7);
				mount_component(pannablebox1, div2, null);
				current = true;

				if (!mounted) {
					dispose = listen_dev(button, "click", /*out*/ ctx[9], false, false, false);
					mounted = true;
				}
			},
			p: function update(ctx, dirty) {
				if (/*loader*/ ctx[3]) {
					if (if_block0) {
						if (dirty & /*loader*/ 8) {
							transition_in(if_block0, 1);
						}
					} else {
						if_block0 = create_if_block_5(ctx);
						if_block0.c();
						transition_in(if_block0, 1);
						if_block0.m(t4.parentNode, t4);
					}
				} else if (if_block0) {
					group_outros();

					transition_out(if_block0, 1, 1, () => {
						if_block0 = null;
					});

					check_outros();
				}

				if (/*importError*/ ctx[7]) {
					if (if_block1) {
						if_block1.p(ctx, dirty);

						if (dirty & /*importError*/ 128) {
							transition_in(if_block1, 1);
						}
					} else {
						if_block1 = create_if_block_4(ctx);
						if_block1.c();
						transition_in(if_block1, 1);
						if_block1.m(t5.parentNode, t5);
					}
				} else if (if_block1) {
					group_outros();

					transition_out(if_block1, 1, 1, () => {
						if_block1 = null;
					});

					check_outros();
				}

				if (/*banner*/ ctx[4]) {
					if (if_block2) {
						if_block2.p(ctx, dirty);

						if (dirty & /*banner*/ 16) {
							transition_in(if_block2, 1);
						}
					} else {
						if_block2 = create_if_block_3(ctx);
						if_block2.c();
						transition_in(if_block2, 1);
						if_block2.m(t6.parentNode, t6);
					}
				} else if (if_block2) {
					group_outros();

					transition_out(if_block2, 1, 1, () => {
						if_block2 = null;
					});

					check_outros();
				}

				const pannablebox0_changes = {};

				if (dirty & /*$$scope, formattedSelectedTo, dateChosen, formattedSelectedFrom*/ 262151) {
					pannablebox0_changes.$$scope = { dirty, ctx };
				}

				pannablebox0.$set(pannablebox0_changes);
				const pannablebox1_changes = {};

				if (dirty & /*$$scope*/ 262144) {
					pannablebox1_changes.$$scope = { dirty, ctx };
				}

				pannablebox1.$set(pannablebox1_changes);
			},
			i: function intro(local) {
				if (current) return;
				transition_in(if_block0);
				transition_in(if_block1);
				transition_in(if_block2);
				transition_in(pannablebox0.$$.fragment, local);
				transition_in(pannablebox1.$$.fragment, local);
				current = true;
			},
			o: function outro(local) {
				transition_out(if_block0);
				transition_out(if_block1);
				transition_out(if_block2);
				transition_out(pannablebox0.$$.fragment, local);
				transition_out(pannablebox1.$$.fragment, local);
				current = false;
			},
			d: function destroy(detaching) {
				if (detaching) detach_dev(div1);
				if (detaching) detach_dev(t3);
				if (if_block0) if_block0.d(detaching);
				if (detaching) detach_dev(t4);
				if (if_block1) if_block1.d(detaching);
				if (detaching) detach_dev(t5);
				if (if_block2) if_block2.d(detaching);
				if (detaching) detach_dev(t6);
				if (detaching) detach_dev(div2);
				destroy_component(pannablebox0);
				destroy_component(pannablebox1);
				mounted = false;
				dispose();
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_else_block$1.name,
			type: "else",
			source: "(87:0) {:else}",
			ctx
		});

		return block;
	}

	// (81:0) {#if logOut}
	function create_if_block$1(ctx) {
		let router;
		let current;

		router = new Router({
			props: { routes: { "/": SignIn } },
			$$inline: true
		});

		const block = {
			c: function create() {
				create_component(router.$$.fragment);
			},
			m: function mount(target, anchor) {
				mount_component(router, target, anchor);
				current = true;
			},
			p: noop,
			i: function intro(local) {
				if (current) return;
				transition_in(router.$$.fragment, local);
				current = true;
			},
			o: function outro(local) {
				transition_out(router.$$.fragment, local);
				current = false;
			},
			d: function destroy(detaching) {
				destroy_component(router, detaching);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_if_block$1.name,
			type: "if",
			source: "(81:0) {#if logOut}",
			ctx
		});

		return block;
	}

	// (97:4) {#if loader}
	function create_if_block_5(ctx) {
		let loader_1;
		let current;
		loader_1 = new Loader({ $$inline: true });

		const block = {
			c: function create() {
				create_component(loader_1.$$.fragment);
			},
			m: function mount(target, anchor) {
				mount_component(loader_1, target, anchor);
				current = true;
			},
			i: function intro(local) {
				if (current) return;
				transition_in(loader_1.$$.fragment, local);
				current = true;
			},
			o: function outro(local) {
				transition_out(loader_1.$$.fragment, local);
				current = false;
			},
			d: function destroy(detaching) {
				destroy_component(loader_1, detaching);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_if_block_5.name,
			type: "if",
			source: "(97:4) {#if loader}",
			ctx
		});

		return block;
	}

	// (100:4) {#if importError}
	function create_if_block_4(ctx) {
		let banner_1;
		let current;

		banner_1 = new Banner({
			props: {
				title: "ERROR!",
				message: /*importErrorMessage*/ ctx[8],
				show: true,
				onClose: /*func*/ ctx[12],
				titleColor: "red"
			},
			$$inline: true
		});

		const block = {
			c: function create() {
				create_component(banner_1.$$.fragment);
			},
			m: function mount(target, anchor) {
				mount_component(banner_1, target, anchor);
				current = true;
			},
			p: function update(ctx, dirty) {
				const banner_1_changes = {};
				if (dirty & /*importErrorMessage*/ 256) banner_1_changes.message = /*importErrorMessage*/ ctx[8];
				if (dirty & /*importError*/ 128) banner_1_changes.onClose = /*func*/ ctx[12];
				banner_1.$set(banner_1_changes);
			},
			i: function intro(local) {
				if (current) return;
				transition_in(banner_1.$$.fragment, local);
				current = true;
			},
			o: function outro(local) {
				transition_out(banner_1.$$.fragment, local);
				current = false;
			},
			d: function destroy(detaching) {
				destroy_component(banner_1, detaching);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_if_block_4.name,
			type: "if",
			source: "(100:4) {#if importError}",
			ctx
		});

		return block;
	}

	// (109:4) {#if banner}
	function create_if_block_3(ctx) {
		let banner_1;
		let current;

		banner_1 = new Banner({
			props: {
				title: "DONE!",
				message: "Your " + /*task*/ ctx[5] + " have been updated!",
				show: true,
				onClose: /*func_1*/ ctx[13],
				titleColor: "green"
			},
			$$inline: true
		});

		const block = {
			c: function create() {
				create_component(banner_1.$$.fragment);
			},
			m: function mount(target, anchor) {
				mount_component(banner_1, target, anchor);
				current = true;
			},
			p: function update(ctx, dirty) {
				const banner_1_changes = {};
				if (dirty & /*task*/ 32) banner_1_changes.message = "Your " + /*task*/ ctx[5] + " have been updated!";
				if (dirty & /*banner*/ 16) banner_1_changes.onClose = /*func_1*/ ctx[13];
				banner_1.$set(banner_1_changes);
			},
			i: function intro(local) {
				if (current) return;
				transition_in(banner_1.$$.fragment, local);
				current = true;
			},
			o: function outro(local) {
				transition_out(banner_1.$$.fragment, local);
				current = false;
			},
			d: function destroy(detaching) {
				destroy_component(banner_1, detaching);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_if_block_3.name,
			type: "if",
			source: "(109:4) {#if banner}",
			ctx
		});

		return block;
	}

	// (148:71) {:else}
	function create_else_block_2(ctx) {
		let t;

		const block = {
			c: function create() {
				t = text("To:");
			},
			m: function mount(target, anchor) {
				insert_dev(target, t, anchor);
			},
			p: noop,
			d: function destroy(detaching) {
				if (detaching) detach_dev(t);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_else_block_2.name,
			type: "else",
			source: "(148:71) {:else}",
			ctx
		});

		return block;
	}

	// (148:26) {#if dateChosen}
	function create_if_block_2$1(ctx) {
		let t0;
		let t1;

		const block = {
			c: function create() {
				t0 = text("To: ");
				t1 = text(/*formattedSelectedFrom*/ ctx[0]);
			},
			m: function mount(target, anchor) {
				insert_dev(target, t0, anchor);
				insert_dev(target, t1, anchor);
			},
			p: function update(ctx, dirty) {
				if (dirty & /*formattedSelectedFrom*/ 1) set_data_dev(t1, /*formattedSelectedFrom*/ ctx[0]);
			},
			d: function destroy(detaching) {
				if (detaching) detach_dev(t0);
				if (detaching) detach_dev(t1);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_if_block_2$1.name,
			type: "if",
			source: "(148:26) {#if dateChosen}",
			ctx
		});

		return block;
	}

	// (146:20) <Datepicker bind:formattedSelected={formattedSelectedFrom} bind:dateChosen>
	function create_default_slot_3(ctx) {
		let button;

		function select_block_type_1(ctx, dirty) {
			if (/*dateChosen*/ ctx[2]) return create_if_block_2$1;
			return create_else_block_2;
		}

		let current_block_type = select_block_type_1(ctx);
		let if_block = current_block_type(ctx);

		const block = {
			c: function create() {
				button = element("button");
				if_block.c();
				attr_dev(button, "class", "custom-button");
				add_location(button, file$3, 146, 24, 4646);
			},
			m: function mount(target, anchor) {
				insert_dev(target, button, anchor);
				if_block.m(button, null);
			},
			p: function update(ctx, dirty) {
				if (current_block_type === (current_block_type = select_block_type_1(ctx)) && if_block) {
					if_block.p(ctx, dirty);
				} else {
					if_block.d(1);
					if_block = current_block_type(ctx);

					if (if_block) {
						if_block.c();
						if_block.m(button, null);
					}
				}
			},
			d: function destroy(detaching) {
				if (detaching) detach_dev(button);
				if_block.d();
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_default_slot_3.name,
			type: "slot",
			source: "(146:20) <Datepicker bind:formattedSelected={formattedSelectedFrom} bind:dateChosen>",
			ctx
		});

		return block;
	}

	// (153:69) {:else}
	function create_else_block_1(ctx) {
		let t;

		const block = {
			c: function create() {
				t = text("To:");
			},
			m: function mount(target, anchor) {
				insert_dev(target, t, anchor);
			},
			p: noop,
			d: function destroy(detaching) {
				if (detaching) detach_dev(t);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_else_block_1.name,
			type: "else",
			source: "(153:69) {:else}",
			ctx
		});

		return block;
	}

	// (153:26) {#if dateChosen}
	function create_if_block_1$1(ctx) {
		let t0;
		let t1;

		const block = {
			c: function create() {
				t0 = text("To: ");
				t1 = text(/*formattedSelectedTo*/ ctx[1]);
			},
			m: function mount(target, anchor) {
				insert_dev(target, t0, anchor);
				insert_dev(target, t1, anchor);
			},
			p: function update(ctx, dirty) {
				if (dirty & /*formattedSelectedTo*/ 2) set_data_dev(t1, /*formattedSelectedTo*/ ctx[1]);
			},
			d: function destroy(detaching) {
				if (detaching) detach_dev(t0);
				if (detaching) detach_dev(t1);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_if_block_1$1.name,
			type: "if",
			source: "(153:26) {#if dateChosen}",
			ctx
		});

		return block;
	}

	// (151:22) <Datepicker bind:formattedSelected={formattedSelectedTo} bind:dateChosen>
	function create_default_slot_2(ctx) {
		let button;

		function select_block_type_2(ctx, dirty) {
			if (/*dateChosen*/ ctx[2]) return create_if_block_1$1;
			return create_else_block_1;
		}

		let current_block_type = select_block_type_2(ctx);
		let if_block = current_block_type(ctx);

		const block = {
			c: function create() {
				button = element("button");
				if_block.c();
				attr_dev(button, "class", "custom-button");
				add_location(button, file$3, 151, 24, 4956);
			},
			m: function mount(target, anchor) {
				insert_dev(target, button, anchor);
				if_block.m(button, null);
			},
			p: function update(ctx, dirty) {
				if (current_block_type === (current_block_type = select_block_type_2(ctx)) && if_block) {
					if_block.p(ctx, dirty);
				} else {
					if_block.d(1);
					if_block = current_block_type(ctx);

					if (if_block) {
						if_block.c();
						if_block.m(button, null);
					}
				}
			},
			d: function destroy(detaching) {
				if (detaching) detach_dev(button);
				if_block.d();
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_default_slot_2.name,
			type: "slot",
			source: "(151:22) <Datepicker bind:formattedSelected={formattedSelectedTo} bind:dateChosen>",
			ctx
		});

		return block;
	}

	// (119:8) <PannableBox>
	function create_default_slot_1$1(ctx) {
		let div3;
		let div1;
		let div0;
		let svg;
		let path;
		let t0;
		let h2;
		let t2;
		let div2;
		let datepicker0;
		let updating_formattedSelected;
		let updating_dateChosen;
		let t3;
		let datepicker1;
		let updating_formattedSelected_1;
		let updating_dateChosen_1;
		let t4;
		let button;
		let current;
		let mounted;
		let dispose;

		function datepicker0_formattedSelected_binding(value) {
    		/*datepicker0_formattedSelected_binding*/ ctx[14](value);
		}

		function datepicker0_dateChosen_binding(value) {
    		/*datepicker0_dateChosen_binding*/ ctx[15](value);
		}

		let datepicker0_props = {
			$$slots: { default: [create_default_slot_3] },
			$$scope: { ctx }
		};

		if (/*formattedSelectedFrom*/ ctx[0] !== void 0) {
			datepicker0_props.formattedSelected = /*formattedSelectedFrom*/ ctx[0];
		}

		if (/*dateChosen*/ ctx[2] !== void 0) {
			datepicker0_props.dateChosen = /*dateChosen*/ ctx[2];
		}

		datepicker0 = new Datepicker({ props: datepicker0_props, $$inline: true });
		binding_callbacks.push(() => bind$1(datepicker0, 'formattedSelected', datepicker0_formattedSelected_binding));
		binding_callbacks.push(() => bind$1(datepicker0, 'dateChosen', datepicker0_dateChosen_binding));

		function datepicker1_formattedSelected_binding(value) {
    		/*datepicker1_formattedSelected_binding*/ ctx[16](value);
		}

		function datepicker1_dateChosen_binding(value) {
    		/*datepicker1_dateChosen_binding*/ ctx[17](value);
		}

		let datepicker1_props = {
			$$slots: { default: [create_default_slot_2] },
			$$scope: { ctx }
		};

		if (/*formattedSelectedTo*/ ctx[1] !== void 0) {
			datepicker1_props.formattedSelected = /*formattedSelectedTo*/ ctx[1];
		}

		if (/*dateChosen*/ ctx[2] !== void 0) {
			datepicker1_props.dateChosen = /*dateChosen*/ ctx[2];
		}

		datepicker1 = new Datepicker({ props: datepicker1_props, $$inline: true });
		binding_callbacks.push(() => bind$1(datepicker1, 'formattedSelected', datepicker1_formattedSelected_binding));
		binding_callbacks.push(() => bind$1(datepicker1, 'dateChosen', datepicker1_dateChosen_binding));

		const block = {
			c: function create() {
				div3 = element("div");
				div1 = element("div");
				div0 = element("div");
				svg = svg_element("svg");
				path = svg_element("path");
				t0 = space();
				h2 = element("h2");
				h2.textContent = "HOURS";
				t2 = space();
				div2 = element("div");
				create_component(datepicker0.$$.fragment);
				t3 = space();
				create_component(datepicker1.$$.fragment);
				t4 = space();
				button = element("button");
				button.textContent = "IMPORT";
				attr_dev(path, "stroke-linecap", "round");
				attr_dev(path, "stroke-linejoin", "round");
				attr_dev(path, "stroke-width", "2");
				attr_dev(path, "d", "M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z");
				add_location(path, file$3, 131, 28, 3919);
				attr_dev(svg, "xmlns", "http://www.w3.org/2000/svg");
				attr_dev(svg, "class", "h-6 w-6 mr-2");
				attr_dev(svg, "fill", "none");
				attr_dev(svg, "viewBox", "0 0 24 24");
				attr_dev(svg, "stroke", "currentColor");
				add_location(svg, file$3, 124, 24, 3610);
				attr_dev(div0, "class", "flex flex-col justify-center");
				add_location(div0, file$3, 123, 20, 3543);
				attr_dev(h2, "class", "text-gray-900 text-lg font-medium title-font");
				add_location(h2, file$3, 139, 20, 4274);
				attr_dev(div1, "class", "text-center content-center flex flex-row justify-center align-middle mb-5");
				add_location(div1, file$3, 120, 16, 3398);
				attr_dev(div2, "class", "dates flex space-between svelte-1s68xjz");
				add_location(div2, file$3, 144, 16, 4487);
				attr_dev(button, "class", "text-white bg-yellow-400 border-0 py-2 px-8 focus:outline-none hover:bg-yellow-500 rounded text-lg mt-6");
				add_location(button, file$3, 156, 16, 5183);
				attr_dev(div3, "class", "flex flex-col bg-gray-100 rounded-lg p-8");
				add_location(div3, file$3, 119, 12, 3327);
			},
			m: function mount(target, anchor) {
				insert_dev(target, div3, anchor);
				append_dev(div3, div1);
				append_dev(div1, div0);
				append_dev(div0, svg);
				append_dev(svg, path);
				append_dev(div1, t0);
				append_dev(div1, h2);
				append_dev(div3, t2);
				append_dev(div3, div2);
				mount_component(datepicker0, div2, null);
				append_dev(div2, t3);
				mount_component(datepicker1, div2, null);
				append_dev(div3, t4);
				append_dev(div3, button);
				current = true;

				if (!mounted) {
					dispose = listen_dev(button, "click", /*handleClickHours*/ ctx[10], false, false, false);
					mounted = true;
				}
			},
			p: function update(ctx, dirty) {
				const datepicker0_changes = {};

				if (dirty & /*$$scope, formattedSelectedFrom, dateChosen*/ 262149) {
					datepicker0_changes.$$scope = { dirty, ctx };
				}

				if (!updating_formattedSelected && dirty & /*formattedSelectedFrom*/ 1) {
					updating_formattedSelected = true;
					datepicker0_changes.formattedSelected = /*formattedSelectedFrom*/ ctx[0];
					add_flush_callback(() => updating_formattedSelected = false);
				}

				if (!updating_dateChosen && dirty & /*dateChosen*/ 4) {
					updating_dateChosen = true;
					datepicker0_changes.dateChosen = /*dateChosen*/ ctx[2];
					add_flush_callback(() => updating_dateChosen = false);
				}

				datepicker0.$set(datepicker0_changes);
				const datepicker1_changes = {};

				if (dirty & /*$$scope, formattedSelectedTo, dateChosen*/ 262150) {
					datepicker1_changes.$$scope = { dirty, ctx };
				}

				if (!updating_formattedSelected_1 && dirty & /*formattedSelectedTo*/ 2) {
					updating_formattedSelected_1 = true;
					datepicker1_changes.formattedSelected = /*formattedSelectedTo*/ ctx[1];
					add_flush_callback(() => updating_formattedSelected_1 = false);
				}

				if (!updating_dateChosen_1 && dirty & /*dateChosen*/ 4) {
					updating_dateChosen_1 = true;
					datepicker1_changes.dateChosen = /*dateChosen*/ ctx[2];
					add_flush_callback(() => updating_dateChosen_1 = false);
				}

				datepicker1.$set(datepicker1_changes);
			},
			i: function intro(local) {
				if (current) return;
				transition_in(datepicker0.$$.fragment, local);
				transition_in(datepicker1.$$.fragment, local);
				current = true;
			},
			o: function outro(local) {
				transition_out(datepicker0.$$.fragment, local);
				transition_out(datepicker1.$$.fragment, local);
				current = false;
			},
			d: function destroy(detaching) {
				if (detaching) detach_dev(div3);
				destroy_component(datepicker0);
				destroy_component(datepicker1);
				mounted = false;
				dispose();
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_default_slot_1$1.name,
			type: "slot",
			source: "(119:8) <PannableBox>",
			ctx
		});

		return block;
	}

	// (164:8) <PannableBox>
	function create_default_slot$1(ctx) {
		let div3;
		let div1;
		let div0;
		let svg;
		let path;
		let t0;
		let h2;
		let t2;
		let div2;
		let pickdatefrom;
		let t3;
		let pickdateto;
		let t4;
		let button;
		let current;
		let mounted;
		let dispose;
		pickdatefrom = new PickDateFrom({ $$inline: true });
		pickdateto = new PickDateTo({ $$inline: true });

		const block = {
			c: function create() {
				div3 = element("div");
				div1 = element("div");
				div0 = element("div");
				svg = svg_element("svg");
				path = svg_element("path");
				t0 = space();
				h2 = element("h2");
				h2.textContent = "PROJECTS";
				t2 = space();
				div2 = element("div");
				create_component(pickdatefrom.$$.fragment);
				t3 = space();
				create_component(pickdateto.$$.fragment);
				t4 = space();
				button = element("button");
				button.textContent = "IMPORT";
				attr_dev(path, "stroke-linecap", "round");
				attr_dev(path, "stroke-linejoin", "round");
				attr_dev(path, "stroke-width", "2");
				attr_dev(path, "d", "M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2m-6 9l2 2 4-4");
				add_location(path, file$3, 176, 28, 6093);
				attr_dev(svg, "xmlns", "http://www.w3.org/2000/svg");
				attr_dev(svg, "class", "h-6 w-6 mr-2");
				attr_dev(svg, "fill", "none");
				attr_dev(svg, "viewBox", "0 0 24 24");
				attr_dev(svg, "stroke", "currentColor");
				add_location(svg, file$3, 169, 24, 5784);
				attr_dev(div0, "class", "flex flex-col justify-center");
				add_location(div0, file$3, 168, 20, 5717);
				attr_dev(h2, "class", "text-gray-900 text-lg font-medium title-font");
				add_location(h2, file$3, 184, 20, 6545);
				attr_dev(div1, "class", "text-center content-center flex flex-row justify-center align-middle mb-5");
				add_location(div1, file$3, 165, 16, 5572);
				attr_dev(div2, "class", "dates flex space-between svelte-1s68xjz");
				add_location(div2, file$3, 189, 16, 6760);
				attr_dev(button, "class", "text-white bg-yellow-400 border-0 py-2 px-8 focus:outline-none hover:bg-yellow-500 rounded text-lg mt-6");
				add_location(button, file$3, 193, 16, 6910);
				attr_dev(div3, "class", "bg-gray-100 rounded-lg p-8 flex flex-col");
				add_location(div3, file$3, 164, 12, 5501);
			},
			m: function mount(target, anchor) {
				insert_dev(target, div3, anchor);
				append_dev(div3, div1);
				append_dev(div1, div0);
				append_dev(div0, svg);
				append_dev(svg, path);
				append_dev(div1, t0);
				append_dev(div1, h2);
				append_dev(div3, t2);
				append_dev(div3, div2);
				mount_component(pickdatefrom, div2, null);
				append_dev(div2, t3);
				mount_component(pickdateto, div2, null);
				append_dev(div3, t4);
				append_dev(div3, button);
				current = true;

				if (!mounted) {
					dispose = listen_dev(button, "click", /*handleClickProjects*/ ctx[11], false, false, false);
					mounted = true;
				}
			},
			p: noop,
			i: function intro(local) {
				if (current) return;
				transition_in(pickdatefrom.$$.fragment, local);
				transition_in(pickdateto.$$.fragment, local);
				current = true;
			},
			o: function outro(local) {
				transition_out(pickdatefrom.$$.fragment, local);
				transition_out(pickdateto.$$.fragment, local);
				current = false;
			},
			d: function destroy(detaching) {
				if (detaching) detach_dev(div3);
				destroy_component(pickdatefrom);
				destroy_component(pickdateto);
				mounted = false;
				dispose();
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_default_slot$1.name,
			type: "slot",
			source: "(164:8) <PannableBox>",
			ctx
		});

		return block;
	}

	function create_fragment$3(ctx) {
		let current_block_type_index;
		let if_block;
		let if_block_anchor;
		let current;
		const if_block_creators = [create_if_block$1, create_else_block$1];
		const if_blocks = [];

		function select_block_type(ctx, dirty) {
			if (/*logOut*/ ctx[6]) return 0;
			return 1;
		}

		current_block_type_index = select_block_type(ctx);
		if_block = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);

		const block = {
			c: function create() {
				if_block.c();
				if_block_anchor = empty();
			},
			l: function claim(nodes) {
				throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			},
			m: function mount(target, anchor) {
				if_blocks[current_block_type_index].m(target, anchor);
				insert_dev(target, if_block_anchor, anchor);
				current = true;
			},
			p: function update(ctx, [dirty]) {
				let previous_block_index = current_block_type_index;
				current_block_type_index = select_block_type(ctx);

				if (current_block_type_index === previous_block_index) {
					if_blocks[current_block_type_index].p(ctx, dirty);
				} else {
					group_outros();

					transition_out(if_blocks[previous_block_index], 1, 1, () => {
						if_blocks[previous_block_index] = null;
					});

					check_outros();
					if_block = if_blocks[current_block_type_index];

					if (!if_block) {
						if_block = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);
						if_block.c();
					} else {
						if_block.p(ctx, dirty);
					}

					transition_in(if_block, 1);
					if_block.m(if_block_anchor.parentNode, if_block_anchor);
				}
			},
			i: function intro(local) {
				if (current) return;
				transition_in(if_block);
				current = true;
			},
			o: function outro(local) {
				transition_out(if_block);
				current = false;
			},
			d: function destroy(detaching) {
				if_blocks[current_block_type_index].d(detaching);
				if (detaching) detach_dev(if_block_anchor);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_fragment$3.name,
			type: "component",
			source: "",
			ctx
		});

		return block;
	}

	function instance$3($$self, $$props, $$invalidate) {
		let { $$slots: slots = {}, $$scope } = $$props;
		validate_slots('Home', slots, []);
		let formattedSelectedFrom;
		let formattedSelectedTo;
		let dateChosen = true;
		let loader;
		let banner;
		let task;
		let logOut = false;
		let importError = false;
		let importErrorMessage;

		function out() {
			$$invalidate(6, logOut = true);
		}

		const handleClickHours = () => {
			$$invalidate(3, loader = true);
			const newDateFrom = `/${formattedSelectedFrom}`;
			const newDateFrom2 = newDateFrom.split("/").join("-");
			const newDateFrom3 = newDateFrom2.slice(-4) + newDateFrom2.slice(0, -5);

			////////
			const newDateTo = `/${formattedSelectedTo}`;

			const newDateTo2 = newDateTo.split("/").join("-");
			const newDateTo3 = newDateTo2.slice(-4) + newDateTo2.slice(0, -5);

			////////
			const urlHours = `/api/hours?trip-start=${newDateFrom3}&trip-end=${newDateTo3}`;

			axios.get(urlHours).then(res => {
				$$invalidate(3, loader = false);
				$$invalidate(4, banner = true);
				$$invalidate(5, task = "hours");
				console.log(res);
			}).catch(e => {
				console.log(e);
				$$invalidate(3, loader = false);
				$$invalidate(7, importError = true);
				$$invalidate(8, importErrorMessage = e.message);
			});
		};

		const handleClickProjects = () => {
			const urlProjects = "/api/projects";
			$$invalidate(3, loader = true);

			axios.get(urlProjects).then(res => console.log(res.data)).then(res => {
				$$invalidate(3, loader = false);
				$$invalidate(4, banner = true);
				$$invalidate(5, task = "projects");
				console.log(res);
			}).catch(e => {
				console.log(e);
				$$invalidate(3, loader = false);
				$$invalidate(7, importError = true);
				$$invalidate(8, importErrorMessage = e.message);
			});
		};

		const writable_props = [];

		Object.keys($$props).forEach(key => {
			if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console_1$1.warn(`<Home> was created with unknown prop '${key}'`);
		});

		const func = () => $$invalidate(7, importError = false);
		const func_1 = () => $$invalidate(4, banner = false);

		function datepicker0_formattedSelected_binding(value) {
			formattedSelectedFrom = value;
			$$invalidate(0, formattedSelectedFrom);
		}

		function datepicker0_dateChosen_binding(value) {
			dateChosen = value;
			$$invalidate(2, dateChosen);
		}

		function datepicker1_formattedSelected_binding(value) {
			formattedSelectedTo = value;
			$$invalidate(1, formattedSelectedTo);
		}

		function datepicker1_dateChosen_binding(value) {
			dateChosen = value;
			$$invalidate(2, dateChosen);
		}

		$$self.$capture_state = () => ({
			PickDateFrom,
			PickDateTo,
			PannableBox,
			axios,
			Datepicker,
			Loader,
			Banner,
			Router,
			SignIn,
			formattedSelectedFrom,
			formattedSelectedTo,
			dateChosen,
			loader,
			banner,
			task,
			logOut,
			importError,
			importErrorMessage,
			out,
			handleClickHours,
			handleClickProjects
		});

		$$self.$inject_state = $$props => {
			if ('formattedSelectedFrom' in $$props) $$invalidate(0, formattedSelectedFrom = $$props.formattedSelectedFrom);
			if ('formattedSelectedTo' in $$props) $$invalidate(1, formattedSelectedTo = $$props.formattedSelectedTo);
			if ('dateChosen' in $$props) $$invalidate(2, dateChosen = $$props.dateChosen);
			if ('loader' in $$props) $$invalidate(3, loader = $$props.loader);
			if ('banner' in $$props) $$invalidate(4, banner = $$props.banner);
			if ('task' in $$props) $$invalidate(5, task = $$props.task);
			if ('logOut' in $$props) $$invalidate(6, logOut = $$props.logOut);
			if ('importError' in $$props) $$invalidate(7, importError = $$props.importError);
			if ('importErrorMessage' in $$props) $$invalidate(8, importErrorMessage = $$props.importErrorMessage);
		};

		if ($$props && "$$inject" in $$props) {
			$$self.$inject_state($$props.$$inject);
		}

		return [
			formattedSelectedFrom,
			formattedSelectedTo,
			dateChosen,
			loader,
			banner,
			task,
			logOut,
			importError,
			importErrorMessage,
			out,
			handleClickHours,
			handleClickProjects,
			func,
			func_1,
			datepicker0_formattedSelected_binding,
			datepicker0_dateChosen_binding,
			datepicker1_formattedSelected_binding,
			datepicker1_dateChosen_binding
		];
	}

	class Home extends SvelteComponentDev {
		constructor(options) {
			super(options);
			init(this, options, instance$3, create_fragment$3, safe_not_equal, {});

			dispatch_dev("SvelteRegisterComponent", {
				component: this,
				tagName: "Home",
				options,
				id: create_fragment$3.name
			});
		}
	}

	/* src/components/A.svelte generated by Svelte v3.42.3 */

	const file$2 = "src/components/A.svelte";

	function create_fragment$2(ctx) {
		let a;
		let current;
		const default_slot_template = /*#slots*/ ctx[2].default;
		const default_slot = create_slot(default_slot_template, ctx, /*$$scope*/ ctx[1], null);

		const block = {
			c: function create() {
				a = element("a");
				if (default_slot) default_slot.c();
				attr_dev(a, "href", /*href*/ ctx[0]);
				attr_dev(a, "class", "text-black flex items-center justify-center bg-gray-200 border-0 px-8 focus:outline-none hover:bg-gray-300 rounded");
				add_location(a, file$2, 4, 0, 40);
			},
			l: function claim(nodes) {
				throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			},
			m: function mount(target, anchor) {
				insert_dev(target, a, anchor);

				if (default_slot) {
					default_slot.m(a, null);
				}

				current = true;
			},
			p: function update(ctx, [dirty]) {
				if (default_slot) {
					if (default_slot.p && (!current || dirty & /*$$scope*/ 2)) {
						update_slot_base(
							default_slot,
							default_slot_template,
							ctx,
    						/*$$scope*/ ctx[1],
							!current
								? get_all_dirty_from_scope(/*$$scope*/ ctx[1])
								: get_slot_changes(default_slot_template, /*$$scope*/ ctx[1], dirty, null),
							null
						);
					}
				}

				if (!current || dirty & /*href*/ 1) {
					attr_dev(a, "href", /*href*/ ctx[0]);
				}
			},
			i: function intro(local) {
				if (current) return;
				transition_in(default_slot, local);
				current = true;
			},
			o: function outro(local) {
				transition_out(default_slot, local);
				current = false;
			},
			d: function destroy(detaching) {
				if (detaching) detach_dev(a);
				if (default_slot) default_slot.d(detaching);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_fragment$2.name,
			type: "component",
			source: "",
			ctx
		});

		return block;
	}

	function instance$2($$self, $$props, $$invalidate) {
		let { $$slots: slots = {}, $$scope } = $$props;
		validate_slots('A', slots, ['default']);
		let { href } = $$props;
		const writable_props = ['href'];

		Object.keys($$props).forEach(key => {
			if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<A> was created with unknown prop '${key}'`);
		});

		$$self.$$set = $$props => {
			if ('href' in $$props) $$invalidate(0, href = $$props.href);
			if ('$$scope' in $$props) $$invalidate(1, $$scope = $$props.$$scope);
		};

		$$self.$capture_state = () => ({ href });

		$$self.$inject_state = $$props => {
			if ('href' in $$props) $$invalidate(0, href = $$props.href);
		};

		if ($$props && "$$inject" in $$props) {
			$$self.$inject_state($$props.$$inject);
		}

		return [href, $$scope, slots];
	}

	class A extends SvelteComponentDev {
		constructor(options) {
			super(options);
			init(this, options, instance$2, create_fragment$2, safe_not_equal, { href: 0 });

			dispatch_dev("SvelteRegisterComponent", {
				component: this,
				tagName: "A",
				options,
				id: create_fragment$2.name
			});

			const { ctx } = this.$$;
			const props = options.props || {};

			if (/*href*/ ctx[0] === undefined && !('href' in props)) {
				console.warn("<A> was created without expected prop 'href'");
			}
		}

		get href() {
			throw new Error("<A>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}

		set href(value) {
			throw new Error("<A>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
		}
	}

	const userbaseStore = writable(null);
	const userStore = writable(null);
	const promiseStore = writable(null);

	/* src/routes/SignIn.svelte generated by Svelte v3.42.3 */

	const { console: console_1 } = globals;
	const file$1 = "src/routes/SignIn.svelte";

	// (78:0) {:else}
	function create_else_block(ctx) {
		let router;
		let current;

		router = new Router({
			props: { routes: { "/": Home } },
			$$inline: true
		});

		const block = {
			c: function create() {
				create_component(router.$$.fragment);
			},
			m: function mount(target, anchor) {
				mount_component(router, target, anchor);
				current = true;
			},
			p: noop,
			i: function intro(local) {
				if (current) return;
				transition_in(router.$$.fragment, local);
				current = true;
			},
			o: function outro(local) {
				transition_out(router.$$.fragment, local);
				current = false;
			},
			d: function destroy(detaching) {
				destroy_component(router, detaching);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_else_block.name,
			type: "else",
			source: "(78:0) {:else}",
			ctx
		});

		return block;
	}

	// (51:18) 
	function create_if_block_1(ctx) {
		let t;
		let pannablebox;
		let current;
		let if_block = /*logError*/ ctx[4] && create_if_block_2(ctx);

		pannablebox = new PannableBox({
			props: {
				$$slots: { default: [create_default_slot] },
				$$scope: { ctx }
			},
			$$inline: true
		});

		const block = {
			c: function create() {
				if (if_block) if_block.c();
				t = space();
				create_component(pannablebox.$$.fragment);
			},
			m: function mount(target, anchor) {
				if (if_block) if_block.m(target, anchor);
				insert_dev(target, t, anchor);
				mount_component(pannablebox, target, anchor);
				current = true;
			},
			p: function update(ctx, dirty) {
				if (/*logError*/ ctx[4]) {
					if (if_block) {
						if_block.p(ctx, dirty);

						if (dirty & /*logError*/ 16) {
							transition_in(if_block, 1);
						}
					} else {
						if_block = create_if_block_2(ctx);
						if_block.c();
						transition_in(if_block, 1);
						if_block.m(t.parentNode, t);
					}
				} else if (if_block) {
					group_outros();

					transition_out(if_block, 1, 1, () => {
						if_block = null;
					});

					check_outros();
				}

				const pannablebox_changes = {};

				if (dirty & /*$$scope, password, username*/ 515) {
					pannablebox_changes.$$scope = { dirty, ctx };
				}

				pannablebox.$set(pannablebox_changes);
			},
			i: function intro(local) {
				if (current) return;
				transition_in(if_block);
				transition_in(pannablebox.$$.fragment, local);
				current = true;
			},
			o: function outro(local) {
				transition_out(if_block);
				transition_out(pannablebox.$$.fragment, local);
				current = false;
			},
			d: function destroy(detaching) {
				if (if_block) if_block.d(detaching);
				if (detaching) detach_dev(t);
				destroy_component(pannablebox, detaching);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_if_block_1.name,
			type: "if",
			source: "(51:18) ",
			ctx
		});

		return block;
	}

	// (49:0) {#if loading}
	function create_if_block(ctx) {
		let loader;
		let current;
		loader = new Loader({ $$inline: true });

		const block = {
			c: function create() {
				create_component(loader.$$.fragment);
			},
			m: function mount(target, anchor) {
				mount_component(loader, target, anchor);
				current = true;
			},
			p: noop,
			i: function intro(local) {
				if (current) return;
				transition_in(loader.$$.fragment, local);
				current = true;
			},
			o: function outro(local) {
				transition_out(loader.$$.fragment, local);
				current = false;
			},
			d: function destroy(detaching) {
				destroy_component(loader, detaching);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_if_block.name,
			type: "if",
			source: "(49:0) {#if loading}",
			ctx
		});

		return block;
	}

	// (52:2) {#if logError}
	function create_if_block_2(ctx) {
		let banner;
		let current;

		banner = new Banner({
			props: {
				title: "ERROR!",
				message: "Your username or password is incorrect.",
				show: true,
				onClose: /*func*/ ctx[6],
				titleColor: "red"
			},
			$$inline: true
		});

		const block = {
			c: function create() {
				create_component(banner.$$.fragment);
			},
			m: function mount(target, anchor) {
				mount_component(banner, target, anchor);
				current = true;
			},
			p: function update(ctx, dirty) {
				const banner_changes = {};
				if (dirty & /*logError*/ 16) banner_changes.onClose = /*func*/ ctx[6];
				banner.$set(banner_changes);
			},
			i: function intro(local) {
				if (current) return;
				transition_in(banner.$$.fragment, local);
				current = true;
			},
			o: function outro(local) {
				transition_out(banner.$$.fragment, local);
				current = false;
			},
			d: function destroy(detaching) {
				destroy_component(banner, detaching);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_if_block_2.name,
			type: "if",
			source: "(52:2) {#if logError}",
			ctx
		});

		return block;
	}

	// (75:6) <Button>
	function create_default_slot_1(ctx) {
		let t;

		const block = {
			c: function create() {
				t = text("Sign in!");
			},
			m: function mount(target, anchor) {
				insert_dev(target, t, anchor);
			},
			d: function destroy(detaching) {
				if (detaching) detach_dev(t);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_default_slot_1.name,
			type: "slot",
			source: "(75:6) <Button>",
			ctx
		});

		return block;
	}

	// (61:2) <PannableBox>
	function create_default_slot(ctx) {
		let form;
		let div;
		let h2;
		let t1;
		let input0;
		let updating_value;
		let t2;
		let input1;
		let updating_value_1;
		let t3;
		let button;
		let current;
		let mounted;
		let dispose;

		function input0_value_binding(value) {
    		/*input0_value_binding*/ ctx[7](value);
		}

		let input0_props = {
			label: "Username",
			type: "text",
			required: true
		};

		if (/*username*/ ctx[0] !== void 0) {
			input0_props.value = /*username*/ ctx[0];
		}

		input0 = new Input({ props: input0_props, $$inline: true });
		binding_callbacks.push(() => bind$1(input0, 'value', input0_value_binding));

		function input1_value_binding(value) {
    		/*input1_value_binding*/ ctx[8](value);
		}

		let input1_props = {
			label: "Password",
			type: "password",
			required: true
		};

		if (/*password*/ ctx[1] !== void 0) {
			input1_props.value = /*password*/ ctx[1];
		}

		input1 = new Input({ props: input1_props, $$inline: true });
		binding_callbacks.push(() => bind$1(input1, 'value', input1_value_binding));

		button = new Button({
			props: {
				$$slots: { default: [create_default_slot_1] },
				$$scope: { ctx }
			},
			$$inline: true
		});

		const block = {
			c: function create() {
				form = element("form");
				div = element("div");
				h2 = element("h2");
				h2.textContent = "Sign In";
				t1 = space();
				create_component(input0.$$.fragment);
				t2 = space();
				create_component(input1.$$.fragment);
				t3 = space();
				create_component(button.$$.fragment);
				attr_dev(h2, "class", "text-gray-900 text-lg font-medium title-font mb-5");
				add_location(h2, file$1, 66, 8, 1707);
				attr_dev(div, "class", "flex flex-col justify-between");
				add_location(div, file$1, 65, 6, 1655);
				attr_dev(form, "class", "bg-gray-100 rounded-lg p-8 flex flex-col no-select svelte-1hvh1p5");
				add_location(form, file$1, 61, 4, 1530);
			},
			m: function mount(target, anchor) {
				insert_dev(target, form, anchor);
				append_dev(form, div);
				append_dev(div, h2);
				append_dev(form, t1);
				mount_component(input0, form, null);
				append_dev(form, t2);
				mount_component(input1, form, null);
				append_dev(form, t3);
				mount_component(button, form, null);
				current = true;

				if (!mounted) {
					dispose = listen_dev(form, "submit", prevent_default(/*loginJwt*/ ctx[5]), false, true, false);
					mounted = true;
				}
			},
			p: function update(ctx, dirty) {
				const input0_changes = {};

				if (!updating_value && dirty & /*username*/ 1) {
					updating_value = true;
					input0_changes.value = /*username*/ ctx[0];
					add_flush_callback(() => updating_value = false);
				}

				input0.$set(input0_changes);
				const input1_changes = {};

				if (!updating_value_1 && dirty & /*password*/ 2) {
					updating_value_1 = true;
					input1_changes.value = /*password*/ ctx[1];
					add_flush_callback(() => updating_value_1 = false);
				}

				input1.$set(input1_changes);
				const button_changes = {};

				if (dirty & /*$$scope*/ 512) {
					button_changes.$$scope = { dirty, ctx };
				}

				button.$set(button_changes);
			},
			i: function intro(local) {
				if (current) return;
				transition_in(input0.$$.fragment, local);
				transition_in(input1.$$.fragment, local);
				transition_in(button.$$.fragment, local);
				current = true;
			},
			o: function outro(local) {
				transition_out(input0.$$.fragment, local);
				transition_out(input1.$$.fragment, local);
				transition_out(button.$$.fragment, local);
				current = false;
			},
			d: function destroy(detaching) {
				if (detaching) detach_dev(form);
				destroy_component(input0);
				destroy_component(input1);
				destroy_component(button);
				mounted = false;
				dispose();
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_default_slot.name,
			type: "slot",
			source: "(61:2) <PannableBox>",
			ctx
		});

		return block;
	}

	function create_fragment$1(ctx) {
		let current_block_type_index;
		let if_block;
		let if_block_anchor;
		let current;
		const if_block_creators = [create_if_block, create_if_block_1, create_else_block];
		const if_blocks = [];

		function select_block_type(ctx, dirty) {
			if (/*loading*/ ctx[3]) return 0;
			if (!/*logged*/ ctx[2]) return 1;
			return 2;
		}

		current_block_type_index = select_block_type(ctx);
		if_block = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);

		const block = {
			c: function create() {
				if_block.c();
				if_block_anchor = empty();
			},
			l: function claim(nodes) {
				throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			},
			m: function mount(target, anchor) {
				if_blocks[current_block_type_index].m(target, anchor);
				insert_dev(target, if_block_anchor, anchor);
				current = true;
			},
			p: function update(ctx, [dirty]) {
				let previous_block_index = current_block_type_index;
				current_block_type_index = select_block_type(ctx);

				if (current_block_type_index === previous_block_index) {
					if_blocks[current_block_type_index].p(ctx, dirty);
				} else {
					group_outros();

					transition_out(if_blocks[previous_block_index], 1, 1, () => {
						if_blocks[previous_block_index] = null;
					});

					check_outros();
					if_block = if_blocks[current_block_type_index];

					if (!if_block) {
						if_block = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);
						if_block.c();
					} else {
						if_block.p(ctx, dirty);
					}

					transition_in(if_block, 1);
					if_block.m(if_block_anchor.parentNode, if_block_anchor);
				}
			},
			i: function intro(local) {
				if (current) return;
				transition_in(if_block);
				current = true;
			},
			o: function outro(local) {
				transition_out(if_block);
				current = false;
			},
			d: function destroy(detaching) {
				if_blocks[current_block_type_index].d(detaching);
				if (detaching) detach_dev(if_block_anchor);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_fragment$1.name,
			type: "component",
			source: "",
			ctx
		});

		return block;
	}

	function instance$1($$self, $$props, $$invalidate) {
		let { $$slots: slots = {}, $$scope } = $$props;
		validate_slots('SignIn', slots, []);
		let username, password;
		let logged = false;
		let loading = false;
		let logError = false;

		const loginJwt = async () => {
			$$invalidate(3, loading = true);
			const body = { username, password };

			const headers = {
				"Content-Type": "application/json",
				"Access-Control-Allow-Origin": "*"
			};

			axios.post("/api/auth/login", body, { headers }).then(res => {
				res.data.access_token
					? $$invalidate(2, logged = true)
					: console.log("HUBO UN ERROR") && $$invalidate(4, logError = true);

				$$invalidate(4, logError = false);
				$$invalidate(3, loading = false);
			}).catch(e => {
				console.log(e);
				$$invalidate(4, logError = true);
				$$invalidate(3, loading = false);
			});
		};

		const writable_props = [];

		Object.keys($$props).forEach(key => {
			if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console_1.warn(`<SignIn> was created with unknown prop '${key}'`);
		});

		const func = () => $$invalidate(4, logError = false);

		function input0_value_binding(value) {
			username = value;
			$$invalidate(0, username);
		}

		function input1_value_binding(value) {
			password = value;
			$$invalidate(1, password);
		}

		$$self.$capture_state = () => ({
			Button,
			Input,
			Loader,
			Home,
			Banner,
			Router,
			A,
			userbaseStore,
			userStore,
			promiseStore,
			PannableBox,
			axios,
			username,
			password,
			logged,
			loading,
			logError,
			loginJwt
		});

		$$self.$inject_state = $$props => {
			if ('username' in $$props) $$invalidate(0, username = $$props.username);
			if ('password' in $$props) $$invalidate(1, password = $$props.password);
			if ('logged' in $$props) $$invalidate(2, logged = $$props.logged);
			if ('loading' in $$props) $$invalidate(3, loading = $$props.loading);
			if ('logError' in $$props) $$invalidate(4, logError = $$props.logError);
		};

		if ($$props && "$$inject" in $$props) {
			$$self.$inject_state($$props.$$inject);
		}

		return [
			username,
			password,
			logged,
			loading,
			logError,
			loginJwt,
			func,
			input0_value_binding,
			input1_value_binding
		];
	}

	class SignIn extends SvelteComponentDev {
		constructor(options) {
			super(options);
			init(this, options, instance$1, create_fragment$1, safe_not_equal, {});

			dispatch_dev("SvelteRegisterComponent", {
				component: this,
				tagName: "SignIn",
				options,
				id: create_fragment$1.name
			});
		}
	}

	/* src/App.svelte generated by Svelte v3.42.3 */
	const file = "src/App.svelte";

	function create_fragment(ctx) {
		let tailwindcss;
		let t;
		let div1;
		let div0;
		let router;
		let current;
		tailwindcss = new Tailwindcss({ $$inline: true });

		router = new Router({
			props: {
				routes: { '/': SignIn, '/signin': SignIn }, // '/forgotpassword': ForgotPassword

			},
			$$inline: true
		});

		const block = {
			c: function create() {
				create_component(tailwindcss.$$.fragment);
				t = space();
				div1 = element("div");
				div0 = element("div");
				create_component(router.$$.fragment);
				attr_dev(div0, "class", "container flex flex-col justify-center items-center w-screen h-screen mx-auto svelte-1mv6ihe");
				add_location(div0, file, 30, 4, 831);
				add_location(div1, file, 29, 0, 821);
			},
			l: function claim(nodes) {
				throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			},
			m: function mount(target, anchor) {
				mount_component(tailwindcss, target, anchor);
				insert_dev(target, t, anchor);
				insert_dev(target, div1, anchor);
				append_dev(div1, div0);
				mount_component(router, div0, null);
				current = true;
			},
			p: noop,
			i: function intro(local) {
				if (current) return;
				transition_in(tailwindcss.$$.fragment, local);
				transition_in(router.$$.fragment, local);
				current = true;
			},
			o: function outro(local) {
				transition_out(tailwindcss.$$.fragment, local);
				transition_out(router.$$.fragment, local);
				current = false;
			},
			d: function destroy(detaching) {
				destroy_component(tailwindcss, detaching);
				if (detaching) detach_dev(t);
				if (detaching) detach_dev(div1);
				destroy_component(router);
			}
		};

		dispatch_dev("SvelteRegisterBlock", {
			block,
			id: create_fragment.name,
			type: "component",
			source: "",
			ctx
		});

		return block;
	}

	function instance($$self, $$props, $$invalidate) {
		let $userStore;
		let $userbaseStore;
		let $promiseStore;
		validate_store(userStore, 'userStore');
		component_subscribe($$self, userStore, $$value => $$invalidate(0, $userStore = $$value));
		validate_store(userbaseStore, 'userbaseStore');
		component_subscribe($$self, userbaseStore, $$value => $$invalidate(1, $userbaseStore = $$value));
		validate_store(promiseStore, 'promiseStore');
		component_subscribe($$self, promiseStore, $$value => $$invalidate(2, $promiseStore = $$value));
		let { $$slots: slots = {}, $$scope } = $$props;
		validate_slots('App', slots, []);
		const userbase = window.userbase;
		window.userbase = null;

		// stores
		set_store_value(userbaseStore, $userbaseStore = userbase, $userbaseStore);

		set_store_value(userStore, $userStore = null, $userStore);

		set_store_value(
			promiseStore,
			$promiseStore = userbase.init({
				appId: 'cc34d760-bf44-4aba-8ed6-9e7db48bb9be'
			}).then(session => set_store_value(userStore, $userStore = session.user, $userStore)),
			$promiseStore
		);

		function signout() {
			set_store_value(promiseStore, $promiseStore = $userbaseStore.signOut().then(() => set_store_value(userStore, $userStore = null, $userStore)), $promiseStore);
		}

		const writable_props = [];

		Object.keys($$props).forEach(key => {
			if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<App> was created with unknown prop '${key}'`);
		});

		$$self.$capture_state = () => ({
			Tailwindcss,
			Router,
			SignUp,
			SignIn,
			Loader,
			userbaseStore,
			userStore,
			promiseStore,
			Home,
			Banner,
			userbase,
			signout,
			$userStore,
			$userbaseStore,
			$promiseStore
		});

		return [];
	}

	class App extends SvelteComponentDev {
		constructor(options) {
			super(options);
			init(this, options, instance, create_fragment, safe_not_equal, {});

			dispatch_dev("SvelteRegisterComponent", {
				component: this,
				tagName: "App",
				options,
				id: create_fragment.name
			});
		}
	}

	const app = new App({
		target: document.body,
		props: {
			name: 'world'
		}
	});

	return app;

}());
//# sourceMappingURL=bundle.js.map
